#include "tmGrammar/Cut.hh"
#include "tmGrammar/Object.hh"
#include "tmGrammar/ObjectGrammar.hh"

#include "tmUtil/tmUtil.hh"

#include <boost/algorithm/string.hpp>

namespace Object
{


// all the objects
const reserved objectName{
  {MU, 1},
  {EG, 1},
  {TAU, 1},
  {JET, 1},
  {ETT, 1},
  {HTT, 1},
  {ETM, 1},
  {HTM, 1},
  {EXT, 1},
  {MBT0HFP, 1},
  {MBT1HFP, 1},
  {MBT0HFM, 1},
  {MBT1HFM, 1},
  {ETTEM, 1},
  {ETMHF, 1},
  {HTMHF, 1},
  {TOWERCOUNT, 1},
  {ASYMET, 1},
  {ASYMHT, 1},
  {ASYMETHF, 1},
  {ASYMHTHF, 1},
  {CENT0, 1},
  {CENT1, 1},
  {CENT2, 1},
  {CENT3, 1},
  {CENT4, 1},
  {CENT5, 1},
  {CENT6, 1},
  {CENT7, 1},
  {MUS0, 1},
  {MUS1, 1},
  {MUS2, 1},
  {MUSOOT0, 1},
  {MUSOOT1, 1},
  {ADT, 1},
  {ZDCP, 1},
  {ZDCM, 1},
  {AXO, 1},
  {TOPO, 1},
  {CICADA, 1},
};


// particles
const std::map<object_type, std::vector<std::string>> objectCuts{
  {Muon, {
    Cut::MU_UPT,
    Cut::MU_ETA,
    Cut::MU_PHI,
    Cut::MU_CHG,
    Cut::MU_QLTY,
    Cut::MU_ISO,
    Cut::MU_IP,
    Cut::MU_SLICE,
    Cut::MU_INDEX,
  }},
  {Egamma, {
    Cut::EG_ETA,
    Cut::EG_PHI,
    Cut::EG_QLTY,
    Cut::EG_ISO,
    Cut::EG_SLICE,
  }},
  {Tau, {
    Cut::TAU_ETA,
    Cut::TAU_PHI,
    Cut::TAU_QLTY,
    Cut::TAU_ISO,
    Cut::TAU_SLICE,
  }},
  {Jet, {
    Cut::JET_ETA,
    Cut::JET_PHI,
    Cut::JET_QLTY,
    Cut::JET_DISP,
    Cut::JET_SLICE,
  }},
  {Vector, {
    Cut::ETM_PHI,
    Cut::HTM_PHI,
    Cut::ETMHF_PHI,
    Cut::HTMHF_PHI,
  }},
  {Adt, {
    Cut::ADT_ASCORE,
  }},
  {Axo, {
    Cut::AXO_MODEL,
    Cut::AXO_SCORE,
  }},
  {Topo, {
    Cut::TOPO_MODEL,
    Cut::TOPO_SCORE,
  }},
  {Cicada, {
    Cut::CICADA_CSCORE,
  }},
};


// scalers
const reserved scalerName{
  {ETT, 1},
  {HTT, 1},
  {MBT0HFP, 1},
  {MBT1HFP, 1},
  {MBT0HFM, 1},
  {MBT1HFM, 1},
  {ETTEM, 1},
  {TOWERCOUNT, 1},
  {ASYMET, 1},
  {ASYMHT, 1},
  {ASYMETHF, 1},
  {ASYMHTHF, 1},
  {ZDCP, 1},
  {ZDCM, 1},
};


// states
const reserved signalName{
  {CENT0, 1},
  {CENT1, 1},
  {CENT2, 1},
  {CENT3, 1},
  {CENT4, 1},
  {CENT5, 1},
  {CENT6, 1},
  {CENT7, 1},
  {MUS0, 1},
  {MUS1, 1},
  {MUS2, 1},
  {MUSOOT0, 1},
  {MUSOOT1, 1},
};


// vectors
const reserved vectorName{
  {ETM, 1},
  {HTM, 1},
  {ETMHF, 1},
  {HTMHF, 1},
};


// all the comparisons
const reserved comparisonName{
  {EQ, 1},
  {NE, 1},
  {GT, 1},
  {GE, 1},
  {LT, 1},
  {LE, 1},
};


// ---------------------------------------------------------------------
// constructor & destructor
// ---------------------------------------------------------------------
Item::Item()
 : name(),
   comparison(GE),
   threshold(),
   bx_offset("+0"),
   cuts(),
   type(Unknown),
   message()
{
}


// ---------------------------------------------------------------------
// methods
// ---------------------------------------------------------------------

object_type
Item::getType() const
{
  if (boost::algorithm::starts_with(name, EXT + "_")) return External;

  for (const auto& item: Object::scalerName)
  {
    if (name == item.first) return Scaler;
  }

  for (const auto& item: Object::signalName)
  {
    if (name == item.first) return Signal;
  }

  for (const auto& item: Object::vectorName)
  {
    if (name == item.first) return Vector;
  }

  if (name == CICADA) return Cicada;
  if (name == TOPO) return Topo;
  if (name == TAU) return Tau;
  if (name == JET) return Jet;
  if (name == ADT) return Adt;
  if (name == AXO) return Axo;
  if (name == MU) return Muon;
  if (name == EG) return Egamma;

  return Unknown;
}


bool
Item::isValidCut(const std::string& cut)
{
  if (objectCuts.count(type))
  {
    for (const auto& object_cut: objectCuts.at(type))
    {
      if (boost::algorithm::starts_with(cut, object_cut))
        return true;
    }
  }

  return false;
}


std::string
Item::getObjectName() const
{
  std::ostringstream oss;

  oss << name;
  if (comparison != GE)
    oss << comparison;
  oss << threshold;
  if (bx_offset != "+0")
    oss << bx_offset;

  return oss.str();
}


bool
parser(const std::string& token,
       Object::Item& item)
{
  std::string::const_iterator begin = token.begin();
  std::string::const_iterator end = token.end();

  typedef std::string::const_iterator iterator_type;
  typedef object_grammar<iterator_type> object_grammar;
  using boost::spirit::ascii::space;

  Item_ item_;
  object_grammar g;
  bool r = phrase_parse(begin, end, g, space, item_);

  if (not (r and begin == end))
  {
    TM_LOG_ERR("parser error: " << TM_QUOTE(token));
    item.message += " Object::parser: '" + token + "'";
    return false;
  }

  item.name = item_.name;
  if (item_.comparison.empty())
  {
    item.comparison = GE;
  }
  else
  {
    item.comparison = item_.comparison;
  }
  item.threshold = item_.threshold;
  if (item_.bx_offset.empty())
  {
    item.bx_offset = "+0";
  }
  else
  {
    item.bx_offset = item_.bx_offset;
  }

  item.type = item.getType();

  if (item_.cuts.empty()) return true;

  Cut::Item cut;
  if (not Cut::parser(item_.cuts, cut))
  {
    item.message += cut.message;
    return false;
  }

  for (const auto& name: cut.name)
  {
    if (not item.isValidCut(name)) {
      TM_LOG_ERR(TM_QUOTE(name) << " is not valid for " << TM_QUOTE(item.name));
      item.message += " Object::parser: '" + name +
                      "' is not valid for '" +  item.name + "'";
      return false;
    }
    item.cuts.emplace_back(name);
  }

  return true;
}


bool
isObject(const std::string& element)
{
  for (const auto& item: objectName)
  {
    // TODO: unsorted weak name check!
    if (boost::algorithm::starts_with(element, item.first))
    {
      Item object;
      if (not parser(element, object))
        continue;
      return true;
    }
  }

  return false;
}

} // namespace Object
