#ifndef test_format_hh
#define test_format_hh

#include <iostream>
#include <string>
#include <vector>

#include "tmGrammar/Algorithm.hh"
#include "tmGrammar/Function.hh"
#include "tmGrammar/Object.hh"
#include "tmGrammar/Cut.hh"

std::ostream&
operator<<(std::ostream& os, const std::vector<std::string> v)
{
  os << "[";
  std::vector<std::string>::const_iterator it;
  for (it = std::begin(v); it != std::end(v); ++it)
  {
    if (it != std::begin(v))
    {
      os << ", ";
    }
    os << "\"" << *it << "\"";
  }
  os << "]";
  return os;
}


std::ostream&
operator<<(std::ostream& os, const Cut::Item& item)
{
  os << "cut:\n";
  os << " name: " << item.name << "\n";
  os << " message: \"" << item.message << "\"\n";
  return os;
}


std::ostream&
operator<<(std::ostream& os, const Object::Item& item)
{
  os << "object:\n";
  os << " name: \"" << item.name << "\"\n";
  os << " type: " << item.type << "\n";
  os << " comparison: \"" << item.comparison << "\"\n";
  os << " bx_offset: \"" << item.bx_offset << "\"\n";
  os << " cuts: " << item.cuts << "\n";
  os << " message: \"" << item.message << "\"\n";
  return os;
}


std::ostream&
operator<<(std::ostream& os, const Function::Item& item)
{
  os << "function:\n";
  os << " name: \"" << item.name << "\"\n";
  os << " type: " << item.type << "\n";
  os << " metric: " << item.metric << "\n";
  os << " objects:\n";
  os << "  - name: \"" << item.name << "\"\n";
  os << "    type: " << item.type << "\n";
  os << " cuts: " << item.cuts << "\n";
  os << " message: \"" << item.message << "\"\n";
  return os;
}

#endif // test_format_hh
