#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE tmGrammar

#include "tmGrammar/Algorithm.hh"
#include "tmGrammar/Function.hh"
#include "tmGrammar/Object.hh"
#include "tmGrammar/Cut.hh"

#include <boost/test/unit_test.hpp>
#include <boost/algorithm/string.hpp>

#include <string>
#include <vector>

BOOST_AUTO_TEST_SUITE(Algorithm)

BOOST_AUTO_TEST_CASE(Algorithm_isGate_valid)
{
  const std::vector<std::string> tokens = {
    Algorithm::AND,
    Algorithm::OR,
    Algorithm::NOT,
    Algorithm::XOR
  };

  for (const auto& token: tokens)
  {
    BOOST_CHECK_EQUAL(Algorithm::isGate(token), true);
  }
}

BOOST_AUTO_TEST_CASE(Algorithm_isGate_invalid)
{
  const std::vector<std::string> tokens = {
    " AND ",
    "MU10",
    "comb{MU0,MU0}",
    "foobar",
    "ANDOR",
    "XORNOT"
  };

  for (const auto& token: tokens)
  {
    BOOST_CHECK_EQUAL(Algorithm::isGate(token), false);
  }
}

BOOST_AUTO_TEST_CASE(Algorithm_parser)
{
  std::vector<std::string> reference;

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("FOO AND NOT BAR"), true);
  // NOTE: RPN (Reverse Polish notation)
  boost::split(reference, "FOO BAR NOT AND", boost::is_any_of(" "));
  BOOST_CHECK_EQUAL_COLLECTIONS(Algorithm::Logic::getTokens().begin(),
                                Algorithm::Logic::getTokens().end(),
                                reference.begin(), reference.end());

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("NOT FOO OR NOT BAR AND BAZ"), true);
  // NOTE: RPN (Reverse Polish notation)
  boost::split(reference, "FOO NOT BAR NOT OR BAZ AND", boost::is_any_of(" "));
  BOOST_CHECK_EQUAL_COLLECTIONS(Algorithm::Logic::getTokens().begin(),
                                Algorithm::Logic::getTokens().end(),
                                reference.begin(), reference.end());

  // functions

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("some_func{FOO42,BAR42,BAZ42}"), true);

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("some_func{FOO42,BAR42}[BAZ]"), true);

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("another_func{FOO42[BAR],BAR42[BAZ]}[BAZ,FOO]"), true);

  // fault injections

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("AND"), false);

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("NOT AND"), false);

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("FOO AND"), false);

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("AND FOO"), false);

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("AND OR AND"), false);

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("FOO OR NOT"), false);

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("FOO NOT OR BAR"), false);

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Algorithm::parser("NOT FOO AND XOR"), false);
}

BOOST_AUTO_TEST_CASE(Algorithm_Logic)
{
  Algorithm::Logic::clear();
  Algorithm::Logic logic;

  logic.tokens.emplace_back("foo");
  logic.append("bar");
  logic.append("baz");

  std::vector<std::string> reference;
  boost::split(reference, "foo bar baz", boost::is_any_of(" "));
  BOOST_CHECK_EQUAL_COLLECTIONS(logic.getTokens().begin(), logic.getTokens().end(),
                                reference.begin(), reference.end());

  Algorithm::Logic::clear();
  BOOST_CHECK_EQUAL(Logic::getTokens().size(), 0);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Object)

BOOST_AUTO_TEST_CASE(Object_parser)
{
  struct test_case_t
  {
    std::string token;
    bool result;
    std::string name;
    std::string threshold;
    std::string bx_offset;
    Object::object_type object_type;
    std::string object_name;
  };

  const std::vector<test_case_t> test_cases = {
    {"MU0", true, "MU", "0", "+0", Object::Muon, "MU0"},
    {"MU0-0", true, "MU", "0", "-0", Object::Muon, "MU0-0"},
    {"MU0-1", true, "MU", "0", "-1", Object::Muon, "MU0-1"},
    {"MU0-2", true, "MU", "0", "-2", Object::Muon, "MU0-2"},
    {"MU0+0", true, "MU", "0", "+0", Object::Muon, "MU0"},
    {"MU0+1", true, "MU", "0", "+1", Object::Muon, "MU0+1"},
    {"MU0+2", true, "MU", "0", "+2", Object::Muon, "MU0+2"},
    {"MU0[MU-UPT_10_20]", true, "MU", "0", "+0", Object::Muon, "MU0"},
    {"MU0[MU-INDEX_10_20]", true, "MU", "0", "+0", Object::Muon, "MU0"},
    {"MU123+1[MU-UPT_10_20]", true, "MU", "123", "+1", Object::Muon, "MU123+1"},
    {"MU2p5-1[MU-UPT_10_20,MU-ETA_A,MU-ETA_B,MU-ETA_C,MU-ETA_D,MU-ETA_E,MU-PHI_A,MU-PHI_B,MU-ISO_A]", true, "MU", "2p5", "-1", Object::Muon, "MU2p5-1"},
    {"EG0", true, "EG", "0", "+0", Object::Egamma, "EG0"},
    {"EG0-0", true, "EG", "0", "-0", Object::Egamma, "EG0-0"},
    {"EG0-1", true, "EG", "0", "-1", Object::Egamma, "EG0-1"},
    {"EG0-2", true, "EG", "0", "-2", Object::Egamma, "EG0-2"},
    {"EG0+0", true, "EG", "0", "+0", Object::Egamma, "EG0"},
    {"EG0+1", true, "EG", "0", "+1", Object::Egamma, "EG0+1"},
    {"EG0+2", true, "EG", "0", "+2", Object::Egamma, "EG0+2"},
    {"JET0", true, "JET", "0", "+0", Object::Jet, "JET0"},
    {"JET0-0", true, "JET", "0", "-0", Object::Jet, "JET0-0"},
    {"JET0-1", true, "JET", "0", "-1", Object::Jet, "JET0-1"},
    {"JET0-2", true, "JET", "0", "-2", Object::Jet, "JET0-2"},
    {"JET0+0", true, "JET", "0", "+0", Object::Jet, "JET0"},
    {"JET0+1", true, "JET", "0", "+1", Object::Jet, "JET0+1"},
    {"JET0+2", true, "JET", "0", "+2", Object::Jet, "JET0+2"},
    {"JET123", true, "JET", "123", "+0", Object::Jet, "JET123"},
    {"TAU0", true, "TAU", "0", "+0", Object::Tau, "TAU0"},
    {"TAU0-0", true, "TAU", "0", "-0", Object::Tau, "TAU0-0"},
    {"TAU0-1", true, "TAU", "0", "-1", Object::Tau, "TAU0-1"},
    {"TAU0-2", true, "TAU", "0", "-2", Object::Tau, "TAU0-2"},
    {"TAU0+0", true, "TAU", "0", "+0", Object::Tau, "TAU0"},
    {"TAU0+1", true, "TAU", "0", "+1", Object::Tau, "TAU0+1"},
    {"TAU0+2", true, "TAU", "0", "+2", Object::Tau, "TAU0+2"},
    {"TAU2p5+1", true, "TAU", "2p5", "+1", Object::Tau, "TAU2p5+1"},
    {"ZDCP0", true, "ZDCP", "0", "+0", Object::Scaler, "ZDCP0"},
    {"ZDCM0", true, "ZDCM", "0", "+0", Object::Scaler, "ZDCM0"},
    {"ZDCP42-1", true, "ZDCP", "42", "-1", Object::Scaler, "ZDCP42-1"},
    {"ZDCM44+1", true, "ZDCM", "44", "+1", Object::Scaler, "ZDCM44+1"},
  };

  for (const auto test_case: test_cases)
  {
    Object::Item item;
    BOOST_CHECK_EQUAL(Object::parser(test_case.token, item), test_case.result);
    BOOST_CHECK_EQUAL(item.message, "");
    BOOST_CHECK_EQUAL(item.name, test_case.name);
    BOOST_CHECK_EQUAL(item.threshold, test_case.threshold);
    BOOST_CHECK_EQUAL(item.bx_offset, test_case.bx_offset);
    BOOST_CHECK_EQUAL(item.getType(), test_case.object_type);
    BOOST_CHECK_EQUAL(item.getObjectName(), test_case.object_name);
  }
}

BOOST_AUTO_TEST_CASE(Object_external_parser)
{
  struct test_case_t
  {
    std::string token;
    bool result;
    std::string name;
    std::string bx_offset;
    std::string object_name;
  };

  const std::vector<test_case_t> test_cases = {
    {"EXT_FOO", true, "EXT_FOO", "+0", "EXT_FOO"},
    {"EXT_FOO+0", true, "EXT_FOO", "+0", "EXT_FOO"},
    {"EXT_FOO+1", true, "EXT_FOO", "+1", "EXT_FOO+1"},
    {"EXT_FOO+2", true, "EXT_FOO", "+2", "EXT_FOO+2"},
    {"EXT_FOO-0", true, "EXT_FOO", "-0", "EXT_FOO-0"},
    {"EXT_FOO-1", true, "EXT_FOO", "-1", "EXT_FOO-1"},
    {"EXT_FOO-2", true, "EXT_FOO", "-2", "EXT_FOO-2"},
  };

  for (const auto test_case: test_cases)
  {
    Object::Item item;
    BOOST_CHECK_EQUAL(Object::parser(test_case.token, item), test_case.result);
    BOOST_CHECK_EQUAL(item.message, "");
    BOOST_CHECK_EQUAL(item.name, test_case.name);
    BOOST_CHECK_EQUAL(item.threshold, "");
    BOOST_CHECK_EQUAL(item.bx_offset, test_case.bx_offset);
    BOOST_CHECK_EQUAL(item.getType(), Object::External);
    BOOST_CHECK_EQUAL(item.getObjectName(), test_case.object_name);
  }
}

BOOST_AUTO_TEST_CASE(Object_signal_parser)
{
  struct test_case_t
  {
    std::string token;
    bool result;
    std::string name;
    std::string bx_offset;
    std::string object_name;
  };

  const std::vector<test_case_t> test_cases = {
    {"CENT0", true, "CENT0", "+0", "CENT0"},
    {"CENT0+0", true, "CENT0", "+0", "CENT0"},
    {"CENT0+1", true, "CENT0", "+1", "CENT0+1"},
    {"CENT0+2", true, "CENT0", "+2", "CENT0+2"},
    {"CENT0-0", true, "CENT0", "-0", "CENT0-0"},
    {"CENT0-1", true, "CENT0", "-1", "CENT0-1"},
    {"CENT0-2", true, "CENT0", "-2", "CENT0-2"},
    {"CENT1", true, "CENT1", "+0", "CENT1"},
    {"CENT2", true, "CENT2", "+0", "CENT2"},
    {"CENT3", true, "CENT3", "+0", "CENT3"},
    {"CENT4", true, "CENT4", "+0", "CENT4"},
    {"CENT5", true, "CENT5", "+0", "CENT5"},
    {"CENT6", true, "CENT6", "+0", "CENT6"},
    {"CENT7", true, "CENT7", "+0", "CENT7"},
    {"MUS0", true, "MUS0", "+0", "MUS0"},
    {"MUS0+0", true, "MUS0", "+0", "MUS0"},
    {"MUS0+1", true, "MUS0", "+1", "MUS0+1"},
    {"MUS0+2", true, "MUS0", "+2", "MUS0+2"},
    {"MUS0-0", true, "MUS0", "-0", "MUS0-0"},
    {"MUS0-1", true, "MUS0", "-1", "MUS0-1"},
    {"MUS0-2", true, "MUS0", "-2", "MUS0-2"},
    {"MUS0", true, "MUS0", "+0", "MUS0"},
    {"MUS1", true, "MUS1", "+0", "MUS1"},
    {"MUS2", true, "MUS2", "+0", "MUS2"},
    {"MUSOOT0", true, "MUSOOT0", "+0", "MUSOOT0"},
    {"MUSOOT1", true, "MUSOOT1", "+0", "MUSOOT1"},
  };

  for (const auto test_case: test_cases)
  {
    Object::Item item;
    BOOST_CHECK_EQUAL(Object::parser(test_case.token, item), test_case.result);
    BOOST_CHECK_EQUAL(item.message, "");
    BOOST_CHECK_EQUAL(item.name, test_case.name);
    BOOST_CHECK_EQUAL(item.threshold, "");
    BOOST_CHECK_EQUAL(item.bx_offset, test_case.bx_offset);
    BOOST_CHECK_EQUAL(item.getType(), Object::Signal);
    BOOST_CHECK_EQUAL(item.getObjectName(), test_case.object_name);
  }
}

BOOST_AUTO_TEST_CASE(Object_ml_trigger_parser)
{
  struct test_case_t
  {
    std::string token;
    bool result;
    std::string name;
    std::string bx_offset;
    Object::object_type object_type;
    std::string object_name;
  };

  const std::vector<test_case_t> test_cases = {
    {"ADT", true, "ADT", "+0", Object::Adt, "ADT"},
    {"ADT+1", true, "ADT", "+1", Object::Adt, "ADT+1"},
    {"ADT[ADT-ASCORE_40k]", true, "ADT", "+0", Object::Adt, "ADT"},
    {"ADT-1[ADT-ASCORE_40k]", true, "ADT", "-1", Object::Adt, "ADT-1"},
    {"AXO", true, "AXO", "+0", Object::Axo, "AXO"},
    {"AXO+1", true, "AXO", "+1", Object::Axo, "AXO+1"},
    {"AXO[AXO-SCORE_40k]", true, "AXO", "+0", Object::Axo, "AXO"},
    {"AXO[AXO-MODEL_v1,AXO-SCORE_40k]", true, "AXO", "+0", Object::Axo, "AXO"},
    {"AXO-1[AXO-MODEL_v1,AXO-SCORE_40k]", true, "AXO", "-1", Object::Axo, "AXO-1"},
    {"TOPO", true, "TOPO", "+0", Object::Topo, "TOPO"},
    {"TOPO+1", true, "TOPO", "+1", Object::Topo, "TOPO+1"},
    {"TOPO[TOPO-SCORE_20k]", true, "TOPO", "+0", Object::Topo, "TOPO"},
    {"TOPO[TOPO-MODEL_v1,TOPO-SCORE_20k]", true, "TOPO", "+0", Object::Topo, "TOPO"},
    {"TOPO-1[TOPO-MODEL_v1,TOPO-SCORE_20k]", true, "TOPO", "-1", Object::Topo, "TOPO-1"},
    {"CICADA", true, "CICADA", "+0", Object::Cicada, "CICADA"},
    {"CICADA+1", true, "CICADA", "+1", Object::Cicada, "CICADA+1"},
    {"CICADA[CICADA-CSCORE_123]", true, "CICADA", "+0", Object::Cicada, "CICADA"},
    {"CICADA-1[CICADA-CSCORE_123]", true, "CICADA", "-1", Object::Cicada, "CICADA-1"},
  };

  for (const auto test_case: test_cases)
  {
    Object::Item item;
    BOOST_CHECK_EQUAL(Object::parser(test_case.token, item), test_case.result);
    BOOST_CHECK_EQUAL(item.message, "");
    BOOST_CHECK_EQUAL(item.name, test_case.name);
    BOOST_CHECK_EQUAL(item.threshold, "");
    BOOST_CHECK_EQUAL(item.bx_offset, test_case.bx_offset);
    BOOST_CHECK_EQUAL(item.getType(), test_case.object_type);
    BOOST_CHECK_EQUAL(item.getObjectName(), test_case.object_name);
  }
}

BOOST_AUTO_TEST_CASE(Object_isObject_true)
{
  struct test_case_t
  {
    std::string token;
    bool result;
  };

  const std::vector<test_case_t> test_cases = {
    {"MU0[MU-UPT_X]", true},
    {"EG1", true},
    {"TAU2", true},
    {"JET4", true},
    {"TAU2", true},
    {"ADT", true},
    {"ADT+1", true},
    {"ADT[ADT-ASCORE_40k]", true},
    {"ADT-2[ADT-ASCORE_40k]", true},
    {"AXO", true},
    {"AXO+1", true},
    {"AXO[AXO-MODEL_v1,AXO-SCORE_40k]", true},
    {"AXO-2[AXO-MODEL_v1,AXO-SCORE_40k]", true},
    {"TOPO", true},
    {"TOPO+1", true},
    {"TOPO[TOPO-MODEL_v1,TOPO-SCORE_20k]", true},
    {"TOPO-2[TOPO-MODEL_v1,TOPO-SCORE_20k]", true},
    {"CICADA", true},
    {"CICADA+1", true},
    {"CICADA[CICADA-CSCORE_123]", true},
    {"CICADA-2[CICADA-CSCORE_123]", true},
    {"ZDCP200-2", true},
    {"ZDCM200+2", true},
  };

  for (const auto& test_case: test_cases)
  {
    BOOST_CHECK_EQUAL(Object::isObject(test_case.token), test_case.result);
  }
}

BOOST_AUTO_TEST_CASE(Object_isObject_false)
{
  const std::vector<std::string> tokens = {
    "FOO[MU-UPT_X]",
    "BAR",
    " TAU1 ",
    "comb{MU0,MU1}"
  };

  for (const auto& token: tokens)
  {
    BOOST_CHECK_EQUAL(Object::isObject(token), false);
  }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Cut)

BOOST_AUTO_TEST_CASE(Cut_parser)
{
  Cut::Item item;
  BOOST_CHECK_EQUAL(Cut::parser("MU-ISO_FOO, MU-ETA_BAR, MU-PHI_BAZ, MU-CHG_POS", item), true);
  BOOST_CHECK_EQUAL(item.message.empty(), true);
  std::vector<std::string> reference;
  boost::split(reference, "MU-ISO_FOO MU-ETA_BAR MU-PHI_BAZ MU-CHG_POS", boost::is_any_of(" "));
  BOOST_CHECK_EQUAL_COLLECTIONS(item.name.begin(), item.name.end(),
                                reference.begin(), reference.end());

  item.name.clear();
  item.message.clear(); // so thats not convenient at all

  BOOST_CHECK_EQUAL(Cut::parser("MU-FOO_BAR", item), false);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Function)

BOOST_AUTO_TEST_CASE(Function_parser_true)
{
  const std::vector<std::string> tokens = {
    "comb{MU0,MU0}",
    "dist{TAU0,EG1}[DETA_X,DR_X]",

    "mass_inv{TAU0,EG1}[MASS_X]",
    "mass_inv{TAU0,EG1}[DR_X,MASS_X]",
    "mass_inv{TAU0,EG1}[DETA_X,MASS_X]",
    "mass_inv{TAU0,EG1}[DPHI_X,MASS_X]",
    "mass_inv{TAU0,EG1}[DETA_X,DPHI_X,MASS_X]",
    "mass_inv{TAU0,EG1}[DR_X,DPHI_X,MASS_X]",
    "mass_inv{TAU0,EG1}[DR_X,DETA_X,MASS_X]",
    "mass_inv{TAU0,EG1}[DR_X,DETA_X,DPHI_X,MASS_X]",

    "mass_inv_upt{MU0,MU1}[MASSUPT_X]",
    "mass_inv_upt{MU0,MU1}[DR_X,MASSUPT_X]",
    "mass_inv_upt{MU0,MU1}[DETA_X,MASSUPT_X]",
    "mass_inv_upt{MU0,MU1}[DPHI_X,MASSUPT_X]",
    "mass_inv_upt{MU0,MU1}[DETA_X,DPHI_X,MASSUPT_X]",
    "mass_inv_upt{MU0,MU1}[DR_X,DPHI_X,MASSUPT_X]",
    "mass_inv_upt{MU0,MU1}[DR_X,DETA_X,MASSUPT_X]",
    "mass_inv_upt{MU0,MU1}[DR_X,DETA_X,DPHI_X,MASSUPT_X]",

    "mass_inv_dr{TAU0,EG1}[MASSDR_X]",
    "mass_inv_dr{TAU0,EG1}[DR_X,MASSDR_X]",
    "mass_inv_dr{TAU0,EG1}[DETA_X,MASSDR_X]",
    "mass_inv_dr{TAU0,EG1}[DPHI_X,MASSDR_X]",
    "mass_inv_dr{TAU0,EG1}[DETA_X,DPHI_X,MASSDR_X]",
    "mass_inv_dr{TAU0,EG1}[DR_X,DPHI_X,MASSDR_X]",
    "mass_inv_dr{TAU0,EG1}[DR_X,DETA_X,MASSDR_X]",
    "mass_inv_dr{TAU0,EG1}[DR_X,DETA_X,DPHI_X,MASSDR_X]",

    "mass_inv_3{EG0,EG1,EG0}[MASS_X]",
    "mass_inv_3{TAU0,TAU1,TAU2}[MASS_X]",
    "mass_inv_3{MU0,MU1,MU2}[MASS_X]",
    "mass_inv_3{MU0,EG1,EG2}[MASS_X]",
    "mass_inv_3{EG1,MU2,TAU3}[MASS_X]",

    "comb_orm{JET0,TAU0}[ORMDETA_X]",
    "comb_orm{JET0,JET1,TAU0}[ORMDR_X]",
    "comb_orm{JET3,JET2,JET1,TAU0}[ORMDR_X]",
    "comb_orm{JET4,JET3,JET2,JET1,TAU0}[ORMDR_X]",

    "dist_orm{TAU0,EG1}[ORMDETA_X,DR_X]",
    "dist_orm{TAU0,TAU1,EG1}[ORMDETA_X,DR_X]",

    "mass_inv_orm{TAU0,EG1}[ORMDETA_X,MASS_X]",
    "mass_inv_orm{TAU0,TAU1,EG1}[ORMDETA_X,MASS_X]",

    "mass_trv_orm{TAU0,EG1}[ORMDETA_X,MASS_X]",
    "mass_trv_orm{TAU0,TAU1,EG1}[ORMDETA_X,MASS_X]"
  };

  for (const auto& token: tokens)
  {
    Function::Item item;
    BOOST_CHECK_MESSAGE(Function::parser(token, item) == true, token);
    BOOST_CHECK_EQUAL(item.message.empty(), true);
  }
}

BOOST_AUTO_TEST_CASE(Function_parser_false)
{
  const std::vector<std::string> tokens = {
    "comb_orm{JET0}[ORMDETA_X]",
    "comb_orm{JET0,JET0,JET0}[ORMDETA_X]",
    "comb_orm{JET3,JET2}[ORMDETA_X]",
    "comb_orm{JET3,JET2,JET1,JET1}[ORMDETA_X]",
    "comb_orm{JET4,JET3,JET2}[ORMDETA_X]",
    "comb_orm{JET4,JET3,JET2,JET1,JET0}[ORMDR_X]",

    "dist_orm{JET0,JET1}[DR_X]", // missing overlap removal cut
    "dist_orm{JET0,TAU0}[ORMDR_X]", // missing delta cut
    "dist_orm{JET0,TAU0,TAU0}[ORMDR_X,DR_X]",
    "dist_orm{JET0,JET1,JET2,TAU0}[ORMDR_X,DR_X]",
    "dist_orm{JET0,JET1,TAU0,TAU1}[ORMDR_X,DR_X]",

    "mass_inv_3{EG0,EG1}[MASS_X]",
    "mass_inv_3{TAU0,TAU1,TAU2}[MASSDR_X]",

    "mass_inv_orm{JET0,JET1}[MASS_Z]", // missing overlap removal cut
    "mass_inv_orm{JET0,TAU0}[ORMDR_X]", // missing mass cut
    "mass_inv_orm{JET0,TAU0,TAU0}[ORMDR_X,MASS_Z]",
    "mass_inv_orm{JET0,JET1,JET2,TAU0}[ORMDR_X,MASS_Z]",
    "mass_inv_orm{JET0,JET1,TAU0,TAU1}[ORMDR_X,MASS_Z]",

    "mass_trv_orm{JET0,JET1}[MASS_Z]", // missing overlap removal cut
    "mass_trv_orm{JET0,TAU0}[ORMDR_X]", // missing mass cut
    "mass_trv_orm{JET0,TAU0,TAU0}[ORMDR_X,MASS_Z]",
    "mass_trv_orm{JET0,JET1,JET2,TAU0}[ORMDR_X,MASS_Z]",
    "mass_trv_orm{JET0,JET1,TAU0,TAU1}[ORMDR_X,MASS_Z]"
  };

  for (const auto& token: tokens)
  {
    Function::Item item;
    BOOST_CHECK_MESSAGE(Function::parser(token, item) == false, token);
    BOOST_CHECK_EQUAL(item.message.empty(), false);
  }
}

BOOST_AUTO_TEST_CASE(Function_isFunction_true)
{
  const std::vector<std::string> tokens = {
    "comb{MU0,MU0}",
    "dist{TAU0,EG1}[DETA_X,DR_X]",
    "mass{TAU0,EG1}[MASS_X]",
    "mass_inv{TAU0,EG1}[MASS_X]",
    "mass_inv_upt{MU0,MU0}[MASSUPT_X]",
    "mass_inv_dr{MU0,MU1}[MASSDR_X]",
    "comb_orm{JET0,TAU0}[ORMDETA_X]",
    "comb_orm{JET0,JET0,TAU0}[ORMDR_X]",
    "comb_orm{JET3,JET2,JET1,TAU0}[ORMDR_X]",
    "comb_orm{JET4,JET3,JET2,JET1,TAU0}[ORMDR_X]"
  };

  for (const auto& token: tokens)
  {
    BOOST_CHECK_EQUAL(Function::isFunction(token), true);
  }
}

BOOST_AUTO_TEST_CASE(Function_isFunction_false)
{
  const std::vector<std::string> tokens = {
    "mass_inv_upt{JET0,TAU0}[MASSUPT_X]",
    "mass_inv_upt{MU0,MU1}[MASS_X]"
  };

  for (const auto& token: tokens)
  {
    BOOST_CHECK_EQUAL(Function::isFunction(token), false);
  }
}

BOOST_AUTO_TEST_SUITE_END()
