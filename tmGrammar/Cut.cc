#include "tmGrammar/Cut.hh"
#include "tmGrammar/CutGrammar.hh"

#include "tmUtil/tmUtil.hh"

namespace Cut
{

const reserved cutName{
  {MU_UPT, 1},
  {MU_ETA, 1},
  {MU_PHI, 1},
  {MU_CHG, 1},
  {MU_QLTY, 1},
  {MU_ISO, 1},
  {MU_IP, 1},
  {MU_SLICE, 1},
  {MU_INDEX, 1},

  {EG_ETA, 1},
  {EG_PHI, 1},
  {EG_QLTY, 1},
  {EG_ISO, 1},
  {EG_SLICE, 1},

  {TAU_ETA, 1},
  {TAU_PHI, 1},
  {TAU_QLTY, 1},
  {TAU_ISO, 1},
  {TAU_SLICE, 1},

  {JET_ETA, 1},
  {JET_PHI, 1},
  {JET_QLTY, 1},
  {JET_DISP, 1},
  {JET_SLICE, 1},

  {ETM_PHI, 1},
  {HTM_PHI, 1},
  {ETMHF_PHI, 1},
  {HTMHF_PHI, 1},

  {ADT_ASCORE, 1},
  
  {AXO_SCORE, 1},
  {AXO_MODEL, 1},
  
  {CICADA_CSCORE, 1},
  
  {TOPO_SCORE, 1},
  {TOPO_MODEL, 1},

  {DETA, 1},
  {DPHI, 1},
  {DR, 1},
  {MASS, 1},
  {MASSUPT, 1},
  {MASSDR, 1},
  {CHGCOR, 1},
  {TBPT, 1},
  {ORMDETA, 1},
  {ORMDPHI, 1},
  {ORMDR, 1},
};


double
parseThreshold(const std::string& threshold)
{
  const auto value = boost::algorithm::replace_first_copy(threshold, "p", ".");
  return boost::lexical_cast<double>(std::move(value));
}


bool
parser(const std::string& token,
       Cut::Item& item)
{
  std::string::const_iterator begin = token.begin();
  std::string::const_iterator end = token.end();

  typedef std::string::const_iterator iterator_type;
  typedef cut_grammar<iterator_type> cut_grammar;
  using boost::spirit::ascii::space;

  cut_grammar g;
  bool r = phrase_parse(begin, end, g, space, item);

  if (not (r and begin == end))
  {
    std::ostringstream oss;
    oss << "parser error: " << TM_QUOTE(token);
    TM_LOG_ERR(oss.str());
    if (item.message.length())
      item.message += ", ";
    item.message += oss.str();
    return false;
  }

  return true;
}

} // namespace Cut
