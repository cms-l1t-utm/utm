/**
 * @author      Takashi Matsushita
 * Created:     04 Jul 2014
 */

#ifndef tmGrammar_Cut_hh
#define tmGrammar_Cut_hh

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <iostream>
#include <map>
#include <vector>
#include <string>

namespace Cut {

// cuts for objects
const std::string UPT = "UPT";
const std::string ETA = "ETA";
const std::string PHI = "PHI";
const std::string CHG = "CHG";
const std::string QLTY = "QLTY";
const std::string ISO = "ISO";
const std::string DISP = "DISP";
const std::string IP = "IP";
const std::string SLICE = "SLICE";
const std::string INDEX = "INDEX";
const std::string ASCORE = "ASCORE";
const std::string SCORE = "SCORE";
const std::string MODEL = "MODEL";
const std::string CSCORE = "CSCORE";

const std::string MU_UPT = "MU-UPT";     /**< muon unconstrained pt */
const std::string MU_ETA = "MU-ETA";     /**< muon eta */
const std::string MU_PHI = "MU-PHI";     /**< muon phi */
const std::string MU_CHG = "MU-CHG";     /**< muon charge */
const std::string MU_QLTY = "MU-QLTY";   /**< muon quality */
const std::string MU_ISO = "MU-ISO";     /**< muon isolation */
const std::string MU_IP = "MU-IP";       /**< muon impact parameter */
const std::string MU_SLICE = "MU-SLICE"; /**< muon collection slice */
const std::string MU_INDEX = "MU-INDEX"; /**< muon collection slice */

const std::string EG_ETA = "EG-ETA";     /**< egamma eta */
const std::string EG_PHI = "EG-PHI";     /**< egamma phi */
const std::string EG_QLTY = "EG-QLTY";   /**< egamma quality */
const std::string EG_ISO = "EG-ISO";     /**< egamma isolation */
const std::string EG_SLICE = "EG-SLICE"; /**< egamma collection slice */

const std::string TAU_ETA = "TAU-ETA";     /**< tau eta */
const std::string TAU_PHI = "TAU-PHI";     /**< tau phi */
const std::string TAU_QLTY = "TAU-QLTY";   /**< tau quality */
const std::string TAU_ISO = "TAU-ISO";     /**< tau isolation */
const std::string TAU_SLICE = "TAU-SLICE"; /**< tau collection slice */

const std::string JET_ETA = "JET-ETA";     /**< jet eta */
const std::string JET_PHI = "JET-PHI";     /**< jet phi */
const std::string JET_QLTY = "JET-QLTY";   /**< jet quality */
const std::string JET_DISP = "JET-DISP";   /**< jet displaced */
const std::string JET_SLICE = "JET-SLICE"; /**< jet collection slice */

const std::string ETM_PHI = "ETM-PHI"; /**< ETM phi */
const std::string HTM_PHI = "HTM-PHI"; /**< HTM phi */
const std::string ETMHF_PHI = "ETMHF-PHI"; /**< ETMHF phi */
const std::string HTMHF_PHI = "HTMHF-PHI"; /**< HTMHF phi */

const std::string ADT_ASCORE = "ADT-ASCORE"; /**< ADT anomaly score */

const std::string AXO_SCORE = "AXO-SCORE"; /**< AXOL1TL anomaly score */
const std::string AXO_MODEL = "AXO-MODEL"; /**< AXOL1TL model */
const std::string CICADA_CSCORE = "CICADA-CSCORE"; /**< Cicada cicada score */
const std::string TOPO_SCORE = "TOPO-SCORE"; /**< Topological score */
const std::string TOPO_MODEL = "TOPO-MODEL"; /**< Topological model */

// cuts for functions
const std::string MASS = "MASS";       /**< mass */
const std::string MASSUPT = "MASSUPT"; /**< mass for unconstrained pt */
const std::string MASSDR = "MASSDR";   /**< mass divided by delta R */
const std::string DETA = "DETA";       /**< delta eta */
const std::string DPHI = "DPHI";       /**< delta phi */
const std::string DR = "DR";           /**< delta R */
const std::string CHGCOR = "CHGCOR";   /**< charge correlation  */
const std::string TBPT = "TBPT";       /**< two body Pt  */
const std::string ORMDETA = "ORMDETA"; /**< delta eta overlap removal */
const std::string ORMDPHI = "ORMDPHI"; /**< delta phi overlap removal */
const std::string ORMDR = "ORMDR";     /**< delta R overlap removal */

// character set for cuts
const std::string CHARSET_CUT = "a-zA-Z0-9_-";
const std::string CHARSET_CUTS = "a-zA-Z0-9_,-"; // TODO: possible weak precept


/**
  * This struct implements data structure for a cut
  */
struct Item
{
  std::vector<std::string> name;  /**< list of name of the cut */
  std::string message;

  Item() = default;
  virtual ~Item() = default;
}; // struct Item


/** parse threshold expression */
double parseThreshold(const std::string& threshold);


/** parse cut expression
  *
  * @param cut [in] cut expression
  * @param item [in/out] Item instance to store thre result
  * @return true if parsing was successfull otherwise false
  */
bool parser(const std::string& cut,
            Cut::Item& item);


/** list of names */
typedef std::map<std::string, int> reserved;
extern const reserved cutName;  /**< list of allowed cut names */

} // namespace Cut

#endif // tmGrammar_Cut_hh
/* eof */
