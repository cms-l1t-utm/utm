/**
 * @author      Takashi Matsushita
 * Created:     03 Jul 2014
 */

#ifndef tmGrammar_Object_hh
#define tmGrammar_Object_hh

#include <iostream>
#include <string>
#include <map>
#include <vector>

namespace Object
{

// object names
const std::string MU = "MU";   /**< muon */
const std::string EG = "EG";   /**< egamma */
const std::string TAU = "TAU"; /**< tau */
const std::string JET = "JET"; /**< jet */
const std::string ETT = "ETT"; /**< total ET */
const std::string HTT = "HTT"; /**< total HT */
const std::string ETM = "ETM"; /**< missing ET */
const std::string HTM = "HTM"; /**< missing HT */
const std::string EXT = "EXT"; /**< external signal */
const std::string MBT0HFP = "MBT0HFP"; /**< Minimum Bias Threshold 0 HF+ */
const std::string MBT1HFP = "MBT1HFP"; /**< Minimum Bias Threshold 1 HF+ */
const std::string MBT0HFM = "MBT0HFM"; /**< Minimum Bias Threshold 0 HF- */
const std::string MBT1HFM = "MBT1HFM"; /**< Minimum Bias Threshold 1 HF- */
const std::string ETTEM = "ETTEM"; /**< ECAL only total ET */
const std::string ETMHF = "ETMHF"; /**< missing ET with HF */
const std::string HTMHF = "HTMHF"; /**< missing HT with HF */
const std::string TOWERCOUNT = "TOWERCOUNT"; /**< calo tower count */
const std::string ASYMET = "ASYMET"; /**< asymmetry ET */
const std::string ASYMHT = "ASYMHT"; /**< asymmetry HT */
const std::string ASYMETHF = "ASYMETHF"; /**< asymmetry ET with HF */
const std::string ASYMHTHF = "ASYMHTHF"; /**< asymmetry HT with HF */
const std::string CENT0 = "CENT0"; /**< centrality 0 */
const std::string CENT1 = "CENT1"; /**< centrality 1 */
const std::string CENT2 = "CENT2"; /**< centrality 2 */
const std::string CENT3 = "CENT3"; /**< centrality 3 */
const std::string CENT4 = "CENT4"; /**< centrality 4 */
const std::string CENT5 = "CENT5"; /**< centrality 5 */
const std::string CENT6 = "CENT6"; /**< centrality 6 */
const std::string CENT7 = "CENT7"; /**< centrality 7 */
const std::string MUS0 = "MUS0"; /**< muon shower 0 */
const std::string MUS1 = "MUS1"; /**< muon shower 1 */
const std::string MUS2 = "MUS2"; /**< muon shower 2 */
const std::string MUSOOT0 = "MUSOOT0"; /**< muon shower out of time 0 */
const std::string MUSOOT1 = "MUSOOT1"; /**< muon shower out of time 1 */
const std::string ADT = "ADT"; /**< anomaly detection trigger */
const std::string ZDCP = "ZDCP"; /**< ZDC+ */
const std::string ZDCM = "ZDCM"; /**< ZDC- */
const std::string AXO = "AXO"; /**< AXOL1TL trigger */
const std::string TOPO = "TOPO"; /**< topological trigger */
const std::string CICADA = "CICADA"; /**< Cicada */

// comparisons
const std::string EQ = ".eq."; /**< equal */
const std::string NE = ".ne."; /**< not equal */
const std::string GT = ".gt."; /**< greater than */
const std::string GE = ".ge."; /**< greater than or equal to */
const std::string LT = ".lt."; /**< less than */
const std::string LE = ".le."; /**< less than or equal to */

// character set for object
const std::string CHARSET_OBJECT = "a-zA-Z0-9+-.";

/** type of objects */
enum object_type {
  Muon,
  Egamma,
  Tau,
  Jet,
  Scaler,
  Vector,
  External,
  Count,
  Unknown,
  Signal,
  Adt,
  Axo,
  Topo,
  Cicada,
};


/**
  * This struct implements data structure for an object
  */
struct Item
{
  std::string name;               /**< object name */
  std::string comparison;         /**< comparison operator */
  std::string threshold;          /**< threshold */
  std::string bx_offset;          /**< bunch crossing offset */
  std::vector<std::string> cuts;  /**< cuts for the object */
  object_type type;               /**< object type */
  std::string message;

  Item();
  virtual ~Item() = default;

  /** returns object type */
  object_type getType() const;

  /** checks if the cut expression is valid for this type */
  bool isValidCut(const std::string& token);

  /** returns object name */
  std::string getObjectName() const;

}; // struct Item


/** parse object expression
  *
  * @param object [in] object expression
  * @param item [in/out] Item instance to store the result
  * @return true if parsing was successfull otherwise false
  */
bool parser(const std::string& object,
            Object::Item& item);


/** checks if the given algorithm expression element is an object or not
 *
 * @param element [in] an element of an algorithm expression
 * @return true if the given element is an object
 */
bool isObject(const std::string& token);


/** list of names */
typedef std::map<std::string, int> reserved;
extern const reserved objectName;     /**< list of allowed object names */
extern const reserved comparisonName; /**< list of allowed comparison operator */
extern const reserved signalName;     /**< list of allowed signal names */

} // namespace Object

#endif // tmGrammar_Object_hh
