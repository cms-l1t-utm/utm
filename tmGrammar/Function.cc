#include "tmGrammar/Cut.hh"
#include "tmGrammar/Object.hh"
#include "tmGrammar/Function.hh"

#include "tmUtil/tmUtil.hh"

#include <boost/algorithm/string.hpp>

#include <algorithm>
#include <string>
#include <vector>

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <regex.h>

/*====================================================================*
 * implementation
 *====================================================================*/
namespace Function
{

/** @cond INTERNAL */
struct Item_
{
  std::string name;
  std::vector<std::string> objects;
  std::string cuts;
}; // struct Item_

/** Comparison functor for object types .*/
struct equalObjectType
{
  int type;
  equalObjectType(int type) : type(type) {}
  bool operator()(const Object::Item& item) { return item.type == type; }
};

/** Get typ name of cut. TODO
 * getCutType("MU-ISO_FOO"); --> "MU-ISO"
 */
std::string
getCutType(const std::string& cut)
{
  std::vector<std::string> tokens;
  boost::split(tokens, cut, boost::is_any_of("_"));
  return tokens.front();
}

/** @endcond */



// ---------------------------------------------------------------------
// static variables
// ---------------------------------------------------------------------

/** @cond INTERNAL */

// map of functions
const reserved functionName{
  {comb, 1},
  {comb_orm, 1},
  {dist, 1},
  {dist_orm, 1},
  {mass, 1},
  {mass_inv, 1},
  {mass_inv_upt, 1},
  {mass_inv_dr, 1},
  {mass_inv_3, 1},
  {mass_inv_orm, 1},
  {mass_trv, 1},
  {mass_trv_orm, 1}
};

// mapping function names to types
const std::map<std::string, FunctionType> functionTypes = {
  {dist, Distance},
  {dist_orm, DistanceOvRm},
  {comb, Combination},
  {comb_orm, CombinationOvRm},
  {mass, InvariantMass}, // for backward compatibility
  {mass_inv, InvariantMass},
  {mass_inv_upt, InvariantMassUpt},
  {mass_inv_dr, InvariantMassDeltaR},
  {mass_inv_3, InvariantMass3},
  {mass_inv_orm, InvariantMassOvRm},
  {mass_trv, TransverseMass},
  {mass_trv_orm, TransverseMassOvRm}
};


// list of cuts for all overlap removal functions
const std::vector<std::string> cutOvRm{
  Cut::ORMDETA,
  Cut::ORMDPHI,
  Cut::ORMDR
};


// objects for combination function
const std::vector<std::string> objComb{
  Object::MU,
  Object::EG,
  Object::TAU,
  Object::JET
};

// cuts for combination function
const std::vector<std::string> cutComb{
  Cut::CHGCOR,
  Cut::TBPT
};


// objects for combination with overlap removal function
const std::vector<std::string> objCombOvRm{
  Object::EG,
  Object::TAU,
  Object::JET
};

// cuts for combination with overlap removal function
const std::vector<std::string> cutCombOvRm{
  Cut::ORMDETA,
  Cut::ORMDPHI,
  Cut::ORMDR,
  Cut::TBPT
};


// objects for invariant mass function
const std::vector<std::string> objMassInv{
  Object::MU,
  Object::EG,
  Object::TAU,
  Object::JET
};

// cuts for invariant mass function
const std::vector<std::string> cutInvMass{
  Cut::MASS,
  Cut::DETA,
  Cut::DPHI,
  Cut::DR,
  Cut::CHGCOR,
  Cut::TBPT
};

// objects for invariant mass function
const std::vector<std::string> objMassInvUpt{
  Object::MU
};

// cuts for invariant mass function
const std::vector<std::string> cutInvMassUpt{
  Cut::MASSUPT,
  Cut::DETA,
  Cut::DPHI,
  Cut::DR,
  Cut::CHGCOR,
  Cut::TBPT
};

// objects for invariant mass divided by delta R function
const std::vector<std::string> objMassInvDeltaR{
  Object::MU,
  Object::EG,
  Object::TAU,
  Object::JET
};

// cuts for invariant mass divided by delta R function
const std::vector<std::string> cutInvMassDeltaR{
  Cut::MASSDR,
  Cut::DETA,
  Cut::DPHI,
  Cut::DR,
  Cut::CHGCOR,
  Cut::TBPT
};

// objects for invariant mass with three particels function
const std::vector<std::string> objMassInv3{
  Object::MU,
  Object::EG,
  Object::TAU,
  Object::JET
};

// cuts for invariant mass with three particles function
const std::vector<std::string> cutInvMass3{
  Cut::MASS,
  Cut::CHGCOR
};

// objects for transverse mass function
const std::vector<std::string> objMassTrv{
  Object::MU,
  Object::EG,
  Object::TAU,
  Object::JET,
  Object::ETM,
  Object::HTM,
  Object::ETMHF,
  Object::HTMHF
};

// cuts for transverse mass function
const std::vector<std::string> cutTrvMass{
  Cut::MASS,
  Cut::DETA,
  Cut::DPHI,
  Cut::DR,
  Cut::CHGCOR,
  Cut::TBPT
};


// invariant mass objects with overlap removal
const std::vector<std::string> objMassInvOvRm{
  Object::EG,
  Object::TAU,
  Object::JET
};

// transverse mass objects with overlap removal
const std::vector<std::string> objMassTrvOvRm{
  Object::EG,
  Object::TAU,
  Object::JET
};

// invariant mass cuts with overlap removal
const std::vector<std::string> cutInvMassOvRm{
  Cut::ORMDETA,
  Cut::ORMDPHI,
  Cut::ORMDR,
  Cut::MASS,
  Cut::DETA,
  Cut::DPHI,
  Cut::DR,
  Cut::TBPT
};

// transverse mass cuts with overlap removal
const std::vector<std::string> cutTrvMassOvRm{
  Cut::ORMDETA,
  Cut::ORMDPHI,
  Cut::ORMDR,
  Cut::MASS,
  Cut::DETA,
  Cut::DPHI,
  Cut::DR,
  Cut::TBPT
};


// objects for distance function with eta and phi
const std::vector<std::string> objDelta{
  Object::MU,
  Object::EG,
  Object::TAU,
  Object::JET
};

// objects for distance function with phi
const std::vector<std::string> objDeltaPhi{
  Object::MU,
  Object::EG,
  Object::TAU,
  Object::JET,
  Object::ETM,
  Object::HTM
};

// cuts for distance function
const std::vector<std::string> cutDist{
  Cut::DETA,
  Cut::DPHI,
  Cut::DR,
  Cut::CHGCOR,
  Cut::TBPT
};


// objects for distance with overlap removal function with eta and phi
const std::vector<std::string> objDeltaOvRm{
  Object::EG,
  Object::TAU,
  Object::JET
};

// objects for distance with overlap removal function with phi
const std::vector<std::string> objDeltaPhiOvRm{
  Object::EG,
  Object::TAU,
  Object::JET
};

// cuts for distance with overlap removal function
const std::vector<std::string> cutDistOvRm{
  Cut::ORMDETA,
  Cut::ORMDPHI,
  Cut::ORMDR,
  Cut::DETA,
  Cut::DPHI,
  Cut::DR,
  Cut::TBPT
};

/** @endcond */


// ---------------------------------------------------------------------
// methods
// ---------------------------------------------------------------------

bool
Item::isValidObject(const std::string& object,
                    std::string& message)
{
  const std::vector<std::string>* objects = 0;

  switch (type)
  {
    case Distance:
    {
      if (metric & DeltaPhi) objects = &objDeltaPhi;
      if ((metric & DeltaEta) or (metric & DeltaR)) objects = &objDelta;
      if (not objects)
      {
        std::ostringstream oss;
        oss << "no metric specified for " << TM_QUOTE(Function::dist);
        if (message.length())
          message.append(", ");
        message.append(oss.str());
        TM_LOG_ERR(oss.str());
      }
    } break;

    case DistanceOvRm:
    {
      if (metric & DeltaPhi) objects = &objDeltaPhiOvRm;
      if ((metric & DeltaEta) or (metric & DeltaR)) objects = &objDeltaOvRm;
      if (not objects)
      {
        std::ostringstream oss;
        oss << "no metric specified for " << TM_QUOTE(Function::dist_orm);
        if (message.length())
          message.append(", ");
        message.append(oss.str());
        TM_LOG_ERR(oss.str());
      }
    } break;

    case Combination:
      objects = &objComb;
      break;

    case CombinationOvRm:
      objects = &objCombOvRm;
      break;

    case InvariantMass:
      objects = &objMassInv;
      break;

    case InvariantMassUpt:
      objects = &objMassInvUpt;
      break;

    case InvariantMassDeltaR:
      objects = &objMassInvDeltaR;
      break;

    case InvariantMass3:
      objects = &objMassInv3;
      break;

    case InvariantMassOvRm:
      objects = &objMassInvOvRm;
      break;

    case TransverseMass:
      objects = &objMassTrv;
      break;

    case TransverseMassOvRm:
      objects = &objMassTrvOvRm;
      break;

    default:
      break;

  } // switch (type)

  // TODO: danger ahead!
  if (not objects) return false;

  for (const auto& token: *objects)
  {
    // NOTE: weak compare, reference list should be sorted by string length, descending
    if (boost::algorithm::starts_with(object, token))
    {
      return true;
    }
  }

  return false;
}


bool
Item::isValidCut(const std::string& cut,
                 std::string& message)
{
  const std::vector<std::string>* cuts = 0;

  switch (type)
  {
    case Distance:
      cuts = &cutDist;
      break;

    case DistanceOvRm:
      cuts = &cutDistOvRm;
      break;

    case Combination:
      cuts = &cutComb;
      break;

    case CombinationOvRm:
      cuts = &cutCombOvRm;
      break;

    case InvariantMass:
      cuts = &cutInvMass;
      break;

    case InvariantMassUpt:
      cuts = &cutInvMassUpt;
      break;

    case InvariantMassDeltaR:
      cuts = &cutInvMassDeltaR;
      break;

    case InvariantMass3:
      cuts = &cutInvMass3;
      break;

    case InvariantMassOvRm:
      cuts = &cutInvMassOvRm;
      break;

    case TransverseMass:
      cuts = &cutTrvMass;
      break;

    case TransverseMassOvRm:
      cuts = &cutTrvMassOvRm;
      break;

    default:
      break;
  }

  // TODO: danger ahead!
  if (not cuts)
  {
    std::ostringstream oss;
    oss << "unkown cut type: " << type;
    if (message.length())
      message.append(", ");
    message.append(oss.str());
    TM_LOG_ERR(oss.str());
    return false;
  }

  for (const auto& token: *cuts)
  {
    // NOTE: weak compare, reference list should be sorted by string length, descending
    if (boost::algorithm::starts_with(cut, token))
    {
      // set metric flags
      switch (type)
      {
        case Distance:
        case InvariantMass:
        case InvariantMassUpt:
        case InvariantMassDeltaR:
        case InvariantMass3:
        case TransverseMass:
        case DistanceOvRm:
        case InvariantMassOvRm:
        case TransverseMassOvRm:
        {
          if (token == Cut::DETA)
          {
            metric |= DeltaEta;
          }
          else if (token == Cut::DPHI)
          {
            metric |= DeltaPhi;
          }
          else if (token == Cut::DR)
          {
            metric |= DeltaR;
          }
        } break;
        default:
          break;
      }
      return true;
    }
  }

  std::ostringstream oss;
  oss << "unkown cut: " << TM_QUOTE(cut);
  if (message.length())
    message.append(", ");
  message.append(oss.str());
  TM_LOG_ERR(oss.str());
  return false;
}


FunctionType
Item::getType() const
{
  if (functionTypes.count(name))
    return functionTypes.at(name);

  return Unknown;
}

void
Item::appendMessage(const std::string& message)
{
  if (this->message.length())
    this->message.append(", ");
  this->message.append(message);
}


/** @cond INTERNAL */
bool
getObjects(const std::string& token,
           std::vector<std::string>& objects,
           std::string& message)
{
  TM_LOG_DBG(TM_VALUE_DBG(token));

  std::string text(token);
  while (text.length())
  {
    size_t offset = text.find_first_not_of(",]");
    if (offset > text.length()) break;
    text = text.substr(offset);

    const size_t length = text.length();
    const size_t cut_start = text.find_first_of("[");
    const size_t cut_stop = text.find_first_of("]");
    const size_t comma = text.find_first_of(",");

    // ignore comma in cut specification
    size_t end = (cut_start < comma and comma < cut_stop) ? cut_stop : comma;
    end = (end > length) ? length: end;

    TM_LOG_DBG(TM_VALUE_DBG(length) << ", " << TM_VALUE_DBG(end)
      << " [ = " << cut_start << " ] = " << cut_stop << " , " << comma);

    // include ']' in an object
    if (end == cut_stop) end += 1;

    std::string object = text.substr(0, end);
    TM_LOG_DBG(TM_VALUE_DBG(end) << ", " << TM_VALUE_DBG(object));
    objects.emplace_back(object);
    text = text.substr(end, length-end);
  }

  if (objects.size() < 2)
  {
    std::ostringstream oss;
    oss << "# of object < 2 " << TM_QUOTE(text);
    if (message.length())
      message.append(", ");
    message.append(oss.str());
    TM_LOG_ERR(oss.str());
    return false;
  }

  return true;
}

bool
validateObjectCount(Function::Item& item, const size_t min, const size_t max)
{
  const size_t count = item.objects.size();
  if (count < min or count > max)
  {
    std::ostringstream message;
    message << "invalid object count for function " << TM_QUOTE(item.name) << ": "
            << count << " (" << min << " <= objects <= " << max << ")";
    item.appendMessage(message.str());
    TM_LOG_ERR(message.str());
    return false;
  }
  return true;
}

bool
validateCombinationFunction(Function::Item& item)
{
  // Check number of objects
  if (not validateObjectCount(item, 2, 12))
    return false;

  const size_t count = item.objects.size();
  const int firstType = item.objects.front().getType();

  // Check that all objects are of same type
  const size_t equalCount = std::count_if(item.objects.begin(), item.objects.end(), equalObjectType(firstType));
  if (count != equalCount)
  {
    std::ostringstream message;
    message << "invalid object combination for function "
            << TM_QUOTE(item.name) << ": all objects must be of same type";
    item.appendMessage(message.str());
    TM_LOG_ERR(message.str());
    return false;
  }

  return true;
}

/** Validate overlap removal objects assignment. */
bool
validateOvRmObjects(Function::Item& item)
{
  const size_t count = item.objects.size();
  const size_t countRight = 1;
  const size_t countLeft = count - countRight;
  const int firstType = item.objects.front().getType();
  const int lastType = item.objects.back().getType();

  // Check that first and last object is of different type
  if (firstType == lastType)
  {
    std::ostringstream message;
    message << "invalid object combination for overlap removal function "
            << TM_QUOTE(item.name) << ": right object must be of different type, eg. (tautau + jet)";
    item.appendMessage(message.str());
    TM_LOG_ERR(message.str());
    return false;
  }

  // Check that left objects are of same type
  const size_t equalCount = std::count_if(item.objects.begin(), item.objects.end(), equalObjectType(firstType));
  if (countLeft != equalCount)
  {
    std::ostringstream message;
    message << "invalid object combination for overlap removal function "
            << TM_QUOTE(item.name) << ": left objects must be of same type, eg. (tautau + jet)";
    item.appendMessage(message.str());
    TM_LOG_ERR(message.str());
    return false;
  }

  return true;
}

/** Validate if there is at least one overlap removal cut assigned. */
bool
validateOvRmCuts(Function::Item& item)
{
  for (const auto& cut: item.cuts)
  {
    const std::string type = getCutType(cut);
    if (std::count(cutOvRm.begin(), cutOvRm.end(), type))
      return true; // found at least one ovlerap removal cut
  }

  std::ostringstream message;
  message << "missing overlap removal cut for function " << TM_QUOTE(item.name);
  item.appendMessage(message.str());
  TM_LOG_ERR(message.str());

  return false;
}

/** Validate if there is at least one distance cut assigned. */
bool
validateDistanceCuts(Function::Item& item)
{
  for (const auto& cut: item.cuts)
  {
    const std::string type = getCutType(cut);
    if (Cut::DR == type or Cut::DETA == type or Cut::DPHI == type)
      return true; // found at least one distance cut
  }

  std::ostringstream message;
  message << " missing DR/DETA/DPHI cut for function " << TM_QUOTE(item.name);
  item.appendMessage(message.str());
  TM_LOG_ERR(message.str());

  return false;
}

/** Validate if there is at least one mass cut assigned. */
bool
validateMassCuts(Function::Item& item)
{
  for (const auto& cut: item.cuts)
  {
    const std::string type = getCutType(cut);
    if (Cut::MASS == type)
      return true; // found at least one mass cut
  }

  std::ostringstream message;
  message << "missing MASS cut for function " << TM_QUOTE(item.name);
  item.appendMessage(message.str());
  TM_LOG_ERR(message.str());

  return false;
}

/** Validate if there is at least one mass for unconstrained pt cut assigned. */
bool
validateMassUptCuts(Function::Item& item)
{
  for (const auto& cut: item.cuts)
  {
    const std::string type = getCutType(cut);
    if (Cut::MASSUPT == type)
      return true; // found at least one mass cut
  }

  std::ostringstream message;
  message << "missing MASSUPT cut for function " << TM_QUOTE(item.name);
  item.appendMessage(message.str());
  TM_LOG_ERR(message.str());

  return false;
}

/** Validate if there is at least one mass divided by delta R cut assigned. */
bool
validateMassDeltaRCuts(Function::Item& item)
{
  for (const auto& cut: item.cuts)
  {
    const std::string type = getCutType(cut);
    if (Cut::MASSDR == type)
      return true; // found at least one mass divided by delta R cut
  }

  std::ostringstream message;
  message << "missing MASSDR cut for function " << TM_QUOTE(item.name);
  item.appendMessage(message.str());
  TM_LOG_ERR(message.str());

  return false;
}

/** Validate combination with overlap removal function objects and cuts. */
bool
validateCombinationOvRmFunction(Function::Item& item)
{
  // Check number of objects
  if (not validateObjectCount(item, 2, 5))
    return false;

  // Check overlap removal objects type and order
  if (not validateOvRmObjects(item))
    return false;

  return true;
}

/** Validate distance function objects and cuts. */
bool
validateDistanceFunction(Function::Item& item)
{
  // Check number of objects
  if (not validateObjectCount(item, 2, 2))
    return false;

  return true;
}

/** Validate distance with overlap removal function objects and cuts. */
bool
validateDistanceOvRmFunction(Function::Item& item)
{
  // Check number of objects
  if (not validateObjectCount(item, 2, 3))
    return false;

  // Check overlap removal objects type and order
  if (not validateOvRmObjects(item))
    return false;

  if (not validateOvRmCuts(item))
    return false;

  return true;
}

/** Validate invariant mass function objects and cuts. */
bool
validateInvariantMassFunction(Function::Item& item)
{
  // Check number of objects
  if (not validateObjectCount(item, 2, 2))
    return false;

  if (not validateMassCuts(item))
    return false;

  return true;
}

/** Validate invariant mass for unconstrained pt function objects and cuts. */
bool
validateInvariantMassUptFunction(Function::Item& item)
{
  // Check number of objects
  if (not validateObjectCount(item, 2, 2))
    return false;

  if (not validateMassUptCuts(item))
    return false;

  return true;
}

/** Validate invariant mass function objects and cuts. */
bool
validateInvariantMassDeltaRFunction(Function::Item& item)
{
  // Check number of objects
  if (not validateObjectCount(item, 2, 2))
    return false;

  if (not validateMassDeltaRCuts(item))
    return false;

  return true;
}

/** Validate invariant mass for three particles function objects and cuts. */
bool
validateInvariantMass3Function(Function::Item& item)
{
  // Check number of objects
  if (not validateObjectCount(item, 3, 3))
    return false;

  if (not validateMassCuts(item))
    return false;

  return true;
}

/** Validate invariant mass with overlap removal function objects and cuts. */
bool
validateInvariantMassOvRmFunction(Function::Item& item)
{
  // Check number of objects
  if (not validateObjectCount(item, 2, 3))
    return false;

  // Check overlap removal objects type and order
  if (not validateOvRmObjects(item))
    return false;

  if (not validateMassCuts(item))
    return false;

  if (not validateOvRmCuts(item))
    return false;

  return true;
}

/** Validate transverse mass function objects and cuts. */
bool
validateTransverseMassFunction(Function::Item& item)
{
  // Check number of objects
  if (not validateObjectCount(item, 2, 2))
    return false;

  if (not validateMassCuts(item))
    return false;

  return true;
}

/** Validate transverse mass with overlap removal function objects and cuts. */
bool
validateTransverseMassOvRmFunction(Function::Item& item)
{
  // Check number of objects
  if (not validateObjectCount(item, 2, 3))
    return false;

  // Check overlap removal objects type and order
  if (not validateOvRmObjects(item))
    return false;

  if (not validateMassCuts(item))
    return false;

  if (not validateOvRmCuts(item))
    return false;

  return true;
}
/** @endcond */


bool
parser(const std::string& function,
       Function::Item& item)
{
  const std::string trimmed = boost::trim_copy(function);
  // Build regular expression list of function names.
  std::ostringstream names;
  names << "(";
  reserved::const_iterator cit;
  for (cit = functionName.begin(); cit != functionName.end(); ++cit)
  {
    if (cit != functionName.begin())
      names << "|";
    names << cit->first;
  }
  names << ")";
  const std::string name = names.str();
  const std::string objects = "\\{(.+)\\}";
  const std::string cuts = "\\[(.+)\\]";
  regex_t regex;

  // try func(obj)[cut] format
  std::string expression = name + objects + cuts;
  if (tmutil::regex_compile(&regex, expression))
  {
    std::ostringstream message;
    message << "tmutil::regex_compile::error '" << expression << "'";
    item.appendMessage(message.str());
    return false;
  }

  std::vector<std::string> matched;
  tmutil::regex_match(&regex, trimmed, matched);
  regfree(&regex);

  Function::Item_ item_;

  // if matched function with cuts
  if (matched.size() == 3)
  {
    item_.name = matched.at(0);
    item_.cuts = matched.at(2);
    if (not getObjects(matched.at(1), item_.objects, item.message))
      return false;
  }
  // else try to match function without cuts
  else
  {
    // try func(obj) format
    expression = name + objects;
    if (tmutil::regex_compile(&regex, expression))
    {
      std::ostringstream message;
      message << "tmutil::regex_compile::error '" << expression << "'";
      item.appendMessage(message.str());
      return false;
    }

    matched.clear();
    tmutil::regex_match(&regex, trimmed, matched);
    regfree(&regex);

    if (matched.size() == 2)
    {
      item_.name = matched.at(0);
      if (not getObjects(matched.at(1), item_.objects, item.message))
        return false;
    }
  }

  item.name = item_.name;
  item.type = item.getType();

  // cut is mandatory except for Combination
  if ((item.type != Combination) and item_.cuts.empty())
  {
    std::ostringstream message;
    message << "no cut specified for " << TM_QUOTE(item.name);
    item.appendMessage(message.str());
    TM_LOG_ERR(message.str());
    return false;
  }

  // decode cuts
  if (item_.cuts.size())
  {
    Cut::Item cut;
    if (not Cut::parser(item_.cuts, cut))
    {
      item.appendMessage(cut.message);
      return false;
    }

    // populate function cuts
    for (const auto& token: cut.name)
    {
      if (not item.isValidCut(token, item.message))
      {
        std::ostringstream message;
        message << "cut " << TM_QUOTE(token)
                << " is not valid for function " << TM_QUOTE(item.name);
        item.appendMessage(message.str());
        TM_LOG_ERR(message.str());
        return false;
      }
      item.cuts.emplace_back(token);
    }

    // test for mandatory function cuts
    // for (const auto& token: item.cuts)
    // {
    // TODO: check allowed cuts for function
    // }
  }

  // decode objects
  for (const auto& token: item_.objects)
  {
    Object::Item object;
    if (not Object::parser(token, object))
    {
      item.appendMessage(object.message);
      return false;
    }

    if (not item.isValidObject(token, item.message))
    {
      std::ostringstream message;
      message << "object " << TM_QUOTE(token)
              << " is not valid for function " << TM_QUOTE(item.name)
              << ((item.type == Distance) ? " with the given metric" : "");
      item.appendMessage(message.str());
      TM_LOG_ERR(message.str());
      return false;
    }

    item.objects.emplace_back(object);
  }

  // Validate function integrity
  switch (item.type)
  {
    case Combination:
      return validateCombinationFunction(item);

    case CombinationOvRm:
      return validateCombinationOvRmFunction(item);

    case Distance:
      return validateDistanceFunction(item);

    case DistanceOvRm:
      return validateDistanceOvRmFunction(item);

    case InvariantMass:
      return validateInvariantMassFunction(item);

    case InvariantMassUpt:
      return validateInvariantMassUptFunction(item);

    case InvariantMassDeltaR:
      return validateInvariantMassDeltaRFunction(item);

    case InvariantMass3:
      return validateInvariantMass3Function(item);

    case InvariantMassOvRm:
      return validateInvariantMassOvRmFunction(item);

    case TransverseMass:
      return validateTransverseMassFunction(item);

    case TransverseMassOvRm:
      return validateTransverseMassOvRmFunction(item);

    default:
      break;
  }

  return false;
}


std::vector<std::string>
getCuts(const Item& item)
{
  std::vector<std::string> names;
  for (const auto& cut: item.cuts)
  {
    names.emplace_back(cut);
  }
  return names;
}


std::vector<std::string>
getObjects(const Item& item)
{
  std::vector<std::string> names;
  for (const auto& object: item.objects)
  {
    names.emplace_back(object.getObjectName());
  }
  return names;
}


std::vector<std::string>
getObjectCuts(const Item& item)
{
  std::vector<std::string> cuts;
  for (const auto& object: item.objects)
  {
    cuts.emplace_back(boost::algorithm::join(object.cuts, ","));
  }
  return cuts;
}


bool
isFunction(const std::string& element)
{
  for (const auto& item: functionName)
  {
    // TODO: unsorted weak name check!
    if (boost::algorithm::starts_with(element, item.first))
    {
      Item function;
      if (parser(element, function))
        return true;
    }
  }

  return false;
}

} // namespace Function
