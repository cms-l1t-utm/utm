#include "tmUtil/tmUtil.hh"
#include "tmXsd/keywords.hh"

// boost
#include <boost/lexical_cast.hpp>

// stl
#include <sstream>
#include <stdexcept>

namespace tmxsd
{

const std::map<std::string, bool> binKey{
  {"number", true},
  {"minimum", true},
  {"maximum", true},
  {"bin_id", false},
  {"scale_id", false},
};


const std::map<std::string, bool> scaleKey{
  {"object", true},
  {"type", true},
  {"minimum", true},
  {"maximum", true},
  {"step", true},
  {"n_bits", true},
  {"comment", false},
  {"scale_id", false},
  {"datetime", false},
  {"n_bins", false},
};


const std::map<std::string, bool> scaleSetKey{
  {"name", true},
  {"comment", false},
  {"scale_set_id", false},
  {"datetime", false},
  {"is_valid", false},
};


const std::map<std::string, bool> extSignalKey{
  {"name", true},
  {"system", true},
  {"cable", true},
  {"channel", true},
  {"description", false},
  {"label", false},
  {"ext_signal_id", false},
  {"datetime", false},
};


const std::map<std::string, bool> extSignalSetKey{
  {"name", true},
  {"comment", false},
  {"ext_signal_set_id", false},
  {"datetime", false},
  {"is_valid", false},
};


const std::map<std::string, bool> objectRequirementKey{
  {"name", true},
  {"type", true},
  {"comparison_operator", true},
  {"threshold", true},
  {"bx_offset", true},
  {"comment", false},
  {"requirement_id", false},
  {"datetime", false},
};

const std::map<std::string, bool> externalRequirementKey{
  {"name", true},
  {"bx_offset", true},
  {"comment", false},
  {"requirement_id", false},
  {"ext_signal_id", false},
  {"datetime", false},
};


const std::map<std::string, bool> cutKey{
  {"name", true},
  {"object", true},
  {"type", true},
  {"minimum", true},
  {"maximum", true},
  {"data", true},
  {"comment", false},
  {"cut_id", false},
  {"datetime", false},
};


const std::map<std::string, bool> algorithmKey{
  {"name", true},
  {"expression", true},
  {"index", true},
  {"module_id", true},
  {"module_index", true},
  {"comment", false},
  {"labels", false},
  {"algorithm_id", false},
  {"datetime", false},
};


const std::map<std::string, bool> menuKey{
  {"ancestor_id", true},
  {"name", true},
  {"uuid_menu", true},
  {"uuid_firmware", true},
  {"global_tag", true},
  {"grammar_version", true},
  {"n_modules", true},
  {"is_valid", true},
  {"is_obsolete", true},
  {"comment", false},
  {"menu_id", false},
  {"datetime", false},
};

} // namespace tmxsd


// ---------------------------------------------------------------------
// methods
// ---------------------------------------------------------------------
template<typename T>
const T tmxsd::getValue(const std::map<std::string, T>&map,
                        const std::string& key)
{
  const auto cit = map.find(key);
  if (cit == std::end(map))
  {
    std::ostringstream message;
    message << "err> tmxsd::getValue() key '" << key << "' not found";
    throw std::runtime_error(message.str());
  }

  return cit->second;
}


template<typename To, typename From>
const To tmxsd::getValue(const std::map<std::string, From>&map,
                         const std::string& key)
{
  const auto cit = map.find(key);
  if (cit == std::end(map))
  {
    std::ostringstream message;
    message << "err> tmxsd::getValue() key '" << key << "' not found";
    throw std::runtime_error(message.str());
  }

  return boost::lexical_cast<To>(cit->second);
}


bool
tmxsd::validateKeys(const std::map<std::string, bool> keys,
                    const std::map<std::string, std::string> map)
{
  for (std::map<std::string, bool>::const_iterator cit = keys.begin();
       cit != keys.end(); cit++)
  {
    if (not cit->second) continue;

    try
    {
      getValue(map, cit->first);
    }
    catch (std::runtime_error& e)
    {
      TM_LOG_ERR("required value is missing: " << cit->first);
      return false;
    }
  }


  for (std::map<std::string, std::string>::const_iterator cit = map.begin();
       cit != map.end(); cit++)
  {
    try
    {
      getValue(keys, cit->first);
    }
    catch (std::runtime_error& e)
    {
      TM_LOG_ERR("unknown value is given: " << cit->first);
      return false;
    }
  }
  return true;
}


std::string
tmxsd::getKeyForBin(const tmtable::Row& scale)
{
  const auto object = tmxsd::getValue(scale, "object");
  const auto type = tmxsd::getValue(scale, "type");
  std::ostringstream key;
  key << object << "-" << type;
  return key.str();
}


// ---------------------------------------------------------------------
// template instantiation
// ---------------------------------------------------------------------
template const int
tmxsd::getValue<int, std::string>(const std::map<std::string, std::string>& map, const std::string& key);

template const unsigned int
tmxsd::getValue<unsigned int, std::string>(const std::map<std::string, std::string>& map, const std::string& key);

template const bool
tmxsd::getValue<bool, std::string>(const std::map<std::string, std::string>& map, const std::string& key);

/* eof */
