.SUFFIXES: .o
.PHONY: all test install clean dist-clean
.PHONY: _all _test _install _clean _dist-clean

_OBJS = $(SRCS:.cc=.o)
_DEPS = $(SRCS:.cc=.d)

all: $(LIBRARY) $(PROGRAMS) _all

$(LIBRARY): $(_OBJS)
	$(AR) -o $@ $^ $(LIBDIR) $(LIBS)

$(PROGRAMS): %: %.o
	$(CXX) -o $@ $(CXXFLAGS) $(CPPFLAGS) $(LIBDIR) $< $(LIBS)

%.o : %.cc
	$(CXX) -c -o $@ $(CXXFLAGS) $(CPPFLAGS) -MMD -MP $(INCDIR) $<

test:
	@if [ -f test/Makefile ]; then $(MAKE) -C test _test; fi

_test: $(PROGRAMS)
	@if [ -f unittest ]; then ./unittest; fi

install: $(LIBRARY) _install
	mkdir -p $(PREFIX)/lib
	cp -p $(LIBRARY) $(PREFIX)/lib
	mkdir -p $(PREFIX)/include/$(PACKAGE)
	cp -p $(HDRS) $(PREFIX)/include/$(PACKAGE)

clean: _clean
	$(RM) $(GARBAGE) $(LIBRARY) $(PROGRAMS)
	@if [ -f test/Makefile ]; then $(MAKE) -C test $@; fi

dist-clean: clean _dist-clean
	@if [ -f test/Makefile ]; then $(MAKE) -C test $@; fi

# dependencies
-include $(_DEPS)
