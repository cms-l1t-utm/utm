# targets
LIBRARY =
PROGRAMS =

SRCS =
HDRS =
INCDIR =
LIBDIR =
LIBS =

CXXFLAGS = -std=c++11 -fPIC -O2 -Wall # -g -pg -Wall -pedantic
CPPFLAGS = -DNDEBUG

# commands
SHELL = /bin/bash
CXX = c++
CPP = c++ -E
AR = c++ -shared -fPIC
MKDEPEND = c++ -MMD -MP
RM = rm -f
TOUCH = touch

# temp
GARBAGE = core* *.o *~ *.d *.log a.out gmon.out

.default: all
