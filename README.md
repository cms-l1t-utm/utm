Contents
========

* [Overview](#overview)
* [Build](#build)
* [Build for XDAQ](#build-for-xdaq)
* [Build for CMSSW](#build-for-cmssw)

Overview
========

Core components of the utm libraries are:

* tmUtil:          common utility library
* tmXsd:           XSD binding for the CMS L1 Menu XML
* tmTable:         C++ classes for the CMS L1 Menu data
* tmGrammar:       parser for the CMS L1 Menu Grammar
* tmEventSetup:    CondFormat compatible data classes for the CMS L1 Menu

Check the README.md in the subdirectories for further information.

Build
=====

## Prerequisites

* CodeSynthesis XSD (http://www.codesynthesis.com/products/xsd/)
* BOOST (https://www.boost.org/)
* Xerces library (http://xerces.apache.org/xerces-c/)

Check the corresponding Web pages for the installation of the packages
as well as the following twiki page;
https://twiki.cern.ch/twiki/bin/view/CMS/GlobalTriggerUpgradeL1T-uTme#uGT_Trigger_Menu_library_utm

### Dependencies

Redhat/CC7:

    $ sudo yum install make gcc-c++ boost-system boost-filesystem boost-devel \
      xsd xerces-c-devel which

Debian/Ubuntu:

    $ sudo apt-get install build-essential libboost-dev libboost-system-dev \
      libboost-filesystem-dev xsdcxx libxerces-c-dev

### Generate XML bindings

Pre-generated XML bindings and XSDC++ headers (`xsd/cxx/`) are bundled with the
projects source code.

The bindings can be manually generated use the following commands (eg. for c++11
compatibility or when using XSDC++ >= 4.0.0)

    $ ./configure
    $ make dist-clean
    $ make genxsd

## Compilation

Building C++ libraries

    $ ./configure
    $ make all -j8

## Install

Install libraries, headers and XSD files, use `PREFIX` to assign a custom
location.

    $ make install PREFIX=/home/user/local

Build for XDAQ15
================

To use the library in the XDAQ/SWATCH environment.

    $ git clone https://gitlab.cern.ch/cms-l1t-utm/utm.git
    $ cd utm
    $ git checkout <tag/branch>

## Compile

    $ ./configure xdaq
    $ make all

## Build RPMs

    $ make rpm

## Install RPMs

    $ sudo yum install $(find tm* -name '*.rpm')

Build for XDAQ15 using Docker
=============================

    $ docker build -f Dockerfile.xdaq -t utm_xdaq:<tag/branch> .

Build for CMSSW
===============

Example of a manual installation as an external library of CMSSW and test of the
installation.

## Compilation

```{r, engine='bash', count_lines}
cmsrel CMSSW_13_0_0_pre4
cd CMSSW_13_0_0_pre4/src
cmsenv
export $(scram tool info xerces-c | grep XERCES_C_BASE=)
echo "XERCES_C_BASE=$XERCES_C_BASE"
export $(scram tool info boost | grep BOOST_BASE=)
echo "BOOST_BASE=$BOOST_BASE"
cd $CMSSW_BASE
git clone https://gitlab.cern.ch/cms-l1t-utm/utm.git
cd utm
git checkout <tag/branch>
./configure
make all -j8
make install PREFIX=.
```

## Setup

```{r, engine='bash', count_lines}
cd $CMSSW_BASE/src
# Now edit ../config/toolbox/${SCRAM_ARCH}/tools/selected/utm.xml and change
# UTM_BASE to the directory pointed to by the output of "echo $CMSSW_BASE/utm"
# or according to install PREFIX used above.
git cms-addpkg L1Trigger/L1TGlobal ## for example
scram setup utm
scram b -j8
```

API documentation
=================

To generate API documentation for tmEventSetup, run

    $ doxygen doxygen.config
