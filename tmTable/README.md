Contents
========

* test:      unit tests, test programs


Test programs
=============

Building test programs

    cd test/
    make

Make sure to setup local environment

    . ../../env.sh
