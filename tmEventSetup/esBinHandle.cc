#include "tmEventSetup/esBinHandle.hh"

#include "tmTable/tmTable.hh"

#include <boost/lexical_cast.hpp>

namespace tmeventsetup
{

// keys for tables
const std::string kNumber{"number"};
const std::string kMinimum{"minimum"};
const std::string kMaximum{"maximum"};


esBinHandle::esBinHandle(const tmtable::Row& bin)
{
  hw_index = boost::lexical_cast<unsigned int>(tmtable::get(bin, kNumber));
  minimum = boost::lexical_cast<double>(tmtable::get(bin, kMinimum));
  maximum = boost::lexical_cast<double>(tmtable::get(bin, kMaximum));
}

} // namespace tmeventsetup
