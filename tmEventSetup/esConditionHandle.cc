#include "tmEventSetup/esConditionHandle.hh"

#include "tmEventSetup/esCutHandle.hh"
#include "tmEventSetup/esObjectHandle.hh"

#include "tmUtil/tmUtil.hh"

namespace tmeventsetup
{

void
esConditionHandle::addObject(const esObject& object)
{
  objects_.emplace_back(object);
}


void
esConditionHandle::addCut(const esCut& cut)
{
  cuts_.emplace_back(cut);
}

} // namespace tmeventsetup
