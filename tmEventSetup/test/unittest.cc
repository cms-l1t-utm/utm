#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE tmEventSetup

#include "tmEventSetup/tmEventSetup.hh"

#include "tmEventSetup/esTriggerMenuHandle.hh"
#include "tmEventSetup/esTriggerMenu.hh"

#include "tmEventSetup/esAlgorithmHandle.hh"
#include "tmEventSetup/esAlgorithm.hh"

#include "tmEventSetup/esConditionHandle.hh"
#include "tmEventSetup/esCondition.hh"

#include "tmEventSetup/esObjectHandle.hh"
#include "tmEventSetup/esObject.hh"

#include "tmEventSetup/esCutHandle.hh"
#include "tmEventSetup/esCut.hh"

#include "tmEventSetup/esScaleHandle.hh"
#include "tmEventSetup/esScale.hh"

#include "tmUtil/tmUtil.hh"

#include <boost/test/unit_test.hpp>

#include <algorithm>
#include <string>
#include <vector>

#include <cmath>

namespace tmeventsetup
{
  // esAlgorithmHandle.cc
  extern size_t replaceInExpression(std::string&, const std::string&, const std::string&);

  // esTriggerMenuHandle.cc
  extern bool isThresholdType(esCutType);
  extern bool isWindowType(esCutType);

  // tmEventSetup.cc
  extern long long toFixedPoint(double, const size_t);
}

BOOST_AUTO_TEST_SUITE(esAlgorithm)

BOOST_AUTO_TEST_CASE(esAlgorithmHandle_replaceInExpression)
{
  using tmeventsetup::replaceInExpression;

  std::string expression{"MU0 OR (comb{MU4,MU2}[CHGCOR_OS] AND (MU0 OR JET0[JET-ETA_X]))"};

  BOOST_CHECK_EQUAL(0, replaceInExpression(expression, "MU", "error"));
  BOOST_CHECK_EQUAL(2, replaceInExpression(expression, "MU0", "SingleMuon_i0"));
  BOOST_CHECK_EQUAL(1, replaceInExpression(expression, "JET0[JET-ETA_X]", "SingleJet_i1"));
  BOOST_CHECK_EQUAL(1, replaceInExpression(expression, "comb{MU4,MU2}[CHGCOR_OS]", "DoubleMuon_i2"));

  BOOST_CHECK_EQUAL("SingleMuon_i0 OR (DoubleMuon_i2 AND (SingleMuon_i0 OR SingleJet_i1))", expression);
}

BOOST_AUTO_TEST_CASE(esAlgorithmHandle_init)
{
  using tmeventsetup::esAlgorithmHandle;
  using tmeventsetup::esAlgorithm;

  const tmtable::Row row{
    {"name", "L1_Unnamed"},
    {"index", "42"},
    {"module_id", "0"},
    {"module_index" ,"1"},
    {"expression", "mass_inv{MU4,MU2}[MASS_Z] OR TAU42"}
  };

  esAlgorithmHandle handle{row};

  esAlgorithm& algorithm = handle;

  BOOST_CHECK_EQUAL("L1_Unnamed", algorithm.getName());
  BOOST_CHECK_EQUAL(42, algorithm.getIndex());
  BOOST_CHECK_EQUAL(0, algorithm.getModuleId());
  BOOST_CHECK_EQUAL(1, algorithm.getModuleIndex());
  BOOST_CHECK_EQUAL("mass_inv{MU4,MU2}[MASS_Z] OR TAU42", algorithm.getExpression());
  BOOST_CHECK_EQUAL("", algorithm.getExpressionInCondition());
}

BOOST_AUTO_TEST_CASE(esAlgorithmHandle_setRpnVector)
{
  using tmeventsetup::esAlgorithmHandle;
  using tmeventsetup::esAlgorithm;

  const tmtable::Row row{
    {"name", "L1_setRpnVector"},
    {"index", "42"},
    {"module_id", "0"},
    {"module_index", "1"},
    {"expression", "MU7 AND TAU3"}
  };

  const std::map<std::string, std::string> conditionMap{
    {"MU7", "SingleMu_4"},
    {"TAU3", "SingleTau_5"}
  };

  esAlgorithmHandle handle{row};
  handle.setExpressionInCondition(conditionMap);

  esAlgorithm& algorithm = handle;

  BOOST_CHECK_EQUAL("L1_setRpnVector", algorithm.getName());
  BOOST_CHECK_EQUAL(42, algorithm.getIndex());
  BOOST_CHECK_EQUAL(0, algorithm.getModuleId());
  BOOST_CHECK_EQUAL(1, algorithm.getModuleIndex());
  BOOST_CHECK_EQUAL("MU7 AND TAU3", algorithm.getExpression());
  BOOST_CHECK_EQUAL("SingleMu_4 AND SingleTau_5", algorithm.getExpressionInCondition());
}

BOOST_AUTO_TEST_CASE(esAlgorithmHandle_setExpressionInCondition)
{
  using tmeventsetup::esAlgorithmHandle;
  using tmeventsetup::esAlgorithm;
  using tmeventsetup::esTriggerMenuHandle;

  const tmtable::Row row{
    {"name", "L1_setExpressionInCondition"},
    {"index", "42"},
    {"module_id", "0"},
    {"module_index", "1"},
    {"expression", "mass_inv{MU4,MU2}[MASS_Z] OR TAU42 AND EXT_SIG_0"}
  };

  const tmtable::Row conditionMap{
    {"mass_inv{MU4,MU2}[MASS_Z]", "mass_123"},
    {"TAU42", "single_tau_124"},
    {"EXT_SIG_0", "ext_signal_125"}
  };

  esAlgorithmHandle handle{row};
  handle.setExpressionInCondition(conditionMap);

  esTriggerMenuHandle triggerMenuHandle;
  handle.setRpnVector(triggerMenuHandle.parseExpression(handle.getExpressionInCondition()));

  esAlgorithm& algorithm = handle;

  BOOST_CHECK_EQUAL("L1_setExpressionInCondition", algorithm.getName());
  BOOST_CHECK_EQUAL(42, algorithm.getIndex());
  BOOST_CHECK_EQUAL(0, algorithm.getModuleId());
  BOOST_CHECK_EQUAL(1, algorithm.getModuleIndex());
  BOOST_CHECK_EQUAL("mass_inv{MU4,MU2}[MASS_Z] OR TAU42 AND EXT_SIG_0", algorithm.getExpression());
  BOOST_CHECK_EQUAL("mass_123 OR single_tau_124 AND ext_signal_125", algorithm.getExpressionInCondition());

  const std::vector<std::string> rpn_vector_ref{
    "mass_123",
    "single_tau_124",
    "OR",
    "ext_signal_125",
    "AND"
  };
  const auto rpn_vector = algorithm.getRpnVector();

  BOOST_CHECK_EQUAL_COLLECTIONS(rpn_vector.begin(), rpn_vector.end(),
                                rpn_vector_ref.begin(), rpn_vector_ref.end());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(esCut)

BOOST_AUTO_TEST_CASE(esCutHandle_init)
{
  using tmeventsetup::esCutHandle;
  using tmeventsetup::esCut;

  const tmtable::Row row{
    {"name", "MU-QLTY_OPEN"},
    {"object", "MU"},
    {"type", "MU-QLTY"}, // Note: type is overwritten in esObjectHandle!
    {"minimum" ,"+0.0000000000000000E+00"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", ""}
  };

  esCutHandle handle{row};

  esCut& cut = handle;

  BOOST_CHECK_EQUAL("MU-QLTY_OPEN", cut.getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Muon, cut.getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::Quality, cut.getCutType());
  BOOST_CHECK_EQUAL(0, cut.getMinimumValue());
  BOOST_CHECK_EQUAL(0, cut.getMaximumValue());
  BOOST_CHECK_EQUAL("0", cut.getData());
  BOOST_CHECK_EQUAL("MU-QLTY", cut.getKey());
  BOOST_CHECK_EQUAL(std::numeric_limits<unsigned int>::max(), cut.getPrecision());
}

BOOST_AUTO_TEST_CASE(esCutHandle_init_jet_disp)
{
  using tmeventsetup::esCutHandle;
  using tmeventsetup::esCut;

  const tmtable::Row row{
    {"name", "JET-DISP_LLP"},
    {"object", "JET"},
    {"type", "JET-DISP"}, // Note: type is overwritten in esObjectHandle!
    {"minimum" ,"+0.0000000000000000E+00"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", "1"}
  };

  esCutHandle handle{row};

  esCut& cut = handle;

  BOOST_CHECK_EQUAL("JET-DISP_LLP", cut.getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Jet, cut.getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::Displaced, cut.getCutType());
  BOOST_CHECK_EQUAL(0, cut.getMinimumValue());
  BOOST_CHECK_EQUAL(0, cut.getMaximumValue());
  BOOST_CHECK_EQUAL("1", cut.getData());
  BOOST_CHECK_EQUAL("JET-DISP", cut.getKey());
  BOOST_CHECK_EQUAL(std::numeric_limits<unsigned int>::max(), cut.getPrecision());
}

BOOST_AUTO_TEST_CASE(esCutHandle_init_adt_ascore)
{
  const tmtable::Row row{
    {"name", "ADT-ASCORE_40k"},
    {"object", "ADT"},
    {"type", "ADT-ASCORE"},
    {"minimum" ,"+4.0000000000000000E+04"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", ""}
  };
  tmeventsetup::esCutHandle handle(row);
  tmeventsetup::esCut* cut = dynamic_cast<tmeventsetup::esCut*>(&handle);
  BOOST_CHECK_EQUAL("ADT-ASCORE_40k", cut->getName());
  BOOST_CHECK_EQUAL(tmeventsetup::ADT, cut->getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::AnomalyScore, cut->getCutType());
  BOOST_CHECK_EQUAL(4e4, cut->getMinimumValue());
  BOOST_CHECK_EQUAL(0, cut->getMaximumValue());
  BOOST_CHECK_EQUAL("", cut->getData());
  BOOST_CHECK_EQUAL("ADT-ASCORE", cut->getKey());
  BOOST_CHECK_EQUAL(0, cut->getPrecision());
}

BOOST_AUTO_TEST_CASE(esCutHandle_init_axo_score)
{
  const tmtable::Row row{
    {"name", "AXO-SCORE_40k"},
    {"object", "AXO"},
    {"type", "AXO-SCORE"},
    {"minimum" ,"+4.0000000000000000E+04"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", ""}
  };

  tmeventsetup::esCutHandle handle(row);

  tmeventsetup::esCut* cut = dynamic_cast<tmeventsetup::esCut*>(&handle);

  BOOST_CHECK_EQUAL("AXO-SCORE_40k", cut->getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Axol1tl, cut->getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::Score, cut->getCutType());
  BOOST_CHECK_EQUAL(4e4, cut->getMinimumValue());
  BOOST_CHECK_EQUAL(0, cut->getMaximumValue());
  BOOST_CHECK_EQUAL("", cut->getData());
  BOOST_CHECK_EQUAL("AXO-SCORE", cut->getKey());
  BOOST_CHECK_EQUAL(0, cut->getPrecision());
}

BOOST_AUTO_TEST_CASE(esCutHandle_init_axo_model)
{
  const tmtable::Row row{
    {"name", "AXO-MODEL_version1"},
    {"object", "AXO"},
    {"type", "AXO-MODEL"},
    {"minimum" ,"+0.0000000000000000E+00"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", "v1"}
  };

  tmeventsetup::esCutHandle handle(row);

  tmeventsetup::esCut* cut = dynamic_cast<tmeventsetup::esCut*>(&handle);

  BOOST_CHECK_EQUAL("AXO-MODEL_version1", cut->getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Axol1tl, cut->getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::Model, cut->getCutType());
  BOOST_CHECK_EQUAL(0, cut->getMinimumValue());
  BOOST_CHECK_EQUAL(0, cut->getMaximumValue());
  BOOST_CHECK_EQUAL("v1", cut->getData());
  BOOST_CHECK_EQUAL("AXO-MODEL", cut->getKey());
  BOOST_CHECK_EQUAL(0, cut->getPrecision());
}

BOOST_AUTO_TEST_CASE(esCutHandle_init_topo_score)
{
  const tmtable::Row row{
    {"name", "TOPO-SCORE_25"},
    {"object", "TOPO"},
    {"type", "TOPO-SCORE"},
    {"minimum" ,"+2.5000000000000000E+01"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", ""}
  };

  tmeventsetup::esCutHandle handle(row);

  tmeventsetup::esCut* cut = dynamic_cast<tmeventsetup::esCut*>(&handle);

  BOOST_CHECK_EQUAL("TOPO-SCORE_25", cut->getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Topological, cut->getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::Score, cut->getCutType());
  BOOST_CHECK_EQUAL(2.5e1, cut->getMinimumValue());
  BOOST_CHECK_EQUAL(0, cut->getMaximumValue());
  BOOST_CHECK_EQUAL("", cut->getData());
  BOOST_CHECK_EQUAL("TOPO-SCORE", cut->getKey());
  BOOST_CHECK_EQUAL(0, cut->getPrecision());
}

BOOST_AUTO_TEST_CASE(esCutHandle_init_topo_model)
{
  const tmtable::Row row{
    {"name", "TOPO-MODEL_had"},
    {"object", "TOPO"},
    {"type", "TOPO-MODEL"},
    {"minimum" ,"+0.0000000000000000E+00"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", "hh_had_v1"}
  };

  tmeventsetup::esCutHandle handle(row);

  tmeventsetup::esCut* cut = dynamic_cast<tmeventsetup::esCut*>(&handle);

  BOOST_CHECK_EQUAL("TOPO-MODEL_had", cut->getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Topological, cut->getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::Model, cut->getCutType());
  BOOST_CHECK_EQUAL(0, cut->getMinimumValue());
  BOOST_CHECK_EQUAL(0, cut->getMaximumValue());
  BOOST_CHECK_EQUAL("hh_had_v1", cut->getData());
  BOOST_CHECK_EQUAL("TOPO-MODEL", cut->getKey());
  BOOST_CHECK_EQUAL(0, cut->getPrecision());
}

BOOST_AUTO_TEST_CASE(esCutHandle_init_cicada_cscore)
{
  const tmtable::Row row{
    {"name", "CICADA-CSCORE_4p273"},
    {"object", "CICADA"},
    {"type", "CICADA-CSCORE"},
    {"minimum" ,"+4.2734375000000000E+00"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", ""}
  };

  tmeventsetup::esCutHandle handle(row);

  tmeventsetup::esCut* cut = dynamic_cast<tmeventsetup::esCut*>(&handle);

  BOOST_CHECK_EQUAL("CICADA-CSCORE_4p273", cut->getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Cicada, cut->getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::CicadaScore, cut->getCutType());
  BOOST_CHECK_EQUAL(4.2734375e0, cut->getMinimumValue());
  BOOST_CHECK_EQUAL(0, cut->getMaximumValue());
  BOOST_CHECK_EQUAL("", cut->getData());
  BOOST_CHECK_EQUAL("CICADA-CSCORE", cut->getKey());
  BOOST_CHECK_EQUAL(0, cut->getPrecision());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(esBin)

BOOST_AUTO_TEST_CASE(esBin_init)
{
  tmeventsetup::esBin bin{42, -1.0, +2.0};

  BOOST_CHECK_EQUAL(42, bin.hw_index);
  BOOST_CHECK_EQUAL(-1.0, bin.minimum);
  BOOST_CHECK_EQUAL(+2.0, bin.maximum);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(esScale)

BOOST_AUTO_TEST_CASE(esScale_init)
{
  std::vector<tmeventsetup::esBin> bins{
    {0, 0.0, 1.0},
    {1, 1.0, 2.0},
    {2, 2.0, 3.0},
  };

  tmeventsetup::esScale scale{
    "JET-ET",
    tmeventsetup::Jet,
    tmeventsetup::EtScale,
    0.0,
    3.0,
    1.0,
    7,
    bins
  };

  BOOST_CHECK_EQUAL("JET-ET", scale.getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Jet, scale.getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::EtScale, scale.getScaleType());
  BOOST_CHECK_EQUAL(0.0, scale.getMinimum());
  BOOST_CHECK_EQUAL(3.0, scale.getMaximum());
  BOOST_CHECK_EQUAL(1.0, scale.getStep());
  BOOST_CHECK_EQUAL(7, scale.getNbits());
  BOOST_CHECK_EQUAL(3, scale.getBins().size());
  BOOST_CHECK_EQUAL(2, scale.getBins().at(2).hw_index);
  BOOST_CHECK_EQUAL(2.0, scale.getBins().at(2).minimum);
  BOOST_CHECK_EQUAL(3.0, scale.getBins().at(2).maximum);
}

BOOST_AUTO_TEST_CASE(esScaleHandle_et)
{
  using tmeventsetup::esScaleHandle;
  using tmeventsetup::esScale;

  const tmtable::Row row{
    {"object", "JET"},
    {"type", "ET"},
    {"minimum", "+0.0000000000000000E+00"},
    {"maximum" ,"+127.0000000000000000E+00"},
    {"step", "+1.0000000000000000E+00"},
    {"n_bits", "7"}
  };

  esScaleHandle handle(row);

  esScale& scale = handle;

  BOOST_CHECK_EQUAL("JET-ET", scale.getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Jet, scale.getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::EtScale, scale.getScaleType());
  BOOST_CHECK_EQUAL(0.0, scale.getMinimum());
  BOOST_CHECK_EQUAL(127.0, scale.getMaximum());
  BOOST_CHECK_EQUAL(1.0, scale.getStep());
  BOOST_CHECK_EQUAL(7, scale.getNbits());
}

BOOST_AUTO_TEST_CASE(esScaleHandle_upt)
{
  using tmeventsetup::esScaleHandle;
  using tmeventsetup::esScale;

  const tmtable::Row row{
    {"object", "MU"},
    {"type", "UPT"},
    {"minimum", "+0.0000000000000000E+00"},
    {"maximum" ,"+255.0000000000000000E+00"},
    {"step", "+1.0000000000000000E+00"},
    {"n_bits", "8"}
  };

  esScaleHandle handle(row);

  esScale& scale = handle;

  BOOST_CHECK_EQUAL("MU-UPT", scale.getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Muon, scale.getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::UnconstrainedPtScale, scale.getScaleType());
  BOOST_CHECK_EQUAL(0.0, scale.getMinimum());
  BOOST_CHECK_EQUAL(255.0, scale.getMaximum());
  BOOST_CHECK_EQUAL(1.0, scale.getStep());
  BOOST_CHECK_EQUAL(8, scale.getNbits());
}

BOOST_AUTO_TEST_CASE(esScaleHandle_index)
{
  const tmtable::Row row{
    {"object", "MU"},
    {"type", "INDEX"},
    {"minimum", "+0.0000000000000000E+00"},
    {"maximum" ,"+1.2700000000000000E+02"},
    {"step", "+1.0000000000000000E+00"},
    {"n_bits", "7"}
  };

  tmeventsetup::esScaleHandle handle(row);

  tmeventsetup::esScale* scale = dynamic_cast<tmeventsetup::esScale*>(&handle);

  BOOST_CHECK_EQUAL("MU-INDEX", scale->getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Muon, scale->getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::IndexScale, scale->getScaleType());
  BOOST_CHECK_EQUAL(0.0, scale->getMinimum());
  BOOST_CHECK_EQUAL(127.0, scale->getMaximum());
  BOOST_CHECK_EQUAL(1.0, scale->getStep());
  BOOST_CHECK_EQUAL(7, scale->getNbits());
}

BOOST_AUTO_TEST_CASE(esScaleHandle_cscore)
{
  using tmeventsetup::esScaleHandle;
  using tmeventsetup::esScale;

  const tmtable::Row row{
    {"object", "CICADA"},
    {"type", "CSCORE"},
    {"minimum", "+0.0000000000000000E+00"},
    {"maximum" ,"+2.5600000000000000E+02"},
    {"step", "+3.9062500000000000E-03"},
    {"n_bits", "16"}
  };

  esScaleHandle handle(row);

  esScale& scale = handle;

  BOOST_CHECK_EQUAL("CICADA-CSCORE", scale.getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Cicada, scale.getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::CScoreScale, scale.getScaleType());
  BOOST_CHECK_EQUAL(0.0, scale.getMinimum());
  BOOST_CHECK_EQUAL(256.0, scale.getMaximum());
  BOOST_CHECK_EQUAL(3.90625e-3, scale.getStep());
  BOOST_CHECK_EQUAL(16, scale.getNbits());
}

BOOST_AUTO_TEST_CASE(esScaleHandle_precision_delta)
{
  using tmeventsetup::esScaleHandle;
  using tmeventsetup::esScale;

  const tmtable::Row row{
    {"object", "PRECISION"},
    {"type", "TAU-MU-Delta"},
    {"minimum", "+0.0000000000000000E+00"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"step", "+0.0000000000000000E+00"},
    {"n_bits", "4"}
  };

  esScaleHandle handle(row);

  esScale& scale = handle;

  BOOST_CHECK_EQUAL("PRECISION-TAU-MU-Delta", scale.getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Precision, scale.getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::DeltaPrecision, scale.getScaleType());
  BOOST_CHECK_EQUAL(0.0, scale.getMinimum());
  BOOST_CHECK_EQUAL(0.0, scale.getMaximum());
  BOOST_CHECK_EQUAL(0.0, scale.getStep());
  BOOST_CHECK_EQUAL(4, scale.getNbits());
}

BOOST_AUTO_TEST_CASE(esScaleHandle_precision_delta_overlap_removal)
{
  using tmeventsetup::esScaleHandle;
  using tmeventsetup::esScale;

  const tmtable::Row row{
    {"object", "PRECISION"},
    {"type", "MU-MU-DeltaOverlapRemoval"},
    {"minimum", "+0.0000000000000000E+00"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"step", "+0.0000000000000000E+00"},
    {"n_bits", "9"}
  };

  esScaleHandle handle(row);

  esScale& scale = handle;

  BOOST_CHECK_EQUAL("PRECISION-MU-MU-DeltaOverlapRemoval", scale.getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Precision, scale.getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::OvRmDeltaPrecision, scale.getScaleType());
  BOOST_CHECK_EQUAL(0.0, scale.getMinimum());
  BOOST_CHECK_EQUAL(0.0, scale.getMaximum());
  BOOST_CHECK_EQUAL(0.0, scale.getStep());
  BOOST_CHECK_EQUAL(9, scale.getNbits());
}

BOOST_AUTO_TEST_CASE(esScaleHandle_precision_mass)
{
  using tmeventsetup::esScaleHandle;
  using tmeventsetup::esScale;

  const tmtable::Row row{
    {"object", "PRECISION"},
    {"type", "EG-TAU-Mass"},
    {"minimum", "+0.0000000000000000E+00"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"step", "+0.0000000000000000E+00"},
    {"n_bits", "1"}
  };

  esScaleHandle handle(row);

  esScale& scale = handle;

  BOOST_CHECK_EQUAL("PRECISION-EG-TAU-Mass", scale.getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Precision, scale.getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::MassPrecision, scale.getScaleType());
  BOOST_CHECK_EQUAL(0.0, scale.getMinimum());
  BOOST_CHECK_EQUAL(0.0, scale.getMaximum());
  BOOST_CHECK_EQUAL(0.0, scale.getStep());
  BOOST_CHECK_EQUAL(1, scale.getNbits());
}

BOOST_AUTO_TEST_CASE(esScaleHandle_precision_cscore)
{
  using tmeventsetup::esScaleHandle;
  using tmeventsetup::esScale;

  const tmtable::Row row{
    {"object", "PRECISION"},
    {"type", "CICADA-CScore"},
    {"minimum", "+0.0000000000000000E+00"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"step", "+0.0000000000000000E+00"},
    {"n_bits", "8"}
  };

  esScaleHandle handle(row);

  esScale& scale = handle;

  BOOST_CHECK_EQUAL("PRECISION-CICADA-CScore", scale.getName());
  BOOST_CHECK_EQUAL(tmeventsetup::Precision, scale.getObjectType());
  BOOST_CHECK_EQUAL(tmeventsetup::CScorePrecision, scale.getScaleType());
  BOOST_CHECK_EQUAL(0.0, scale.getMinimum());
  BOOST_CHECK_EQUAL(0.0, scale.getMaximum());
  BOOST_CHECK_EQUAL(0.0, scale.getStep());
  BOOST_CHECK_EQUAL(8, scale.getNbits());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(esTriggerMenu)

BOOST_AUTO_TEST_CASE(esTriggerMenu_attributes)
{
  using tmeventsetup::esTriggerMenu;

  // References
  const std::string name = "L1Menu_Foobar";
  const std::string version = "1.42";
  const std::string comment = "NO-body expects the Spanish Inquisition!";
  const std::string datetime = "2016-12-25T18:00:00+0000";
  const std::string firmwareUuid = "f06d0803-1852-4ac8-9f79-01299371a28b";
  const std::string scaleSetName = "spam";
  const size_t nModules = 4;

  // Create menu container
  esTriggerMenu menu;

  // Populate attribuites
  menu.setName(name);
  menu.setVersion(version);
  menu.setComment(comment);
  menu.setDatetime(datetime);
  menu.setFirmwareUuid(firmwareUuid);
  menu.setScaleSetName(scaleSetName);
  menu.setNmodules(nModules);

  // Check attributes
  BOOST_CHECK_EQUAL(name, menu.getName());
  BOOST_CHECK_EQUAL(version, menu.getVersion());
  BOOST_CHECK_EQUAL(comment, menu.getComment());
  BOOST_CHECK_EQUAL(datetime, menu.getDatetime());
  BOOST_CHECK_EQUAL(firmwareUuid, menu.getFirmwareUuid());
  BOOST_CHECK_EQUAL(scaleSetName, menu.getScaleSetName());
  BOOST_CHECK_EQUAL(nModules, menu.getNmodules());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(esTriggerMenuHandle)

BOOST_AUTO_TEST_CASE(esTriggerMenuHandle_isThresholdType)
{
  using tmeventsetup::isThresholdType;

  BOOST_CHECK_EQUAL(true, isThresholdType(tmeventsetup::Threshold));
  BOOST_CHECK_EQUAL(true, isThresholdType(tmeventsetup::Count));

  BOOST_CHECK_EQUAL(false, isThresholdType(tmeventsetup::Eta));
  BOOST_CHECK_EQUAL(false, isThresholdType(tmeventsetup::Phi));
  BOOST_CHECK_EQUAL(false, isThresholdType(tmeventsetup::UnconstrainedPt));
}

BOOST_AUTO_TEST_CASE(esTriggerMenuHandle_isWindowType)
{
  using tmeventsetup::isWindowType;

  BOOST_CHECK_EQUAL(true, isWindowType(tmeventsetup::Eta));
  BOOST_CHECK_EQUAL(true, isWindowType(tmeventsetup::Phi));
  BOOST_CHECK_EQUAL(true, isWindowType(tmeventsetup::UnconstrainedPt));

  BOOST_CHECK_EQUAL(false, isWindowType(tmeventsetup::Threshold));
  BOOST_CHECK_EQUAL(false, isWindowType(tmeventsetup::Count));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(tmEventSetup)

BOOST_AUTO_TEST_CASE(tmEventSetup_getValue)
{
  using tmeventsetup::getValue;

  const tmtable::Row data{{"foo", "42"}};

  BOOST_CHECK_EQUAL(getValue(data, ""), "");
  BOOST_CHECK_EQUAL(getValue(data, "foo"), "42");
  BOOST_CHECK_EQUAL(getValue(data, "bar"), "");
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getHash)
{
  BOOST_CHECK_EQUAL(tmeventsetup::getHash("foo"), 1685487);
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getHashUlong)
{
  BOOST_CHECK_EQUAL(tmeventsetup::getHashUlong("foo"), 1685487);
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getMmHashN)
{
  unsigned int seed = 3735927486; // fixed inside getMmHashN

  std::string phrase = "NO-body expects the Spanish Inquisition!";

  BOOST_CHECK_EQUAL(
    tmeventsetup::getMmHashN(phrase),
    tmutil::MurmurHashNeutral2(phrase.c_str(), phrase.size(), seed)
  );
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getExternalName)
{
  using tmeventsetup::getExternalName;

  BOOST_CHECK_EQUAL(getExternalName(""), "");
  BOOST_CHECK_EQUAL(getExternalName("EXT_"), "");
  BOOST_CHECK_EQUAL(getExternalName("+1"), "");
  BOOST_CHECK_EQUAL(getExternalName("-1"), "");
  BOOST_CHECK_EQUAL(getExternalName("EXT_A+B+C"), "A");
  BOOST_CHECK_EQUAL(getExternalName("EXT_FOO"), "FOO");
  BOOST_CHECK_EQUAL(getExternalName("EXT_FOO+0"), "FOO");
  BOOST_CHECK_EQUAL(getExternalName("EXT_FOO-0"), "FOO");
  BOOST_CHECK_EQUAL(getExternalName("EXT_FOO_BAR+2"), "FOO_BAR");
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getObjectCombination)
{
  using tmeventsetup::getObjectCombination;

  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Muon, tmeventsetup::Muon), tmeventsetup::MuonMuonCombination);

  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Muon, tmeventsetup::ETM), tmeventsetup::MuonEsumCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Muon, tmeventsetup::HTM), tmeventsetup::MuonEsumCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Muon, tmeventsetup::ETMHF), tmeventsetup::MuonEsumCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Muon, tmeventsetup::HTMHF), tmeventsetup::MuonEsumCombination);

  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Muon, tmeventsetup::Egamma), tmeventsetup::CaloMuonCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Muon, tmeventsetup::Tau), tmeventsetup::CaloMuonCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Muon, tmeventsetup::Jet), tmeventsetup::CaloMuonCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Muon, tmeventsetup::Egamma), tmeventsetup::CaloMuonCombination);

  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Egamma, tmeventsetup::Egamma), tmeventsetup::CaloCaloCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Egamma, tmeventsetup::Tau), tmeventsetup::CaloCaloCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Egamma, tmeventsetup::Jet), tmeventsetup::CaloCaloCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Tau, tmeventsetup::Egamma), tmeventsetup::CaloCaloCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Tau, tmeventsetup::Tau), tmeventsetup::CaloCaloCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Tau, tmeventsetup::Jet), tmeventsetup::CaloCaloCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Jet, tmeventsetup::Egamma), tmeventsetup::CaloCaloCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Jet, tmeventsetup::Tau), tmeventsetup::CaloCaloCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Jet, tmeventsetup::Jet), tmeventsetup::CaloCaloCombination);

  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Egamma, tmeventsetup::ETM), tmeventsetup::CaloEsumCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Egamma, tmeventsetup::HTM), tmeventsetup::CaloEsumCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Egamma, tmeventsetup::ETMHF), tmeventsetup::CaloEsumCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Egamma, tmeventsetup::HTMHF), tmeventsetup::CaloEsumCombination);

  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Tau, tmeventsetup::ETM), tmeventsetup::CaloEsumCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Tau, tmeventsetup::HTM), tmeventsetup::CaloEsumCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Tau, tmeventsetup::ETMHF), tmeventsetup::CaloEsumCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Tau, tmeventsetup::HTMHF), tmeventsetup::CaloEsumCombination);

  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Jet, tmeventsetup::ETM), tmeventsetup::CaloEsumCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Jet, tmeventsetup::HTM), tmeventsetup::CaloEsumCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Jet, tmeventsetup::ETMHF), tmeventsetup::CaloEsumCombination);
  BOOST_CHECK_EQUAL(getObjectCombination(tmeventsetup::Jet, tmeventsetup::HTMHF), tmeventsetup::CaloEsumCombination);
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getPrecisions)
{
  using precision_type = std::map<std::string, unsigned int>;
  using scaleMap_type = std::map<std::string, tmeventsetup::esScale>;

  precision_type precision;
  scaleMap_type scaleMap;

  tmeventsetup::getPrecisions(precision, scaleMap);
  BOOST_CHECK_EQUAL(precision.size(), 0);

  {
    const tmtable::Row row{
      {"object", "JET"},
      {"type", "ET"},
      {"minimum", "+0.0000000000000000E+00"},
      {"maximum" ,"+127.0000000000000000E+00"},
      {"step", "+1.0000000000000000E+00"},
      {"n_bits", "7"}
    };

    tmeventsetup::esScaleHandle handle(row);
    tmeventsetup::esScale* scale = dynamic_cast<tmeventsetup::esScale*>(&handle);
    scaleMap["JET-ET"] = *scale;
  }

  tmeventsetup::getPrecisions(precision, scaleMap);
  BOOST_CHECK_EQUAL(precision.size(), 0);

  {
    const tmtable::Row row{
      {"object", "PRECISION"},
      {"type", "EG-EG-Delta"},
      {"minimum", "+0.0000000000000000E+00"},
      {"maximum" ,"+0.0000000000000000E+00"},
      {"step", "+0.0000000000000000E+00"},
      {"n_bits", "3"}
    };

    tmeventsetup::esScaleHandle handle(row);
    tmeventsetup::esScale* scale = dynamic_cast<tmeventsetup::esScale*>(&handle);
    scaleMap["PRECISION-EG-EG-Delta"] = *scale;
  }

  tmeventsetup::getPrecisions(precision, scaleMap);
  BOOST_CHECK_EQUAL(precision.size(), 1);
  BOOST_CHECK_EQUAL(precision.at("PRECISION-EG-EG-Delta"), 3);

}

BOOST_AUTO_TEST_CASE(tmEventSetup_toFixedPoint)
{
  using tmeventsetup::toFixedPoint;

  BOOST_CHECK_EQUAL(0, toFixedPoint(0.123, 0));
  BOOST_CHECK_EQUAL(1, toFixedPoint(0.123, 1));
  BOOST_CHECK_EQUAL(12, toFixedPoint(0.123, 2));
  BOOST_CHECK_EQUAL(123, toFixedPoint(0.123, 3));

  BOOST_CHECK_EQUAL(123, toFixedPoint(123.456, 0));
  BOOST_CHECK_EQUAL(1235, toFixedPoint(123.456, 1));
  BOOST_CHECK_EQUAL(12346, toFixedPoint(123.456, 2));
  BOOST_CHECK_EQUAL(123456, toFixedPoint(123.456, 3));

  // Check rounding
  BOOST_CHECK_EQUAL(14, toFixedPoint(1.449, 1));
  BOOST_CHECK_EQUAL(15, toFixedPoint(1.499, 1));
  BOOST_CHECK_EQUAL(15, toFixedPoint(1.500, 1));
}

BOOST_AUTO_TEST_CASE(tmEventSetup_setLut)
{
  size_t precision = 1;

  const std::vector<double> values {0.123, 1.234, 2.345, 3.456 };

  // Reference set with precision of 1
  const std::vector<long long> reference{1, 12, 23, 35};

  std::vector<long long> results;
  tmeventsetup::setLut(results, values, precision);

  BOOST_CHECK_EQUAL_COLLECTIONS(results.begin(), results.end(),
                                reference.begin(), reference.end());
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getLut)
{
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getCaloMuonEtaConversionLut)
{
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getCaloMuonPhiConversionLut)
{
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getDeltaVector)
{
}

BOOST_AUTO_TEST_CASE(tmEventSetup_applySin)
{
  std::vector<double> values{0.123, 1.234, 2.345, 3.456, 4.567, 5.678};

  std::vector<double> reference(values.size());
  std::transform(values.begin(), values.end(), reference.begin(), ::sin);

  tmeventsetup::applySin(values, values.size());

  BOOST_CHECK_EQUAL_COLLECTIONS(values.begin(), values.end(),
                                reference.begin(), reference.end());
}

BOOST_AUTO_TEST_CASE(tmEventSetup_applyCos)
{
  std::vector<double> values{0.123, 1.234, 2.345, 3.456, 4.567, 5.678};

  std::vector<double> reference(values.size());
  std::transform(values.begin(), values.end(), reference.begin(), ::cos);

  tmeventsetup::applyCos(values, values.size());

  BOOST_CHECK_EQUAL_COLLECTIONS(values.begin(), values.end(),
                                reference.begin(), reference.end());
}

BOOST_AUTO_TEST_CASE(tmEventSetup_applyCosh)
{
  std::vector<double> values{0.123, 1.234, 2.345, 3.456, 4.567, 5.678};

  std::vector<double> reference(values.size());
  std::transform(values.begin(), values.end(), reference.begin(), ::cosh);

  tmeventsetup::applyCosh(values, values.size());

  BOOST_CHECK_EQUAL_COLLECTIONS(values.begin(), values.end(),
                                reference.begin(), reference.end());
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getModel_object)
{
  using tmeventsetup::esObjectHandle;
  using tmeventsetup::esCutHandle;

  esObjectHandle object;

  object.addCut(esCutHandle({
    {"name", "TOPO-MODEL_hh_mu"},
    {"object", "TOPO"},
    {"type", "TOPO-MODEL"},
    {"minimum" ,"+0.0000000000000000E+00"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", "hh_mu"}
  }));

  const auto model = tmeventsetup::getModel(object);

  BOOST_CHECK(model);
  BOOST_CHECK_EQUAL("hh_mu", *model);
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getModel_condition)
{
  using tmeventsetup::esConditionHandle;
  using tmeventsetup::esObjectHandle;
  using tmeventsetup::esCutHandle;

  esObjectHandle object;
  object.addCut(esCutHandle({
    {"name", "TOPO-MODEL_hh_mu"},
    {"object", "TOPO"},
    {"type", "TOPO-MODEL"},
    {"minimum" ,"+0.0000000000000000E+00"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", "hh_mu"}
  }));
  esConditionHandle condition;
  condition.addObject(object);

  const auto model = tmeventsetup::getModel(condition);

  BOOST_CHECK(model);
  BOOST_CHECK_EQUAL("hh_mu", *model);
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getScore_object)
{
  using tmeventsetup::esObjectHandle;
  using tmeventsetup::esCutHandle;

  esObjectHandle object;
  object.addCut(esCutHandle({
    {"name", "TOPO-SCORE_42"},
    {"object", "TOPO"},
    {"type", "TOPO-SCORE"},
    {"minimum" ,"+4.2000000000000000E+01"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", ""}
  }));

  const auto score = tmeventsetup::getScore(object);

  BOOST_CHECK(score);
  BOOST_CHECK_EQUAL(42, *score);
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getScore_condition)
{
  using tmeventsetup::esConditionHandle;
  using tmeventsetup::esObjectHandle;
  using tmeventsetup::esCutHandle;

  esObjectHandle object;
  object.addCut(esCutHandle({
    {"name", "TOPO-SCORE_42"},
    {"object", "TOPO"},
    {"type", "TOPO-SCORE"},
    {"minimum" ,"+4.2000000000000000E+01"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", ""}
  }));
  esConditionHandle condition;
  condition.addObject(object);

  const auto score = tmeventsetup::getScore(condition);

  BOOST_CHECK(score);
  BOOST_CHECK_EQUAL(42, *score);
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getAnomalyScore_object)
{
  using tmeventsetup::esObjectHandle;
  using tmeventsetup::esCutHandle;

  esObjectHandle object;
  object.addCut(esCutHandle({
    {"name", "ADT-ASCORE_42"},
    {"object", "ADT"},
    {"type", "ADT-ASCORE"},
    {"minimum" ,"+4.2000000000000000E+01"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", ""}
  }));

  const auto score = tmeventsetup::getAnomalyScore(object);

  BOOST_CHECK(score);
  BOOST_CHECK_EQUAL(42, *score);
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getAnomalyScore_condition)
{
  using tmeventsetup::esConditionHandle;
  using tmeventsetup::esObjectHandle;
  using tmeventsetup::esCutHandle;

  esObjectHandle object;
  object.addCut(esCutHandle({
    {"name", "ADT-ASCORE_42"},
    {"object", "ADT"},
    {"type", "ADT-ASCORE"},
    {"minimum" ,"+4.2000000000000000E+01"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", ""}
  }));
  esConditionHandle condition;
  condition.addObject(object);

  const auto score = tmeventsetup::getAnomalyScore(condition);

  BOOST_CHECK(score);
  BOOST_CHECK_EQUAL(42, *score);
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getCicadaScore_object)
{
  using tmeventsetup::esObjectHandle;
  using tmeventsetup::esCutHandle;

  esObjectHandle object;
  esCutHandle cut({
    {"name", "CICADA-ASCORE_42p0"},
    {"object", "CICADA"},
    {"type", "CICADA-CSCORE"},
    {"minimum" ,"+4.2000000000000000E+01"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", ""}
  });
  // 10752 in fixed-point encoded integer with 8 bits fraction
  cut.setMinimumIndex(tmutil::encodeFixedPoint<unsigned int>(cut.getMinimumValue(), 8));
  object.addCut(cut);

  const auto score = tmeventsetup::getCicadaScore(object);

  BOOST_CHECK(score);
  BOOST_CHECK_EQUAL(10752, *score);
}

BOOST_AUTO_TEST_CASE(tmEventSetup_getCicadaScore_condition)
{
  using tmeventsetup::esConditionHandle;
  using tmeventsetup::esObjectHandle;
  using tmeventsetup::esCutHandle;

  esObjectHandle object;
  esCutHandle cut({
    {"name", "CICADA-CSCORE_42p0"},
    {"object", "CICADA"},
    {"type", "CICADA-CSCORE"},
    {"minimum" ,"+4.2000000000000000E+01"},
    {"maximum" ,"+0.0000000000000000E+00"},
    {"data", ""}
  });
  // 10752 in fixed-point encoded integer with 8 bits fraction
  cut.setMinimumIndex(tmutil::encodeFixedPoint<unsigned int>(cut.getMinimumValue(), 8));
  object.addCut(cut);
  esConditionHandle condition;
  condition.addObject(object);

  const auto score = tmeventsetup::getCicadaScore(condition);

  BOOST_CHECK(score);
  BOOST_CHECK_EQUAL(10752, *score);
}

BOOST_AUTO_TEST_SUITE_END()
