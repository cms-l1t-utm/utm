#include "tmEventSetup/esCutHandle.hh"

#include "tmGrammar/Cut.hh"
#include "tmGrammar/Object.hh"
#include "tmGrammar/Function.hh"

#include "tmUtil/tmUtil.hh"

#include <boost/algorithm/string/replace.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

namespace tmeventsetup
{

// keys for tables
const std::string kName{"name"};
const std::string kType{"type"};
const std::string kMinimum{"minimum"};
const std::string kMaximum{"maximum"};
const std::string kData{"data"};
const std::string kSeparator{"-"};

const std::map<std::string, esObjectType> ObjectTypeMap {
  {Object::MU, Muon},
  {Object::EG, Egamma},
  {Object::TAU, Tau},
  {Object::JET, Jet},
  {Object::ETM, ETM},
  {Object::HTM, HTM},
  {Object::ETT, ETT},
  {Object::HTT, HTT},
  {Object::MBT0HFP, MBT0HFP},
  {Object::MBT1HFP, MBT1HFP},
  {Object::MBT0HFM, MBT0HFM},
  {Object::MBT1HFM, MBT1HFM},
  {Object::ETTEM, ETTEM},
  {Object::ETMHF, ETMHF},
  {Object::HTMHF, HTMHF},
  {Object::TOWERCOUNT, TOWERCOUNT},
  {Object::ASYMET, ASYMET},
  {Object::ASYMHT, ASYMHT},
  {Object::ASYMETHF, ASYMETHF},
  {Object::ASYMHTHF, ASYMHTHF},
  {Object::ADT, ADT},
  {Object::ZDCP, ZDCP},
  {Object::ZDCM, ZDCM},
  {Object::AXO, Axol1tl},
  {Object::TOPO, Topological},
  {Object::CICADA, Cicada},
};

const std::map<std::string, esFunctionType> FunctionTypeMap {
  {Cut::DETA, DistFunction},
  {Cut::DPHI, DistFunction},
  {Cut::DR, DistFunction},
  {Cut::MASS, MassFunction},
  {Cut::MASSUPT, MassFunction},
  {Cut::MASSDR, MassFunction},
  {Cut::CHGCOR, CombFunction},
  {Cut::TBPT, MassFunction},
  {Cut::ORMDETA, DistFunction},
  {Cut::ORMDPHI, DistFunction},
  {Cut::ORMDR, DistFunction},
};

const std::map<std::string, esCutType> CutTypeMap {
  {ET_THR, Threshold},
  {Cut::UPT, UnconstrainedPt},
  {Cut::ISO, Isolation},
  {Cut::ETA, Eta},
  {Cut::QLTY, Quality},
  {Cut::DISP, Displaced},
  {Cut::CHG, Charge},
  {Cut::PHI, Phi},
  {Cut::SLICE, Slice},
  {Cut::INDEX, Index},
  {Cut::IP, ImpactParameter},
  {Cut::ASCORE, AnomalyScore},
  {Cut::SCORE, Score},
  {Cut::MODEL, Model},
  {Cut::CSCORE, CicadaScore},
  {COUNT, Count},
  {Cut::DETA, DeltaEta},
  {Cut::DPHI, DeltaPhi},
  {Cut::DR, DeltaR},
  {Cut::MASS, Mass},
  {Cut::MASSUPT, MassUpt},
  {Cut::MASSDR, MassDeltaR},
  {Cut::CHGCOR, ChargeCorrelation},
  {Cut::TBPT, TwoBodyPt},
  {Cut::ORMDETA, OvRmDeltaEta},
  {Cut::ORMDPHI, OvRmDeltaPhi},
  {Cut::ORMDR, OvRmDeltaR},
};

// select cut key prefix
const std::map<int, std::string> KeyPrefixMap {
  {Muon, Object::MU},
  {Egamma, Object::EG},
  {Tau, Object::TAU},
  {Jet, Object::JET},
  {ETM, Object::ETM},
  {HTM, Object::HTM},
  {ETT, Object::ETT},
  {HTT, Object::HTT},
  {MBT0HFP, Object::MBT0HFP},
  {MBT1HFP, Object::MBT1HFP},
  {MBT0HFM, Object::MBT0HFM},
  {MBT1HFM, Object::MBT1HFM},
  {ETTEM, Object::ETTEM},
  {ETMHF, Object::ETMHF},
  {HTMHF, Object::HTMHF},
  {TOWERCOUNT, Object::TOWERCOUNT},
  {ASYMET, Object::ASYMET},
  {ASYMHT, Object::ASYMHT},
  {ASYMETHF, Object::ASYMETHF},
  {ASYMHTHF, Object::ASYMHTHF},
  {ADT, Object::ADT},
  {ZDCP, Object::ZDCP},
  {ZDCM, Object::ZDCM},
  {Axol1tl, Object::AXO},
  {Topological, Object::TOPO},
  {Cicada, Object::CICADA},
  {CombFunction, ""},
  {DistFunction, ""},
  {MassFunction, ""}, // alias for invariant mass
  {InvariantMassFunction, ""},
  {InvariantMassDeltaRFunction, ""},
  {TransverseMassFunction, ""},
};

// select cut key suffix
const std::map<int, std::string> KeySuffixMap {
  {Threshold, ET_THR},
  {UnconstrainedPt, Cut::UPT},
  {Eta, Cut::ETA},
  {Phi, Cut::PHI},
  {Charge, Cut::CHG},
  {Quality, Cut::QLTY},
  {Isolation, Cut::ISO},
  {Displaced, Cut::DISP},
  {ImpactParameter, Cut::IP},
  {DeltaEta, Cut::DETA},
  {DeltaPhi, Cut::DPHI},
  {DeltaR, Cut::DR},
  {Mass, Cut::MASS},
  {MassUpt, Cut::MASSUPT},
  {MassDeltaR, Cut::MASSDR},
  {ChargeCorrelation, Cut::CHGCOR},
  {Slice, Cut::SLICE},
  {Index, Cut::INDEX},
  {AnomalyScore, Cut::ASCORE},
  {Score, Cut::SCORE},
  {Model, Cut::MODEL},
  {CicadaScore, Cut::CSCORE},
  {Count, COUNT},
  {TwoBodyPt, Cut::TBPT},
  {OvRmDeltaEta, Cut::ORMDETA},
  {OvRmDeltaPhi, Cut::ORMDPHI},
  {OvRmDeltaR, Cut::ORMDR}
};


esObjectType
toCutObjectType(const std::string& object)
{
  const auto cit = ObjectTypeMap.find(object);
  if (cit == std::end(ObjectTypeMap))
  {
    TM_FATAL_ERROR("no such object type: " << TM_QUOTE(object));
  }
  return cit->second;
}


esFunctionType
toCutFunctionType(const std::string& function)
{
  const auto cit = FunctionTypeMap.find(function);
  if (cit == std::end(FunctionTypeMap))
  {
    TM_FATAL_ERROR("no such function type: " << TM_QUOTE(function));
  }
  return cit->second;
}


esCutType
toCutType(const std::string& type)
{
  const auto cit = CutTypeMap.find(type);
  if (cit == std::end(CutTypeMap))
  {
    TM_FATAL_ERROR("no such cut type: " << TM_QUOTE(type));
  }
  return cit->second;
}


const std::string&
toKeyPrefix(int type)
{
  const auto cit = KeyPrefixMap.find(type);
  if (cit == std::end(KeyPrefixMap))
  {
    TM_FATAL_ERROR("no such object type: " << type);
  }
  return cit->second;
}


const std::string&
toKeySuffix(int type)
{
  const auto cit = KeySuffixMap.find(type);
  if (cit == std::end(KeySuffixMap))
  {
    TM_FATAL_ERROR("no such cut type: " << type);
  }
  return cit->second;
}


esCutHandle::esCutHandle(const tmtable::Row& cut)
{
  TM_LOG_DBG("");

  name_ = tmtable::get(cut, kName);
  const auto& type = tmtable::get(cut, kType);
  TM_LOG_DBG(TM_VALUE_DBG(name_));
  TM_LOG_DBG(TM_VALUE_DBG(type));

  // Split cut type into tokens, eg. "MU-ISO" -> ["MU", "ISO"].
  boost::char_separator<char> sep(kSeparator.c_str());
  boost::tokenizer<boost::char_separator<char> > tok(type, sep);
  std::vector<std::string> tokens(std::begin(tok), std::end(tok));

  object_type_ = static_cast<esObjectType>(Undef); // TODO
  cut_type_ = static_cast<esCutType>(Undef);

  if (tokens.size() == 1)
  {
    const auto& functionType = tokens.front();
    object_type_ = toCutFunctionType(functionType);

    const auto& cutType = tokens.back();
    cut_type_ = toCutType(cutType);
  }
  else if (tokens.size() == 2)
  {
    const auto& objectType = tokens.front();
    object_type_ = toCutObjectType(objectType);

    const auto& cutType = tokens.back();
    cut_type_ = toCutType(cutType);
  }
  else
  {
    TM_FATAL_ERROR("invalid # of tokens: " << tokens.size());
  }

  const auto& minimum = tmtable::get(cut, kMinimum);
  const auto& maximum = tmtable::get(cut, kMaximum);

  if (cut_type_ == Threshold)
  {
    try
    {
      setMinimumValue(Cut::parseThreshold(minimum));
    }
    catch (boost::bad_lexical_cast& e)
    {
      TM_FATAL_ERROR("invalid threshold " << TM_QUOTE(minimum) << " for cut " << TM_QUOTE(name_));
    }
  }

  // Set minimum value if available.
  if (not minimum.empty())
  {
    try
    {
      setMinimumValue(boost::lexical_cast<double>(minimum));
    }
    catch (boost::bad_lexical_cast& e)
    {
      TM_FATAL_ERROR("invalid minimum value " << TM_QUOTE(minimum) << " for cut " << TM_QUOTE(name_));
    }
  }

  // Set maximum value if available.
  if (not maximum.empty())
  {
    try
    {
      setMaximumValue(boost::lexical_cast<double>(maximum));
    }
    catch (boost::bad_lexical_cast& e)
    {
      TM_FATAL_ERROR("invalid maximum value '" << TM_QUOTE(maximum) << " for cut " << TM_QUOTE(name_));
    }
  }

  if ((cut_type_ == AnomalyScore) or (cut_type_ == Score) or (cut_type_ == CicadaScore))
  {
    setPrecision(0);
  }
  if ((cut_type_ == Model))
  {
    setPrecision(0);
  }

  // Set data.
  setData(tmtable::get(cut, kData));
  setKey();
}


void
esCutHandle::setData(const std::string& data)
{
  switch (cut_type_)
  {
    case Quality:
    case Isolation:
    case ImpactParameter:
    {
      data_ = toLutData(data);
    } break;

    case Displaced:
    {
      data_ = toIndexData(data);
    } break;

    default:
      data_ = data;
      break;
  }
}


void
esCutHandle::setKey()
{
  TM_LOG_DBG("");

  key_ = toKeyPrefix(object_type_);

  if (key_.size())
    key_ += kSeparator;

  key_ += toKeySuffix(cut_type_);

  TM_LOG_DBG(TM_VALUE_DBG(key_));
}


std::string
esCutHandle::toIndexData(const std::string& data) const
{
  // data stored as index number
  try
  {
    return std::to_string(boost::lexical_cast<unsigned int>(data));
  }
  catch(boost::bad_lexical_cast& e)
  {
    TM_FATAL_ERROR("invalid data for " << name_ << ": " << TM_QUOTE(data));
  }
}


std::string
esCutHandle::toLutData(const std::string& data) const
{
  // LUT data stored as comma separated values,
  // take OR of all the bit patterns
  try
  {
    return std::to_string(tmutil::splitLut(data));
  }
  catch(boost::bad_lexical_cast& e)
  {
    TM_FATAL_ERROR("invalid LUT data for " << name_ << ": " << TM_QUOTE(data));
  }
}

} // namespace tmeventsetup
