#include "tmEventSetup/tmEventSetup.hh"
#include "tmEventSetup/esTriggerMenuHandle.hh"

#include "tmGrammar/Algorithm.hh"
#include "tmGrammar/Object.hh"
#include "tmTable/tmTable.hh"

#include "tmUtil/tmUtil.hh"

#include <boost/lexical_cast.hpp>
#include <boost/none.hpp>

#include <algorithm>
#include <cmath>

namespace tmeventsetup
{

const std::string kSuffixSeparator{"_"};


// Functors for C math functions (workaround for gcc 6xx/7xx together with boost).
struct math_sin { double operator()(const double d){ return std::sin(d); } };
struct math_cos { double operator()(const double d){ return std::cos(d); } };
struct math_cosh { double operator()(const double d){ return std::cosh(d); } };


const esTriggerMenu*
getTriggerMenu(const std::string& path)
{
  TM_LOG_DBG(TM_VALUE_DBG(path));

  // read menu
  tmtable::Menu menu;
  tmtable::Scale scale;
  tmtable::ExtSignal extSignal;

  const auto message = tmtable::xml2menu(path.c_str(), menu, scale, extSignal);
  if (message.length())
  {
    TM_LOG_ERR(message);
    TM_FATAL_ERROR("failed to load from file: " << TM_QUOTE(path));
  }

  return new esTriggerMenuHandle(menu, scale, extSignal);
}


const esTriggerMenu*
getTriggerMenu(std::istringstream& iss)
{
  TM_LOG_DBG("");

  // read menu
  tmtable::Menu menu;
  tmtable::Scale scale;
  tmtable::ExtSignal extSignal;

  const auto message = tmtable::xml2menu(iss, menu, scale, extSignal);
  if (message.length())
  {
    TM_LOG_ERR(message);
    TM_FATAL_ERROR("failed to load from input stream");
  }

  return new esTriggerMenuHandle(menu, scale, extSignal);
}


std::string
getValue(const std::map<std::string, std::string>& map,
         const std::string& key)
{
  std::string value;
  std::map<std::string, std::string>::const_iterator cit;

  cit = map.find(key);
  if (cit != std::end(map))
  {
    value = cit->second;
  }

  return value;
}


long
getHash(const std::string& s)
{
  std::locale loc;
  const std::collate<char>& coll = std::use_facet<std::collate<char> >(loc);
  return coll.hash(s.data(), s.data() + s.length());
}


unsigned long
getHashUlong(const std::string& s)
{
  return static_cast<unsigned long>(getHash(s));
}


unsigned long
getMmHashN(const std::string& s)
{
  const void* key = s.c_str();
  int len = s.size();
  unsigned int seed = 3735927486;

  return tmutil::MurmurHashNeutral2(key, len, seed);
}


std::string
getExternalName(const std::string& name)
{
  const std::string prefix = Object::EXT + kSuffixSeparator;
  std::string ext_name(name);
  std::string::size_type index = ext_name.find(prefix);

  // Strip EXT_ prefix
  if (index == 0) ext_name.erase(index, prefix.length());

  // Strip optional BX offset suffix
  index = ext_name.find("+");
  if (index != std::string::npos)
    ext_name = ext_name.substr(0, index);
  index = ext_name.find("-");
  if (index != std::string::npos)
    ext_name = ext_name.substr(0, index);

  return ext_name;
}


esCombinationType
getObjectCombination(int type1,
                     int type2)
{
  return getObjectCombination(static_cast<esObjectType>(type1),
                              static_cast<esObjectType>(type2));
}


esCombinationType
getObjectCombination(esObjectType type1,
                     esObjectType type2)
{
  esCombinationType combination = static_cast<esCombinationType>(Undef);

  switch (type1)
  {
    case Muon:
      switch (type2)
      {
        case Muon:
          combination = MuonMuonCombination;
          break;
        case Egamma:
        case Tau:
        case Jet:
          combination = CaloMuonCombination;
          break;
        case ETM:
        case HTM:
        case ETMHF:
          combination = MuonEsumCombination;
          break;
        case HTMHF:
          combination = MuonEsumCombination;
          break;
        default:
          TM_FATAL_ERROR("unknown object type: " << type2);
      }
      break;

    case Egamma:
    case Tau:
    case Jet:
      switch (type2)
      {
        case Muon:
          combination = CaloMuonCombination;
          break;
        case Egamma:
        case Tau:
        case Jet:
          combination = CaloCaloCombination;
          break;
        case ETM:
        case HTM:
        case ETMHF:
          combination = CaloEsumCombination;
          break;
        case HTMHF:
          combination = CaloEsumCombination;
          break;
        default:
          TM_FATAL_ERROR("unknown object type: " << type2);
      }
      break;

    case ETM:
    case HTM:
    case ETMHF:
      switch (type2)
      {
        case Muon:
          combination = MuonEsumCombination;
          break;
        case Egamma:
        case Tau:
        case Jet:
          combination = CaloEsumCombination;
          break;
        default:
          TM_FATAL_ERROR("unknown object type: " << type2);
      }
      break;
    case HTMHF:
      switch (type2)
      {
        case Muon:
          combination = MuonEsumCombination;
          break;
        case Egamma:
        case Tau:
        case Jet:
          combination = CaloEsumCombination;
          break;
        default:
          TM_FATAL_ERROR("unknown object type: " << type2);
      }
      break;

    default:
      TM_FATAL_ERROR("unknown object type: " << type2);
  }

  return combination;
}


void
getPrecisions(std::map<std::string, unsigned int>& precision,
              const std::map<std::string, tmeventsetup::esScale>& scaleMap)
{
  for (const auto& scale: scaleMap)
  {
    switch (scale.second.getScaleType())
    {
      case DeltaPrecision:
      case OvRmDeltaPrecision:
      case MassPrecision:
      case MassPtPrecision:
      case MathPrecision:
      case TwoBodyPtPrecision:
      case TwoBodyPtMathPrecision:
      case InverseDeltaRPrecision:
      case CScorePrecision:
        break;

      default:
        continue;
    }

    // Note: precision value is stored in nbits entry of esScale
    const auto cit = precision.insert({scale.first, scale.second.getNbits()});
    if (not cit.second)
    {
      TM_FATAL_ERROR("insertion failed: " << TM_QUOTE(scale.first));
    }
  }
}


#if defined(SWIG)
void
getPrecisionsPy(std::map<std::string, unsigned int>& precision,
                const std::map<std::string, tmeventsetup::esScale*>& scaleMap)
{
  for (const auto& scale: scaleMap)
  {
    switch (scale.second->getScaleType())
    {
      case DeltaPrecision:
      case OvRmDeltaPrecision:
      case MassPrecision:
      case MassPtPrecision:
      case MathPrecision:
      case TwoBodyPtPrecision:
      case TwoBodyPtMathPrecision:
      case InverseDeltaRPrecision:
      case CScorePrecision:
        break;
      default:
        continue;
    }

    // Note: precision value is stored in nbits entry of esScale
    const auto rc = precision.insert({scale.first, scale.second->getNbits()});
    if (not rc.second)
    {
      TM_FATAL_ERROR("insertion failed: " << TM_QUOTE(scale.first));
    }
  }
}
#endif


long long
toFixedPoint(double real, const size_t precision)
{
  // Note: round to nearest integer
  return static_cast<long long>(std::floor(real * tmutil::pow10(precision) + 0.5));
}


void
setLut(std::vector<long long>& lut,
       const std::vector<double>& array,
       const unsigned int precision)
{
  lut.clear();
  for (const auto& value: array)
  {
    lut.emplace_back(std::move(toFixedPoint(value, precision)));
  }
}


size_t
getLut(std::vector<long long>& lut,
       const esScale* scale,
       const unsigned int precision)
{
  // Resize and clear
  lut.resize(std::pow(2, scale->getNbits()));
  std::fill(std::begin(lut), std::end(lut), 0);

  const std::vector<tmeventsetup::esBin> bins = scale->getBins();
  for (size_t ii = 0; ii < bins.size(); ii++)
  {
    double centre = (bins.at(ii).minimum + bins.at(ii).maximum)*0.5;
    lut.at(ii) = toFixedPoint(centre, precision);
  }
  size_t rc = bins.size();

  const std::string et(ET_THR);
  const bool setOverFlowBins = std::equal(et.rbegin(), et.rend(), scale->getName().rbegin());
  if (setOverFlowBins)
  {
    const double maximum = scale->getMaximum();
    const double step = fabs(maximum - scale->getMinimum())/(double)bins.size();
    const double overflow = maximum + step;
    for (size_t ii = bins.size(); ii < lut.size(); ii++)
    {
      lut.at(ii) = toFixedPoint(overflow, precision);
    }
    rc = lut.size();
  }

  return rc;
}


bool
isCaloMuonScale(const esScale* scale1,
                const esScale* scale2)
{
  bool rc = true;

  if (scale1->getScaleType() != scale2->getScaleType())
  {
    TM_FATAL_ERROR("different scale type given: "
      << scale1->getScaleType() << " != " << scale2->getScaleType());
  }

  switch (scale1->getObjectType())
  {
    case Egamma:
    case Tau:
    case Jet:
    case ETM:
    case HTM:
    case ETMHF:
      break;
    case HTMHF:
      break;

    default:
      TM_FATAL_ERROR("scale1 not of calo type: " << scale1->getObjectType());
  }

  if (scale2->getObjectType() != Muon)
  {
    TM_FATAL_ERROR("scale2 not of muon type: " << scale2->getObjectType());
  }

  return rc;
}


size_t
getCaloMuonEtaConversionLut(std::vector<long long>& lut,
                            const esScale* scale1,
                            const esScale* scale2)
{
  const esScaleType type = static_cast<esScaleType>(scale1->getScaleType());
  if (type != EtaScale)
  {
    TM_FATAL_ERROR("not EtaScale: " << type);
  }

  if (not isCaloMuonScale(scale1, scale2))
  {
    TM_FATAL_ERROR("not a calo muon scale pair");
  }

  const double step1 = scale1->getStep();
  const double step2 = scale2->getStep();
  if (step1 == 0.0 or step2 == 0.0)
  {
    TM_FATAL_ERROR("step size is 0.0");
  }

  if (step1 <= step2)
  {
    TM_FATAL_ERROR("step size of scale2 should be smaller");
  }


  lut.clear();

  const int ratio = step1 / step2;
  const int offset = ratio / 2;
  const size_t n = std::pow(2, scale1->getNbits());

  for (size_t ii = 0; ii < n/2; ii++)
  {
    lut.emplace_back(offset + ratio*ii);
  }
  for (size_t ii = n/2; ii < n; ii++)
  {
    lut.emplace_back(offset + ratio*(ii - static_cast<int>(n)));
  }

  return n;
}


size_t
getCaloMuonPhiConversionLut(std::vector<long long>& lut,
                            const esScale* scale1,
                            const esScale* scale2)
{
  const esScaleType type = static_cast<esScaleType>(scale1->getScaleType());
  if (type != PhiScale)
  {
    TM_FATAL_ERROR("not PhiScale: " << type);
  }

  if (not isCaloMuonScale(scale1, scale2))
  {
    TM_FATAL_ERROR("not a calo muon scale pair");
  }

  const double step1 = scale1->getStep();
  const double step2 = scale2->getStep();
  if (step1 == 0.0 or step2 == 0.0)
  {
    TM_FATAL_ERROR("step size is 0.0");
  }

  if (step1 <= step2)
  {
    TM_FATAL_ERROR("step size of scale2 should be smaller");
  }

  // Resize and clear
  lut.resize(std::pow(2, scale1->getNbits()));
  std::fill(std::begin(lut), std::end(lut), 0);

  const int ratio = step1 / step2;
  const int offset = ratio / 2;

  for (size_t ii = 0; ii < scale1->getBins().size(); ii++)
  {
    lut.at(ii) = offset + ratio*ii;
  }

  return scale1->getBins().size();
}


size_t
getDeltaVector(std::vector<double>& array,
               const esScale* scale1,
               const esScale* scale2)
{
  const double step = std::min(scale1->getStep(), scale2->getStep());
  const double range1 = scale1->getMaximum() - scale1->getMinimum();
  const double range2 = scale2->getMaximum() - scale2->getMinimum();
  const double range = std::max(range1, range2);
  const size_t n = std::ceil(range / step);
  const size_t bitwidth = std::ceil(std::log10(n) / std::log10(2));

  // Resize and clear
  array.resize(std::pow(2, bitwidth));
  std::fill(std::begin(array), std::end(array), 0);

  for (size_t ii = 0; ii < n; ii++)
  {
    array.at(ii) = step * ii;
  }

  return n;
}


void
applySin(std::vector<double>& array, const size_t n)
{
  const size_t offset = std::min(n, array.size());
  std::transform(std::begin(array), std::begin(array) + offset, std::begin(array), math_sin());
}


void
applyCos(std::vector<double>& array, const size_t n)
{
  const size_t offset = std::min(n, array.size());
  std::transform(std::begin(array), std::begin(array) + offset, std::begin(array), math_cos());
}


void
applyCosh(std::vector<double>& array, const size_t n)
{
  const size_t offset = std::min(n, array.size());
  std::transform(std::begin(array), std::begin(array) + offset, std::begin(array), math_cosh());
}


boost::optional<std::string>
getModel(const esObject& object)
{
  for (const auto& cut: object.getCuts())
  {
    if (cut.getCutType() == esCutType::Model)
    {
      return cut.getData();
    }
  }
  return boost::none;
}


boost::optional<std::string>
getModel(const esCondition& condition)
{
  for (const auto& object: condition.getObjects())
  {
    auto model = getModel(object);
    if (model)
    {
      return model;
    }
  }
  return boost::none;
}


boost::optional<unsigned int>
getScore(const esObject& object)
{
  for (const auto& cut: object.getCuts())
  {
    if (cut.getCutType() == esCutType::Score)
    {
      return static_cast<unsigned int>(cut.getMinimumValue());
    }
  }
  return boost::none;
}


boost::optional<unsigned int>
getScore(const esCondition& condition)
{
  for (const auto& object: condition.getObjects())
  {
    auto score = getScore(object);
    if (score)
    {
      return score;
    }
  }
  return boost::none;
}


boost::optional<unsigned int>
getAnomalyScore(const esObject& object)
{
  for (const auto& cut: object.getCuts())
  {
    if (cut.getCutType() == esCutType::AnomalyScore)
    {
      return cut.getMinimumValue();
    }
  }
  return boost::none;
}


boost::optional<unsigned int>
getAnomalyScore(const esCondition& condition)
{
  for (const auto& object: condition.getObjects())
  {
    auto score = getAnomalyScore(object);
    if (score)
    {
      return score;
    }
  }
  return boost::none;
}


boost::optional<unsigned int>
getCicadaScore(const esObject& object)
{
  for (const auto& cut: object.getCuts())
  {
    if (cut.getCutType() == esCutType::CicadaScore)
    {
      return cut.getMinimumIndex();
    }
  }
  return boost::none;
}


boost::optional<unsigned int>
getCicadaScore(const esCondition& condition)
{
  for (const auto& object: condition.getObjects())
  {
    auto score = getCicadaScore(object);
    if (score)
    {
      return score;
    }
  }
  return boost::none;
}

} // namespace tmeventsetup

/* eof */
