/**
 * @author      Takashi Matsushita
 * Created:     13 Nov 2015
 */

#ifndef tmEventSetup_esConditionHandle_hh
#define tmEventSetup_esConditionHandle_hh

#include "tmEventSetup/esCondition.hh"

namespace tmeventsetup
{

/**
 *  This class implements data structure for Condition
 */
class esConditionHandle : public esCondition
{
  public:
    esConditionHandle() = default;

    virtual ~esConditionHandle() = default;

    /** add object to condition */
    void addObject(const esObject& object);

    /** add cut to condition */
    void addCut(const esCut& cut);
};

} // namespace tmeventsetup

#endif // tmEventSetup_esConditionHandle_hh
