/**
 * @author      Takashi Matsushita
 * Created:     12 Mar 2015
 */

#ifndef tmEventSetup_esBin_hh
#define tmEventSetup_esBin_hh

#include <limits>

namespace tmeventsetup
{

/**
 *  This class implements data structure for Bin
 */
class esBin
{
  public:
    esBin()
    : hw_index(std::numeric_limits<unsigned int>::max()),
      minimum(std::numeric_limits<double>::min()),
      maximum(std::numeric_limits<double>::max()),
      version(0) {}

    esBin(const unsigned int index,
          const double minimum,
          const double maximum)
    : hw_index(index), minimum(minimum), maximum(maximum), version(0) {}

    virtual ~esBin() = default;

    unsigned int hw_index;   /**< HW index of bin */
    double minimum;          /**< minimum value of bin */
    double maximum;          /**< maximum value of bin */
    unsigned int version;
};

} // namespace tmeventsetup

#endif // tmEventSetup_esBin_hh
