/**
 * @author      Takashi Matsushita
 * Created:     11 Nov 2015
 */

#ifndef tmEventSetup_esObjectHandle_hh
#define tmEventSetup_esObjectHandle_hh

#include "tmEventSetup/esObject.hh"
#include "tmEventSetup/esCut.hh"
#include "tmGrammar/Object.hh"
#include "tmTable/tmTable.hh"

namespace tmeventsetup
{

/**
 *  This class implements data structure for Object
 */
class esObjectHandle : public esObject
{
  public:
    esObjectHandle() = default;

    esObjectHandle(const Object::Item& item);

    esObjectHandle(const Object::Item& item,
                   const tmtable::Table& cuts);

    virtual ~esObjectHandle() = default;

    /** set external channel id
     *
     * @param item [in] external map<name, id>
     */
    void setExternalChannelId(const std::map<std::string, unsigned int>& map);

    void addCut(const esCut& cut);

  private:
    const std::string& getThresholdType() const;
};

} // namespace tmeventsetup

#endif // tmEventSetup_esObjectHandle_hh
