/**
 * @author      Takashi Matsushita
 * Created:     12 Mar 2015
 */

#ifndef tmEventSetup_esBinHandle_hh
#define tmEventSetup_esBinHandle_hh

#include "tmEventSetup/esBin.hh"
#include "tmTable/tmTable.hh"

namespace tmeventsetup
{

/**
 *  This class implements data structure for Bin
 */
class esBinHandle : public esBin
{
  public:
    esBinHandle() = default;

    esBinHandle(const tmtable::Row& bin);

    virtual ~esBinHandle() = default;
};

} // namespace tmeventsetup

#endif // tmEventSetup_esBinHandle_hh
