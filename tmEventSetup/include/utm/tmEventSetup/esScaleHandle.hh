/**
 * @author      Takashi Matsushita
 * Created:     12 Mar 2015
 */

#ifndef tmEventSetup_esScaleHandle_hh
#define tmEventSetup_esScaleHandle_hh

#include "tmEventSetup/esScale.hh"

#include "tmTable/tmTable.hh"

namespace tmeventsetup
{

/**
 *  This class implements data structure for Scale
 */
class esScaleHandle : public esScale
{
  public:
    esScaleHandle() = default;

    esScaleHandle(const tmtable::Row& scale);

    virtual ~esScaleHandle() = default;

    /** add bin to the scale */
    void addBin(const esBin& bin);

    /** sort the esBin vector with esBin.minimum */
    void sortBins();

  protected:
    void sortBinsByMinimum();
    void sortBinsByHwIndex();

};

} // namespace tmeventsetup

#endif // tmEventSetup_esScaleHandle_hh
