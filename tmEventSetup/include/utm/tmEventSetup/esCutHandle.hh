/**
 * @author      Takashi Matsushita
 * Created:     12 Mar 2015
 */

#ifndef tmEventSetup_esCutHandle_hh
#define tmEventSetup_esCutHandle_hh

#include "tmEventSetup/esCut.hh"
#include "tmTable/tmTable.hh"

#include <string>

namespace tmeventsetup
{

/**
 *  This class implements data structure for Cut
 */
class esCutHandle : public esCut
{
  public:
    esCutHandle() = default;

    esCutHandle(const tmtable::Row& cut);

    virtual ~esCutHandle() = default;

    /** set data */
    void setData(const std::string& data);

    /** get a key for accessing a scale */
    void setKey();

  private:
    std::string toIndexData(const std::string& data) const;
    std::string toLutData(const std::string& data) const;
};

} // namespace tmeventsetup

#endif // tmEventSetup_esCutHandle_hh
