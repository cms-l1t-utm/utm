/**
 * @author      Takashi Matsushita
 * Created:     12 Mar 2015
 */

#ifndef tmEventSetup_esCutValue_hh
#define tmEventSetup_esCutValue_hh

#include <limits>

namespace tmeventsetup
{

/**
 *  This class implements data structure for CutValue
 */
struct esCutValue
{
  esCutValue()
    : value(std::numeric_limits<double>::max()),
      index(std::numeric_limits<unsigned int>::max()),
      version(0) { };

  virtual ~esCutValue() = default;

  double value;               /**< cut value */
  unsigned int index;         /**< HW index for the cut value */
  unsigned int version;
};

} // namespace tmeventsetup

#endif // tmEventSetup_esCutValue_hh
