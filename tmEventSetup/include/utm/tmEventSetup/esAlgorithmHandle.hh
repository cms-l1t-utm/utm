/**
 * @author      Takashi Matsushita
 * Created:     13 Nov 2015
 */

#ifndef tmEventSetup_esAlgorithmHandle_hh
#define tmEventSetup_esAlgorithmHandle_hh

#include "tmEventSetup/esAlgorithm.hh"

#include "tmTable/tmTable.hh"

namespace tmeventsetup
{

/**
 *  This class implements data structure for Algorithm
 */
class esAlgorithmHandle : public esAlgorithm
{
  public:
    esAlgorithmHandle() = default;

    esAlgorithmHandle(const tmtable::Row& algorithm);

    virtual ~esAlgorithmHandle() = default;

    /** set expression_in_condition_ from the token to condition name map
     * @param map [in]  look-up table: <key, value> = <token, condition name>
     */
    void setExpressionInCondition(const std::map<std::string, std::string>& items);

#if defined(SWIG)
    void setLabels(const std::set<std::string>& labels);
#endif
};

} // namespace tmeventsetup

#endif // tmEventSetup_esAlgorithmHandle_hh
