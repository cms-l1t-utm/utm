/**
 * @author      Takashi Matsushita
 * Created:     14 Nov 2015
 */

#ifndef tmEventSetup_esTriggerMenuHandle_hh
#define tmEventSetup_esTriggerMenuHandle_hh

#include "tmEventSetup/esTriggerMenu.hh"

#include "tmEventSetup/esCutValue.hh"
#include "tmGrammar/Function.hh"
#include "tmTable/tmTable.hh"

namespace tmeventsetup
{

// Get grammar version
const std::string& getGrammarVersion();

/**
 *  This class implements data structure for TriggerMenu
 */
class esTriggerMenuHandle : public esTriggerMenu
{
  public:
    esTriggerMenuHandle() = default;

    esTriggerMenuHandle(const tmtable::Menu& menu,
                        const tmtable::Scale& scale,
                        const tmtable::ExtSignal& extSignal);


    virtual ~esTriggerMenuHandle() = default;

    /** verify grammar version
     *
     * @param version [in] grammar version
     * @throws runtime exception if version > supported version
     */
    void verifyVersion(const std::string& version) const;

    /** parse algorithm expression
     *
     * @param expression [in] algorithm expression
     * @return copy of tokens in algorithm expression
     */
    std::vector<std::string> parseExpression(const std::string& expression) const;

    /** set HW index for instances of esCutValue
     *
     * @param bins [in] list of bins for scales (Scale.bins)
     */
    void setHwIndex(const tmtable::StringTableMap& bins);

    /** set fixed-point cut values for delta-R/invariant-Mass cuts
     *
     */
    void setFunctionCuts();

    /** set values in algorithm_map_
     *
     * @param algorithm [in] tmtable::Row instance of algorithm
     */
    void setAlgorithmMap(const tmtable::Row& algorithm);

    /** set values in condition_map_
     *
     * @param token [in] token of algorithm expression
     * @param cuts [in] list of cuts used in the given algorithm
     */
    void setConditionMap(const std::string& token,
                         const tmtable::Table& cuts);

    /** set values in scale_map_
     *
     * @param cuts [in] list of cuts used in the given algorithm
     */
    void setScaleMap(const tmtable::Scale& scale);

    /** set values in external_map_
     *
     * @param map [in] external signal mapping
     */
    void setExternalMap(const tmtable::ExtSignal& map);

  private:
    /** get condition
     *
     * @param token [in] token of algorithm expression in grammar
     * @param cuts [in] list of cuts used in algorithm
     */
    esCondition getCondition(const std::string& token,
                             const tmtable::Table& cuts) const;

    /** get object condition
     *
     * @param item [in] object item
     * @param cuts [in] list of cuts used in algorithm
     */
    esCondition getObjectCondition(const Object::Item& item,
                                   const tmtable::Table& cuts) const;
    /** get function condition
     *
     * @param item [in] function item
     * @param cuts [in] list of cuts used in algorithm
     */
    esCondition getFunctionCondition(const Function::Item& item,
                                     const tmtable::Table& cuts) const;
    /** get multi-object condition
     *
     * @param token [in] token of algorithm expression in grammar
     * @param cuts [in] list of cuts used in algorithm
     */
    esCondition getCombCondition(const Function::Item& item,
                                 const tmtable::Table& cuts) const;

    /** get distance (correlation) condition
     *
     * @param token [in] token of algorithm expression in grammar
     * @param cuts [in] list of cuts used in algorithm
     */
    esCondition getDistCondition(const Function::Item& item,
                                 const tmtable::Table& cuts) const;

    /** get distance (correlation) condition
     *
     * @param token [in] token of algorithm expression in grammar
     * @param cuts [in] list of cuts used in algorithm
     */
    esCondition getDistOverlapRemovalCondition(const Function::Item& item,
                                               const tmtable::Table& cuts) const;

    /** get invariant/transverse mass condition
     *
     * @param token [in] token of algorithm expression in grammar
     * @param cuts [in] list of cuts used in algorithm
     */
    esCondition getMassCondition(const Function::Item& item,
                                 const tmtable::Table& cuts) const;

    /** get invariant mass for unconstrained pt condition
     *
     * @param token [in] token of algorithm expression in grammar
     * @param cuts [in] list of cuts used in algorithm
     */
    esCondition getMassUptCondition(const Function::Item& item,
                                    const tmtable::Table& cuts) const;

    /** get invariant mass divided by delta R condition
     *
     * @param token [in] token of algorithm expression in grammar
     * @param cuts [in] list of cuts used in algorithm
     */
    esCondition getMassDeltaRCondition(const Function::Item& item,
                                       const tmtable::Table& cuts) const;

    /** get invariant/transverse mass condition
     *
     * @param token [in] token of algorithm expression in grammar
     * @param cuts [in] list of cuts used in algorithm
     */
    esCondition getMassOverlapRemovalCondition(const Function::Item& item,
                                               const tmtable::Table& cuts) const;

    /** get single/combination overlap removal condition
     */
    esCondition getOverlapRemovalCondition(const Function::Item& item,
                                           const tmtable::Table& cuts) const;


    const std::string& getInstanceName(const std::string& token, const esCondition& condition);

    /** get name of condition
     *
     * @param type [in] type of condition
     * @return character array representing object name
     */
    const std::string& getConditionName(esConditionType type) const;

    /** get HW index of a bin
     *
     * @param cut [in] esCutValue with value set
     * @param range [in] "minimum" or "maximum"
     * @param bins [in] tmtable::Table instance for scale
     * @return HW index of a bin
     */
    unsigned int getIndex(const esCutValue& cut,
                          const std::string& key,
                          const tmtable::Table& bins) const;

    const std::string& getPrefix4Precision(const std::vector<esObject>& objects);
};

} // namespace tmeventsetup

#endif // tmEventSetup_esTriggerMenuHandle_hh
