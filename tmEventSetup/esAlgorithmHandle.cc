#include "tmEventSetup/esAlgorithmHandle.hh"

#include "tmTable/tmTable.hh"

#include "tmUtil/tmUtil.hh"

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

namespace tmeventsetup
{

// keys for tables
const std::string kName{"name"};
const std::string kIndex{"index"};
const std::string kModuleId{"module_id"};
const std::string kModuleIndex{"module_index"};
const std::string kExpression{"expression"};
const std::string kLabels{"labels"};


size_t
replaceInExpression(std::string& expression, const std::string& from, const std::string& to)
{
  size_t count = 0;
  size_t pos = 0;
  while ((pos = expression.find(from, pos)) != std::string::npos)
  {
    bool replace = true;
    const auto end = pos + from.length();
    if (end < expression.size())
    {
      switch (expression.at(end))
      {
        case ' ': break;
        case ')': break;
        default: replace = false; break;
      }
    }
    if (replace)
    {
      expression.replace(pos, from.length(), to);
      ++count;
    }
    pos += to.length();
    if (pos >= expression.size()) break;
  }
  return count;
}


esAlgorithmHandle::esAlgorithmHandle(const tmtable::Row& algorithm)
{
  name_ = tmtable::get(algorithm, kName);
  expression_ = tmtable::get(algorithm, kExpression);
  boost::trim(expression_);
  boost::replace_all(expression_, "\r", "");
  boost::replace_all(expression_, "\n", "");

  index_ = boost::lexical_cast<unsigned int>(tmtable::get(algorithm, kIndex));
  module_id_ = boost::lexical_cast<unsigned int>(tmtable::get(algorithm, kModuleId));
  module_index_ = boost::lexical_cast<unsigned int>(tmtable::get(algorithm, kModuleIndex));

#if defined(SWIG)
  if (tmtable::has(algorithm, kLabels))
  {
    setLabels(tmutil::splitLabels(tmtable::get(algorithm, kLabels)));
  }
#endif
}


void
esAlgorithmHandle::setExpressionInCondition(const std::map<std::string, std::string>& items)
{
  TM_LOG_DBG("");
  expression_in_condition_ = expression_;
  for (const auto& item: items)
  {
    TM_LOG_DBG(TM_QUOTE(item.first) << " : " << TM_QUOTE(item.second));
    replaceInExpression(expression_in_condition_, item.first, item.second);
  }
}


#if defined(SWIG)
void
esAlgorithmHandle::setLabels(const std::set<std::string>& labels)
{
  labels_ = labels;
}
#endif

} // namespace tmeventsetup
