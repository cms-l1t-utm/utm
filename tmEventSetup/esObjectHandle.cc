#include "tmEventSetup/esObjectHandle.hh"

#include "tmEventSetup/tmEventSetup.hh"
#include "tmEventSetup/esCutHandle.hh"
#include "tmGrammar/Cut.hh"
#include "tmUtil/tmUtil.hh"

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/replace.hpp>

namespace tmeventsetup
{

// keys for tables
const std::string kName{"name"};
const std::string kObject{"object"};
const std::string kType{"type"};
const std::string kMinimum{"minimum"};
const std::string kMaximum{"maximum"};
const std::string kData{"data"};
const std::string kSeparator{"-"};
const std::string kSuffixSeparator{"_"};
const std::string kValueFormat{"%+23.16E"};

const std::map<std::string, esObjectType> ObjectTypeMap {
  {Object::MU, Muon},
  {Object::EG, Egamma},
  {Object::TAU, Tau},
  {Object::JET, Jet},
  {Object::ETT, ETT},
  {Object::HTT, HTT},
  {Object::ETM, ETM},
  {Object::HTM, HTM},
  {Object::MBT0HFP, MBT0HFP},
  {Object::MBT1HFP, MBT1HFP},
  {Object::MBT0HFM, MBT0HFM},
  {Object::MBT1HFM, MBT1HFM},
  {Object::TOWERCOUNT, TOWERCOUNT},
  {Object::ASYMET, ASYMET},
  {Object::ASYMHT, ASYMHT},
  {Object::ASYMETHF, ASYMETHF},
  {Object::ASYMHTHF, ASYMHTHF},
  {Object::CENT0, CENT0},
  {Object::CENT1, CENT1},
  {Object::CENT2, CENT2},
  {Object::CENT3, CENT3},
  {Object::CENT4, CENT4},
  {Object::CENT5, CENT5},
  {Object::CENT6, CENT6},
  {Object::CENT7, CENT7},
  {Object::MUS0, MUS0},
  {Object::MUS1, MUS1},
  {Object::MUS2, MUS2},
  {Object::MUSOOT0, MUSOOT0},
  {Object::MUSOOT1, MUSOOT1},
  {Object::ADT, ADT},
  {Object::ETTEM, ETTEM},
  {Object::ETMHF, ETMHF},
  {Object::HTMHF, HTMHF},
  {Object::ZDCP, ZDCP},
  {Object::ZDCM, ZDCM},
  {Object::AXO, Axol1tl},
  {Object::TOPO, Topological},
  {Object::CICADA, Cicada},
  {Object::EXT, EXT}
};

const std::map<esObjectType, std::string> ThresholdTypeMap {
  {Muon, ET_THR},
  {Egamma, ET_THR},
  {Tau, ET_THR},
  {Jet, ET_THR},
  {ETT, ET_THR},
  {HTT, ET_THR},
  {ETM, ET_THR},
  {HTM, ET_THR},
  {ETTEM, ET_THR},
  {ETMHF, ET_THR},
  {HTMHF, ET_THR},
  {MBT0HFP, COUNT},
  {MBT1HFP, COUNT},
  {MBT0HFM, COUNT},
  {MBT1HFM, COUNT},
  {TOWERCOUNT, COUNT},
  {ASYMET, COUNT},
  {ASYMHT, COUNT},
  {ASYMETHF, COUNT},
  {ASYMHTHF, COUNT},
  {ZDCP, COUNT},
  {ZDCM, COUNT},
};

const std::map<std::string, esComparisonOperator> ComparisonOperatorMap{
  {Object::GE, GE},
  {Object::NE, NE},
  {Object::EQ, EQ},
};


esObjectType
toObjectType(const std::string& type)
{
  const auto cit = ObjectTypeMap.find(type);
  if (cit == std::end(ObjectTypeMap))
  {
    TM_FATAL_ERROR("no such object type: " << TM_QUOTE(type));
  }
  return cit->second;
}


esComparisonOperator
toComparisonOperatorType(const std::string& type)
{
  const auto cit = ComparisonOperatorMap.find(type);
  if (cit == std::end(ComparisonOperatorMap))
  {
    TM_FATAL_ERROR("no such comparison operator type: " << TM_QUOTE(type));
  }
  return cit->second;
}


bool
isThresholdType(esObjectType type)
{
  return ThresholdTypeMap.find(type) != std::end(ThresholdTypeMap);
}


template<typename T>
std::string
formatValue(const T& value)
{
  return boost::str(boost::format(kValueFormat) % value);
}


esObjectHandle::esObjectHandle(const Object::Item& item)
{
  name_ = item.getObjectName();

  auto token = item.name;

  // TODO
  if (token.rfind(Object::EXT, 0) == 0)
  {
    token = Object::EXT;
  }

  type_ = toObjectType(token);
  bx_offset_ = boost::lexical_cast<int>(item.bx_offset);

  if (type_ == EXT)
  {
    ext_signal_name_ = getExternalName(name_);
  }
  else if (isThresholdType(static_cast<esObjectType>(type_)))
  {
    comparison_operator_ = toComparisonOperatorType(item.comparison);

    threshold_ = Cut::parseThreshold(item.threshold);
    // Set proper cut type for object.
    const auto& cutType = getThresholdType();

    // Create table for threshold cut.
    const tmtable::Row cut{
      {kName, item.name + kSeparator + cutType + kSuffixSeparator + item.threshold}, // MU-ET_1p2
      {kObject, item.name},
      {kType, item.name + kSeparator + cutType},
      {kMinimum, formatValue(threshold_)},
      {kMaximum, ""},
      {kData, ""},
    };

    // Create threshold cut from table.
    addCut(esCutHandle(cut));
  }
}


esObjectHandle::esObjectHandle(const Object::Item& item, const tmtable::Table& cuts)
: esObjectHandle(item)
{
  for (const auto& item_cut: item.cuts)
  {
    for (const auto& algo_cut: cuts)
    {
      tmtable::Row& cut = const_cast<tmtable::Row&>(algo_cut);
      if (item_cut != cut[kName]) continue;

      // add object name if it is not included in the cut["type"]
      if (cut[kType].find(kSeparator) == std::string::npos)
      {
        cut[kType] = item.name + kSeparator + cut[kType];
      }
      addCut(esCutHandle(cut));
    }
  }
}


void
esObjectHandle::setExternalChannelId(const std::map<std::string, unsigned int>& items)
{
  if (type_ == EXT)
  {
    const auto cit = items.find(ext_signal_name_);
    if (cit == std::end(items))
    {
      TM_FATAL_ERROR("no such external signal name: " << TM_QUOTE(ext_signal_name_));
    }

    ext_channel_id_ = cit->second;
  }
}


void
esObjectHandle::addCut(const esCut& cut)
{
  cuts_.emplace_back(cut);
}


const std::string&
esObjectHandle::getThresholdType() const
{
  const auto key = static_cast<esObjectType>(type_);
  const auto cit = ThresholdTypeMap.find(key);
  if (cit == std::end(ThresholdTypeMap))
  {
    TM_FATAL_ERROR("no thresold for type: " << TM_QUOTE(type_));
  }
  return cit->second;
}

} // namespace tmeventsetup
