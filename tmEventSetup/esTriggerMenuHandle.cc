#include "tmEventSetup/esTriggerMenuHandle.hh"

#include "tmEventSetup/esBinHandle.hh"
#include "tmEventSetup/esScaleHandle.hh"
#include "tmEventSetup/esCutHandle.hh"
#include "tmEventSetup/esObjectHandle.hh"
#include "tmEventSetup/esConditionHandle.hh"
#include "tmEventSetup/esAlgorithmHandle.hh"
#include "tmEventSetup/tmEventSetup.hh"

#include "tmGrammar/Cut.hh"
#include "tmGrammar/Function.hh"
#include "tmGrammar/Algorithm.hh"

#include "tmUtil/tmUtil.hh"

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <cmath>
#include <cstdio>
#include <locale>
#include <utility>
#include <sstream>

namespace tmeventsetup
{

// TODO: better to define with tmGrammar?
const std::string GRAMMAR_VERSION{"0.13"};

// keys for menu tables
const std::string kGrammarVersion{"grammar_version"};
const std::string kName{"name"};
const std::string kComment{"comment"};
const std::string kDateTime{"datetime"};
const std::string kUUIDMenu{"uuid_menu"};
const std::string kUUIDFirmware{"uuid_firmware"};
const std::string kNModules{"n_modules"};
const std::string kExpression{"expression"};

// keys for tables
const std::string kChannel{"channel"};
const std::string kNumber{"number"};
const std::string kMinimum{"minimum"};
const std::string kMaximum{"maximum"};
const std::string kValueFormat{"%+23.16E"};

// keys for conditions
const std::string kInstanceSeparator{"_i"};

const size_t MaxCombObjects{4};
const size_t MaxCombMultiObjects{12};

// Retain old names for backward compatibility
const std::map<esConditionType, std::string> ConditionNameMap{
  {SingleMuon, "SingleMU"},
  {DoubleMuon, "DoubleMU"},
  {TripleMuon, "TripleMU"},
  {QuadMuon, "QuadMU"},
  {SingleEgamma, "SingleEG"},
  {DoubleEgamma, "DoubleEG"},
  {TripleEgamma, "TripleEG"},
  {QuadEgamma, "QuadEG"},
  {SingleTau, "SingleTAU"},
  {DoubleTau, "DoubleTAU"},
  {TripleTau, "TripleTAU"},
  {QuadTau, "QuadTAU"},
  {SingleJet, "SingleJET"},
  {DoubleJet, "DoubleJET"},
  {TripleJet, "TripleJET"},
  {QuadJet, "QuadJET"},
  {TotalEt, "SingleETT"},
  {TotalHt, "SingleHTT"},
  {MissingEt, "SingleETM"},
  {MissingHt, "SingleHTM"},
  {Externals, "SingleEXT"},
  {MuonMuonCorrelation, "MuonMuonCorrelation"},
  {MuonEsumCorrelation, "MuonEsumCorrelation"},
  {CaloMuonCorrelation, "CaloMuonCorrelation"},
  {CaloCaloCorrelation, "CaloCaloCorrelation"},
  {CaloEsumCorrelation, "CaloEsumCorrelation"},
  {InvariantMass, "InvariantMass"},
  {MinBiasHFP0, "SingleMBT0HFP"},
  {MinBiasHFP1, "SingleMBT1HFP"},
  {MinBiasHFM0, "SingleMBT0HFM"},
  {MinBiasHFM1, "SingleMBT1HFM"},
  {TotalEtEM, "SingleETEM"},
  {MissingEtHF, "SingleETMHF"},
  {MissingHtHF, "SingleHTMHF"},
  {TowerCount, "SingleTOWERCOUNT"},
  {TransverseMass, "TransverseMass"},
  {SingleEgammaOvRm, "SingleEgammaOvRm"},
  {DoubleEgammaOvRm, "DoubleEgammaOvRm"},
  {TripleEgammaOvRm, "TripleEgammaOvRm"},
  {QuadEgammaOvRm, "QuadEgammaOvRm"},
  {SingleTauOvRm, "SingleTauOvRm"},
  {DoubleTauOvRm, "DoubleTauOvRm"},
  {TripleTauOvRm, "TripleTauOvRm"},
  {QuadTauOvRm, "QuadTauOvRm"},
  {SingleJetOvRm, "SingleJetOvRm"},
  {DoubleJetOvRm, "DoubleJetOvRm"},
  {TripleJetOvRm, "TripleJetOvRm"},
  {QuadJetOvRm, "QuadJetOvRm"},
  {CaloCaloCorrelationOvRm, "CaloCaloCorrelationOvRm"},
  {InvariantMassOvRm, "InvariantMassOvRm"},
  {TransverseMassOvRm, "TransverseMassOvRm"},
  {AsymmetryEt, "SingleASYMET"},
  {AsymmetryHt, "SingleASYMHT"},
  {AsymmetryEtHF, "SingleASYMETHF"},
  {AsymmetryHtHF, "SingleASYMHTHF"},
  {Centrality0, "SingleCENT0"},
  {Centrality1, "SingleCENT1"},
  {Centrality2, "SingleCENT2"},
  {Centrality3, "SingleCENT3"},
  {Centrality4, "SingleCENT4"},
  {Centrality5, "SingleCENT5"},
  {Centrality6, "SingleCENT6"},
  {Centrality7, "SingleCENT7"},
  {InvariantMass3, "InvariantMass3"},
  {InvariantMassDeltaR, "InvariantMassDeltaR"},
  {InvariantMassUpt, "InvariantMassUpt"},
  {MuonShower0, "MuonShower0"},
  {MuonShower1, "MuonShower1"},
  {MuonShower2, "MuonShower2"},
  {MuonShowerOutOfTime0, "MuonShowerOutOfTime0"},
  {MuonShowerOutOfTime1, "MuonShowerOutOfTime1"},
  {AnomalyDetectionTrigger, "AnomalyDetectionTrigger"},
  {ZDCPlus, "ZDCPlus"},
  {ZDCMinus, "ZDCMinus"},
  {Axol1tlTrigger, "Axol1tlTrigger"},
  {TopologicalTrigger, "TopologicalTrigger"},
  {CicadaTrigger, "CicadaTrigger"},
  {MultiEgamma, "MultiEG"},
  {MultiJet, "MultiJET"},
  {MultiTau, "MultiTAU"},
};

const std::map<std::pair<esObjectType, esObjectType>, std::string> PrecisionPrefixMap{
  {{Muon, Muon}, "PRECISION-MU-MU-"},
  {{Muon, Egamma}, "PRECISION-EG-MU-"},
  {{Muon, Tau}, "PRECISION-TAU-MU-"},
  {{Muon, Jet}, "PRECISION-JET-MU-"},
  {{Muon, ETM}, "PRECISION-MU-ETM-"},
  {{Muon, HTM}, "PRECISION-MU-HTM-"},
  {{Muon, ETMHF}, "PRECISION-MU-ETMHF-"},
  {{Muon, HTMHF}, "PRECISION-MU-HTMHF-"},
  {{Egamma, Muon}, "PRECISION-EG-MU-"},
  {{Egamma, Egamma}, "PRECISION-EG-EG-"},
  {{Egamma, Tau}, "PRECISION-EG-TAU-"},
  {{Egamma, Jet}, "PRECISION-EG-JET-"},
  {{Egamma, ETM}, "PRECISION-EG-ETM-"},
  {{Egamma, HTM}, "PRECISION-EG-HTM-"},
  {{Egamma, ETMHF}, "PRECISION-EG-ETMHF-"},
  {{Egamma, HTMHF}, "PRECISION-EG-HTMHF-"},
  {{Tau, Muon}, "PRECISION-TAU-MU-"},
  {{Tau, Egamma}, "PRECISION-EG-TAU-"},
  {{Tau, Tau}, "PRECISION-TAU-TAU-"},
  {{Tau, Jet}, "PRECISION-JET-TAU-"},
  {{Tau, ETM}, "PRECISION-TAU-ETM-"},
  {{Tau, HTM}, "PRECISION-TAU-HTM-"},
  {{Tau, ETMHF}, "PRECISION-TAU-ETMHF-"},
  {{Tau, HTMHF}, "PRECISION-TAU-HTMHF-"},
  {{Jet, Muon}, "PRECISION-JET-MU-"},
  {{Jet, Egamma}, "PRECISION-EG-JET-"},
  {{Jet, Tau}, "PRECISION-JET-TAU-"},
  {{Jet, Jet}, "PRECISION-JET-JET-"},
  {{Jet, ETM}, "PRECISION-JET-ETM-"},
  {{Jet, HTM}, "PRECISION-JET-HTM-"},
  {{Jet, ETMHF}, "PRECISION-JET-ETMHF-"},
  {{Jet, HTMHF}, "PRECISION-JET-HTMHF-"},
  {{ETM, Muon}, "PRECISION-MU-ETM-"},
  {{ETM, Egamma}, "PRECISION-EG-ETM-"},
  {{ETM, Tau}, "PRECISION-TAU-ETM-"},
  {{ETM, Jet}, "PRECISION-JET-ETM-"},
  {{HTM, Muon}, "PRECISION-MU-HTM-"},
  {{HTM, Egamma}, "PRECISION-EG-HTM-"},
  {{HTM, Tau}, "PRECISION-TAU-HTM-"},
  {{HTM, Jet}, "PRECISION-JET-HTM-"},
  {{ETMHF, Muon}, "PRECISION-MU-ETMHF-"},
  {{ETMHF, Egamma}, "PRECISION-EG-ETMHF-"},
  {{ETMHF, Tau}, "PRECISION-TAU-ETMHF-"},
  {{ETMHF, Jet}, "PRECISION-JET-ETMHF-"},
  {{HTMHF, Muon}, "PRECISION-MU-HTMHF-"},
  {{HTMHF, Egamma}, "PRECISION-EG-HTMHF-"},
  {{HTMHF, Tau}, "PRECISION-TAU-HTMHF-"},
  {{HTMHF, Jet}, "PRECISION-JET-HTMHF-"},
  {{Cicada, Cicada}, "PRECISION-CICADA-"},
};


const std::string&
getGrammarVersion()
{
  return GRAMMAR_VERSION;
}


const std::string&
toPrecisionPrefix(esObjectType type)
{
  const auto cit = PrecisionPrefixMap.find({type, type});
  if (cit == std::end(PrecisionPrefixMap))
  {
    TM_FATAL_ERROR("no such precision object type: " << type);
  }
  return cit->second;
}


const std::string&
toPrecisionPrefix(esObjectType type1, esObjectType type2)
{
  const auto cit = PrecisionPrefixMap.find({type1, type2});
  if (cit == std::end(PrecisionPrefixMap))
  {
    TM_FATAL_ERROR("no such precision object type: (" << type1 << ", " << type2 << ")");
  }
  return cit->second;
}


template<typename T>
std::string
formatValue(const T& value)
{
  return boost::str(boost::format(kValueFormat) % value);
}


/** Returns const refrence to scale table or throws a std::runtime_error if
 * the required scale does not exist.
 */
const tmtable::Table&
getScaleTable(const tmtable::StringTableMap& bins, const std::string& key)
{
    const auto cit = bins.find(key);

    if (cit == std::end(bins))
    {
      TM_FATAL_ERROR("no such scale set: " << TM_QUOTE(key));
    }

    return cit->second;
}


bool
isThresholdType(esCutType type)
{
  switch (type)
  {
    case Threshold:
    case Count:
      return true;
    default:
      return false;
  }
}


bool
isWindowType(esCutType type)
{
  switch (type)
  {
    case UnconstrainedPt:
    case Eta:
    case Phi:
    case Index:
      return true;
    default:
      return false;
  }
}


bool
isScaleStub(esObjectType type)
{
  switch (type)
  {
    case Precision:
    case Cicada:
      return true;
    default:
      return false;
  }
}


esTriggerMenuHandle::esTriggerMenuHandle(const tmtable::Menu& menu,
                                         const tmtable::Scale& scale,
                                         const tmtable::ExtSignal& extSignal)
{
  TM_LOG_DBG("");

  const auto name = tmtable::get(menu.menu, kName);
  const auto version = tmtable::get(menu.menu, kGrammarVersion);
  const auto comment = tmtable::get(menu.menu, kComment, "");
  const auto dateTime = tmtable::get(menu.menu, kDateTime, "");
#if defined(SWIG)
  const auto uuidMenu = tmtable::get(menu.menu, kUUIDMenu);
#endif
  const auto uuidFirmware = tmtable::get(menu.menu, kUUIDFirmware);
  const auto nModules = boost::lexical_cast<unsigned int>(tmtable::get(menu.menu, kNModules));

  const auto scaleSetName = tmtable::get(scale.scaleSet, kName);

  verifyVersion(version);

  setExternalMap(extSignal);
  setScaleMap(scale);

  // set menu information
  setName(name);
  setVersion(version);
  setComment(comment);
  setDatetime(dateTime);
#if defined(SWIG)
  setMenuUuid(uuidMenu);
#endif
  setFirmwareUuid(uuidFirmware);
  setNmodules(nModules);

  setScaleSetName(scaleSetName);

  // set condition and algorithm maps
  for (const auto& algorithm: menu.algorithms)
  {
    const auto& name = tmtable::get(algorithm, kName);
    const auto& expression = tmtable::get(algorithm, kExpression);

    const auto cit = menu.cuts.find(name);
    static const auto empty = tmtable::Table();
    const auto& cuts = (cit != std::end(menu.cuts)) ? cit->second : empty;

    for (const auto& token: parseExpression(expression))
    {
      if (Algorithm::isGate(token)) continue;
      setConditionMap(token, cuts);
    }
    setAlgorithmMap(algorithm);
  }

  // set HW index
  setHwIndex(scale.bins);

  // set Function cuts
  setFunctionCuts();
}


void
esTriggerMenuHandle::verifyVersion(const std::string& version) const
{
  if (tmutil::Version(version) > tmutil::Version(GRAMMAR_VERSION))
  {
    TM_FATAL_ERROR("invalid grammar version in menu: " << version << ", supported grammar version is: " << GRAMMAR_VERSION);
  }
}


std::vector<std::string>
esTriggerMenuHandle::parseExpression(const std::string& expression) const
{
  Algorithm::Logic::clear();

  if (not Algorithm::parser(expression))
  {
    TM_FATAL_ERROR("bad algorithm expression: " << TM_QUOTE(expression));
  }

  return Algorithm::Logic::tokens;
}


const std::string&
esTriggerMenuHandle::getConditionName(esConditionType type) const
{
  const auto cit = ConditionNameMap.find(type);

  if (cit == std::end(ConditionNameMap))
  {
    TM_FATAL_ERROR("no such condition type: " << type);
  }

  return cit->second;
}


const std::string&
esTriggerMenuHandle::getInstanceName(const std::string& token, const esCondition& condition)
{
  // Create new instance name if not exist.
  if (not token_to_condition_.count(token))
  {
    const auto type = static_cast<esConditionType>(condition.getType());
    const auto& prefix = getConditionName(type);
    const auto instance = token_to_condition_.size();
    std::ostringstream oss;
    oss << prefix << kInstanceSeparator << instance;
    token_to_condition_.insert({token, oss.str()});
  }

  return token_to_condition_.at(token);
}


esCondition
esTriggerMenuHandle::getCondition(const std::string& token,
                                  const tmtable::Table& cuts) const
{
  if (Object::isObject(token))
  {
    Object::Item item;
    Object::parser(token, item);
    return getObjectCondition(item, cuts);
  }
  else if (Function::isFunction(token))
  {
    Function::Item item;
    Function::parser(token, item);
    return getFunctionCondition(item, cuts);
  }
  else
  {
    TM_FATAL_ERROR("bad condition token: " << TM_QUOTE(token));
  }
}


esCondition
esTriggerMenuHandle::getObjectCondition(const Object::Item& item,
                                        const tmtable::Table& cuts) const
{
  esObjectHandle objectHandle(item, cuts);
  objectHandle.setExternalChannelId(external_map_);

  esConditionHandle conditionHandle;
  conditionHandle.addObject(objectHandle);

  switch (objectHandle.getType())
  {
    case Muon: conditionHandle.setType(SingleMuon); break;
    case Egamma: conditionHandle.setType(SingleEgamma); break;
    case Tau: conditionHandle.setType(SingleTau); break;
    case Jet: conditionHandle.setType(SingleJet); break;
    case ETT: conditionHandle.setType(TotalEt); break;
    case HTT: conditionHandle.setType(TotalHt); break;
    case ETM: conditionHandle.setType(MissingEt); break;
    case HTM: conditionHandle.setType(MissingHt); break;
    case EXT: conditionHandle.setType(Externals); break;
    case MBT0HFP: conditionHandle.setType(MinBiasHFP0); break;
    case MBT1HFP: conditionHandle.setType(MinBiasHFP1); break;
    case MBT0HFM: conditionHandle.setType(MinBiasHFM0); break;
    case MBT1HFM: conditionHandle.setType(MinBiasHFM1); break;
    case ETTEM: conditionHandle.setType(TotalEtEM); break;
    case ETMHF: conditionHandle.setType(MissingEtHF); break;
    case HTMHF: conditionHandle.setType(MissingHtHF); break;
    case TOWERCOUNT: conditionHandle.setType(TowerCount); break;
    case ASYMET: conditionHandle.setType(AsymmetryEt); break;
    case ASYMHT: conditionHandle.setType(AsymmetryHt); break;
    case ASYMETHF: conditionHandle.setType(AsymmetryEtHF); break;
    case ASYMHTHF: conditionHandle.setType(AsymmetryHtHF); break;
    case CENT0: conditionHandle.setType(Centrality0); break;
    case CENT1: conditionHandle.setType(Centrality1); break;
    case CENT2: conditionHandle.setType(Centrality2); break;
    case CENT3: conditionHandle.setType(Centrality3); break;
    case CENT4: conditionHandle.setType(Centrality4); break;
    case CENT5: conditionHandle.setType(Centrality5); break;
    case CENT6: conditionHandle.setType(Centrality6); break;
    case CENT7: conditionHandle.setType(Centrality7); break;
    case MUS0: conditionHandle.setType(MuonShower0); break;
    case MUS1: conditionHandle.setType(MuonShower1); break;
    case MUS2: conditionHandle.setType(MuonShower2); break;
    case MUSOOT0: conditionHandle.setType(MuonShowerOutOfTime0); break;
    case MUSOOT1: conditionHandle.setType(MuonShowerOutOfTime1); break;
    case ADT: conditionHandle.setType(AnomalyDetectionTrigger); break;
    case ZDCP: conditionHandle.setType(ZDCPlus); break;
    case ZDCM: conditionHandle.setType(ZDCMinus); break;
    case Axol1tl: conditionHandle.setType(Axol1tlTrigger); break;
    case Topological: conditionHandle.setType(TopologicalTrigger); break;
    case Cicada: conditionHandle.setType(CicadaTrigger); break;
    default:
    {
      TM_FATAL_ERROR("no such esObjectType: " << objectHandle.getType());
    } break;
  }

  return conditionHandle;
}


esCondition
esTriggerMenuHandle::getFunctionCondition(const Function::Item& item,
                                          const tmtable::Table& cuts) const
{
  TM_LOG_DBG("-");

  switch (item.type)
  {
    case Function::Combination: return getCombCondition(item, cuts);
    case Function::CombinationOvRm: return getOverlapRemovalCondition(item, cuts);
    case Function::Distance: return getDistCondition(item, cuts);
    case Function::DistanceOvRm: return getDistOverlapRemovalCondition(item, cuts);
    case Function::InvariantMass: return getMassCondition(item, cuts);
    case Function::InvariantMassUpt: return getMassUptCondition(item, cuts);
    case Function::InvariantMassDeltaR: return getMassDeltaRCondition(item, cuts);
    case Function::InvariantMass3: return getMassCondition(item, cuts);
    case Function::InvariantMassOvRm: return getMassOverlapRemovalCondition(item, cuts);
    case Function::TransverseMass: return getMassCondition(item, cuts);
    case Function::TransverseMassOvRm: return getMassOverlapRemovalCondition(item, cuts);
    default:
    {
      TM_FATAL_ERROR("no such esFunctionType: " << TM_QUOTE(item.type));
    } break;
  }
}


esCondition
esTriggerMenuHandle::getCombCondition(const Function::Item& item,
                                      const tmtable::Table& cuts) const
{
  TM_LOG_DBG("-");

  // multi-object condition
  esConditionHandle conditionHandle;

  // populate condition with objects
  for (const auto& object: item.objects)
  {
    conditionHandle.addObject(esObjectHandle(object, cuts));
  }

  // populate condition with cuts
  for (const auto& cut: item.cuts)
  {
    for (const auto& row: cuts)
    {
      if (cut == row.at(kName))
      {
        conditionHandle.addCut(esCutHandle(row));
      }
    }
  }

  const esObjectType type = static_cast<esObjectType>(conditionHandle.getObjects().at(0).getType());

  // count of objects
  const size_t count = conditionHandle.getObjects().size();

  // Combinations double, triple, quad objects
  if (count <= MaxCombObjects)
  {
    switch (count)
    {
      case 2: // double condition
        switch (type)
        {
          case Muon: conditionHandle.setType(DoubleMuon); break;
          case Egamma: conditionHandle.setType(DoubleEgamma); break;
          case Tau: conditionHandle.setType(DoubleTau); break;
          case Jet: conditionHandle.setType(DoubleJet); break;
          default:
            TM_FATAL_ERROR("type not implemented: " << TM_QUOTE(type));
            break;
        }
        break;

      case 3: // triple condition
        switch (type)
        {
          case Muon: conditionHandle.setType(TripleMuon); break;
          case Egamma: conditionHandle.setType(TripleEgamma); break;
          case Tau: conditionHandle.setType(TripleTau); break;
          case Jet: conditionHandle.setType(TripleJet); break;
          default:
            TM_FATAL_ERROR("type not implemented: " << TM_QUOTE(type));
            break;
        }
        break;

      case 4: // quad condition
        switch (type)
        {
          case Muon: conditionHandle.setType(QuadMuon); break;
          case Egamma: conditionHandle.setType(QuadEgamma); break;
          case Tau: conditionHandle.setType(QuadTau); break;
          case Jet: conditionHandle.setType(QuadJet); break;
          default:
            TM_FATAL_ERROR("type not implemented: " << TM_QUOTE(type));
            break;
        }
        break;

      default:
        TM_FATAL_ERROR("unsupported # of objects: " << count);
        break;
    }
  }
  // Combinations of multiple objects
  else if (count <= MaxCombMultiObjects)
  {
    switch (type)
    {
      case Egamma: conditionHandle.setType(MultiEgamma); break;
      case Tau: conditionHandle.setType(MultiTau); break;
      case Jet: conditionHandle.setType(MultiJet); break;
      default:
        TM_FATAL_ERROR("type not implemented: " << TM_QUOTE(type));
        break;
    }
  }
  else
  {
    TM_FATAL_ERROR("unsupported # of objects: " << count);
  }

  return conditionHandle;
}


esCondition
esTriggerMenuHandle::getOverlapRemovalCondition(const Function::Item& item,
                                                const tmtable::Table& cuts) const
{
  TM_LOG_DBG("-");

  esConditionHandle conditionHandle;

  // populate condition with objects
  for (const auto& object: item.objects)
  {
    conditionHandle.addObject(esObjectHandle(object, cuts));
  }

  // populate condition with cuts
  for (const auto& cut: item.cuts)
  {
    for (const auto& row: cuts)
    {
      if (cut == row.at(kName))
      {
        conditionHandle.addCut(esCutHandle(row));
      }
    }
  }

  // get list of objects to process
  const std::vector<esObject>& objects = conditionHandle.getObjects();

  // get type of first object
  const esObjectType type = static_cast<esObjectType>(objects.front().getType());

  // get type of possible different object (always last one)
  const esObjectType typeReference = static_cast<esObjectType>(objects.back().getType());

  // check for appended reference object of different type
  const bool hasReference = (type != typeReference);

  // count of objects (without possible reference)
  const size_t count = (hasReference ? objects.size() - 1 : objects.size());

  // distinguish number of left side objects
  switch (count)
  {
    case 1: // SingleOverlapRemoval
      switch (type)
      {
        case Egamma: conditionHandle.setType(SingleEgammaOvRm); break;
        case Tau: conditionHandle.setType(SingleTauOvRm); break;
        case Jet: conditionHandle.setType(SingleJetOvRm); break;
        default:
          TM_FATAL_ERROR("not implemented: " << TM_QUOTE(type));
          break;
      }
      break;
    case 2: // DoubleOverlapRemoval
      switch (type)
      {
        case Egamma: conditionHandle.setType(DoubleEgammaOvRm); break;
        case Tau: conditionHandle.setType(DoubleTauOvRm); break;
        case Jet: conditionHandle.setType(DoubleJetOvRm); break;
        default:
          TM_FATAL_ERROR("not implemented: " << TM_QUOTE(type));
          break;
      }
      break;

    case 3: // TripleOverlapRemoval
      switch (type)
      {
        case Egamma: conditionHandle.setType(TripleEgammaOvRm); break;
        case Tau: conditionHandle.setType(TripleTauOvRm); break;
        case Jet: conditionHandle.setType(TripleJetOvRm); break;
        default:
          TM_FATAL_ERROR("not implemented: " << TM_QUOTE(type));
          break;
      }
      break;

    case 4: // QuadOverlapRemoval
      switch (type)
      {
        case Egamma: conditionHandle.setType(QuadEgammaOvRm); break;
        case Tau: conditionHandle.setType(QuadTauOvRm); break;
        case Jet: conditionHandle.setType(QuadJetOvRm); break;
        default:
          TM_FATAL_ERROR("not implemented: " << TM_QUOTE(type));
          break;
      }
      break;

    default:
      TM_FATAL_ERROR("unsupported # of objects: " << count);
      break;
  }

  return conditionHandle;
}


esCondition
esTriggerMenuHandle::getDistCondition(const Function::Item& item,
                                      const tmtable::Table& cuts) const
{
  TM_LOG_DBG("-");

  // distance (correlation) condition
  if (item.objects.size() != 2)
  {
    TM_FATAL_ERROR("# of objects != 2: " << item.objects.size());
  }

  esConditionHandle conditionHandle;
  for (const auto& object: item.objects)
  {
    conditionHandle.addObject(esObjectHandle(object, cuts));
  }

  size_t nCut = item.cuts.size();
  if (nCut == 0 or nCut > 3)  // deta/dphi/chgcor combination possible
  {
    TM_FATAL_ERROR("# of cuts not in range [1,3]: " << nCut);
  }

  bool hasDistCut = false;
  for (const auto& row: cuts)
  {
    for (const auto& cut: item.cuts)
    {
      if (cut == row.at(kName))
      {
        const esCutHandle cutHandle(row);
        switch (cutHandle.getCutType())
        {
          case DeltaEta:
          case DeltaPhi:
          case DeltaR:
            hasDistCut = true;
            break;
          case ChargeCorrelation:
            break;
          case TwoBodyPt:
            break;
          default:
            TM_FATAL_ERROR("unknown cut type: " << cutHandle.getCutType());
        }
        conditionHandle.addCut(std::move(cutHandle));
      }
    }
  }

  if (not hasDistCut)
  {
    TM_FATAL_ERROR("no dist cut specified");
  }


  const esObjectType type1 = static_cast<esObjectType>(conditionHandle.getObjects().at(0).getType());
  const esObjectType type2 = static_cast<esObjectType>(conditionHandle.getObjects().at(1).getType());
  const esCombinationType combination = getObjectCombination(type1, type2);

  switch (combination)
  {
    case MuonMuonCombination:
      conditionHandle.setType(MuonMuonCorrelation);
      break;

    case MuonEsumCombination:
      conditionHandle.setType(MuonEsumCorrelation);
      break;

    case CaloMuonCombination:
      conditionHandle.setType(CaloMuonCorrelation);
      break;

    case CaloCaloCombination:
      conditionHandle.setType(CaloCaloCorrelation);
      break;

    case CaloEsumCombination:
      conditionHandle.setType(CaloEsumCorrelation);
      break;

    default:
      TM_FATAL_ERROR("unknown combination type: " << combination);
  }

  return conditionHandle;
}


esCondition
esTriggerMenuHandle::getDistOverlapRemovalCondition(const Function::Item& item,
                                                    const tmtable::Table& cuts) const
{
  TM_LOG_DBG("-");

  // distance (correlation) condition with overlap removal
  if (item.objects.size() < 2 or
      item.objects.size() > 3)
  {
    TM_FATAL_ERROR("# of objects != 2 or 2+1: " << item.objects.size());
  }

  esConditionHandle conditionHandle;
  for (const auto& object: item.objects)
  {
    conditionHandle.addObject(esObjectHandle(object, cuts));
  }

  size_t nCut = item.cuts.size();
  if (nCut == 0 or nCut > 6)  // deta/dphi/chgcor/ormdeta/ormdphi/ormdr combination possible
  {
    TM_FATAL_ERROR("# of cuts not in range [1,6]: " << nCut);
  }

  bool hasDistCut = false;
  bool hasOvRmCut = false;
  for (const auto& row: cuts)
  {
    for (const auto& cut: item.cuts)
    {
      if (cut == row.at(kName))
      {
        const esCutHandle cutHandle(row);
        switch (cutHandle.getCutType())
        {
          case DeltaEta:
          case DeltaPhi:
          case DeltaR:
            hasDistCut = true;
            break;
          case OvRmDeltaEta:
          case OvRmDeltaPhi:
          case OvRmDeltaR:
            hasOvRmCut = true;
            break;
          case ChargeCorrelation:
            break;
          case TwoBodyPt:
            break;
          default:
            TM_FATAL_ERROR("unknown cut type: " << cutHandle.getCutType());
        }
        conditionHandle.addCut(std::move(cutHandle));
      }
    }
  }

  if (not hasDistCut)
  {
    TM_FATAL_ERROR("missing dist cut (mandatory)");
  }

  if (not hasOvRmCut)
  {
    TM_FATAL_ERROR("missing overlap removal cut (mandatory)");
  }

  const esObjectType type1 = static_cast<esObjectType>(conditionHandle.getObjects().at(0).getType());
  const esObjectType type2 = static_cast<esObjectType>(conditionHandle.getObjects().at(1).getType());
  const esCombinationType combination = getObjectCombination(type1, type2);

  switch (combination)
  {
    case CaloCaloCombination: // NOTE !
      conditionHandle.setType(CaloCaloCorrelationOvRm); // NOTE !
      break;

    default:
      TM_FATAL_ERROR("unknown combination type: " << combination);
  }

  return conditionHandle;
}


esCondition
esTriggerMenuHandle::getMassCondition(const Function::Item& item,
                                      const tmtable::Table& cuts) const
{
  TM_LOG_DBG("-");

  // verify number of objects
  if (item.type == Function::InvariantMass3)
  {
    if (item.objects.size() != 3)
    {
      TM_FATAL_ERROR("# of objects != 3: " << item.objects.size());
    }
  }
  else
  {
    if (item.objects.size() != 2)
    {
      TM_FATAL_ERROR("# of objects != 2: " << item.objects.size());
    }
  }

  esConditionHandle conditionHandle;

  // assign objects
  for (const auto& object: item.objects)
  {
    conditionHandle.addObject(esObjectHandle(object, cuts));
  }

  const size_t nCut = item.cuts.size();

  // TODO: ...
  if (item.type == Function::InvariantMass3)
  {
    if (nCut == 0 or nCut > 2)  // mass
    {
      TM_FATAL_ERROR("# of cuts not in range [1,2]: " << nCut);
    }
  }
  else
  {
    if (nCut == 0 or nCut > 5)  // mass/chgcor/dR|(dEta/dPhi)/tbpt
    {
      TM_FATAL_ERROR("# of cuts not in range [1,5]: " << nCut);
    }
  }

  bool hasMassCut = false;

  // assign function cuts
  for (const auto& row: cuts)
  {
    for (const auto& cut: item.cuts)
    {
      if (cut == row.at(kName))
      {
        const esCutHandle cutHandle(row);
        if (cutHandle.getCutType() == Mass) hasMassCut = true;
        conditionHandle.addCut(std::move(cutHandle));
      }
    }
  }

  if (not hasMassCut)
  {
    TM_FATAL_ERROR("no MASS cut specified");
  }

  if (item.type == Function::InvariantMass)
    conditionHandle.setType(InvariantMass);
  else if (item.type == Function::InvariantMass3)
    conditionHandle.setType(InvariantMass3);
  else if (item.type == Function::TransverseMass)
    conditionHandle.setType(TransverseMass);

  return conditionHandle;
}


esCondition
esTriggerMenuHandle::getMassUptCondition(const Function::Item& item,
                                         const tmtable::Table& cuts) const
{
  TM_LOG_DBG("-");

  // verify number of objects
  if (item.objects.size() != 2)
  {
    TM_FATAL_ERROR("# of objects != 2: " << item.objects.size());
  }

  esConditionHandle conditionHandle;

  // assign objects
  for (const auto& object: item.objects)
  {
    conditionHandle.addObject(esObjectHandle(object, cuts));
  }

  const size_t nCut = item.cuts.size();

  if (nCut == 0 or nCut > 5)  // massupt/chgcor/dR|(dEta/dPhi)/tbpt
  {
    TM_FATAL_ERROR("# of cuts not in range [1,5]: " << nCut);
  }

  bool hasMassUptCut = false;

  // assign function cuts
  for (const auto& row: cuts)
  {
    for (const auto& cut: item.cuts)
    {
      if (cut == row.at(kName))
      {
        const esCutHandle cutHandle(row);
        if (cutHandle.getCutType() == MassUpt) hasMassUptCut = true;
        conditionHandle.addCut(std::move(cutHandle));
      }
    }
  }

  if (not hasMassUptCut)
  {
    TM_FATAL_ERROR("no MASSUPT cut specified");
  }

  conditionHandle.setType(InvariantMassUpt);

  return conditionHandle;
}


esCondition
esTriggerMenuHandle::getMassDeltaRCondition(const Function::Item& item,
                                            const tmtable::Table& cuts) const
{
  TM_LOG_DBG("-");

  // verify number of objects
  if (item.objects.size() != 2)
  {
    TM_FATAL_ERROR("# of objects != 2: " << item.objects.size());
  }

  esConditionHandle conditionHandle;

  // assign objects
  for (const auto& object: item.objects)
  {
    conditionHandle.addObject(esObjectHandle(object, cuts));
  }

  size_t nCut = item.cuts.size();

  if (nCut == 0 or nCut > 5)  // mass/chgcor/dR|(dEta/dPhi)/tbpt
  {
    TM_FATAL_ERROR("# of cuts not in range [1,5]: " << nCut);
  }

  bool hasMassDeltaRCut = false;

  // assign function cuts
  for (const auto& row: cuts)
  {
    for (const auto& cut: item.cuts)
    {
      if (cut == row.at(kName))
      {
        const esCutHandle cutHandle(row);
        if (cutHandle.getCutType() == MassDeltaR) hasMassDeltaRCut = true;
        conditionHandle.addCut(std::move(cutHandle));
      }
    }
  }

  if (not hasMassDeltaRCut)
  {
    TM_FATAL_ERROR("no MASSDR cut specified");
  }

  conditionHandle.setType(InvariantMassDeltaR);

  return conditionHandle;
}


esCondition
esTriggerMenuHandle::getMassOverlapRemovalCondition(const Function::Item& item,
                                                    const tmtable::Table& cuts) const
{
  TM_LOG_DBG("-");

  // invariant-mass condition
  if (item.objects.size() < 2 or
      item.objects.size() > 3)
  {
    TM_FATAL_ERROR("# of objects != 2 or 2+1: " << item.objects.size());
  }

  esConditionHandle conditionHandle;
  for (const auto& object: item.objects)
  {
    conditionHandle.addObject(esObjectHandle(object, cuts));
  }

  size_t nCut = item.cuts.size();
  if (nCut == 0 or nCut > 7)  // mass/dR|(dEta/dPhi)/tbpt
  {
    TM_FATAL_ERROR("# of cuts not in range [1,7]: " << nCut);
  }

  bool hasMassCut = false;
  bool hasOvRmCut = false;

  for (const auto& row: cuts)
  {
    for (const auto& cut: item.cuts)
    {
      if (cut == row.at(kName))
      {
        const esCutHandle cutHandle(row);
        if (cutHandle.getCutType() == Mass) hasMassCut = true;
        if (cutHandle.getCutType() == OvRmDeltaEta) hasOvRmCut = true;
        if (cutHandle.getCutType() == OvRmDeltaPhi) hasOvRmCut = true;
        if (cutHandle.getCutType() == OvRmDeltaR) hasOvRmCut = true;
        conditionHandle.addCut(std::move(cutHandle));
      }
    }
  }

  if (not hasMassCut)
  {
    TM_FATAL_ERROR("missing mass cut (mandatory)");
  }

  if (not hasOvRmCut)
  {
    TM_FATAL_ERROR("missing overlap removal cut (mandatory)");
  }

  if (item.type == Function::InvariantMassOvRm)
    conditionHandle.setType(InvariantMassOvRm);
  else if (item.type == Function::TransverseMassOvRm)
    conditionHandle.setType(TransverseMassOvRm);

  return conditionHandle;
}


void
esTriggerMenuHandle::setConditionMap(const std::string& token,
                                     const tmtable::Table& cuts)
{
  TM_LOG_DBG(TM_VALUE_DBG(condition_map_.size()));

  auto condition = getCondition(token, cuts);

  const auto& name = getInstanceName(token, condition);
  condition.setName(name);

  condition_map_.insert({name, condition});
#if defined(SWIG)
  condition_map_p_.insert({name, &(condition_map_.at(name))});
#endif
}


void
esTriggerMenuHandle::setAlgorithmMap(const tmtable::Row& algorithm)
{
  TM_LOG_DBG(TM_VALUE_DBG(algorithm_map_.size()));

  auto algoHandle = esAlgorithmHandle{algorithm};
  algoHandle.setExpressionInCondition(token_to_condition_);
  algoHandle.setRpnVector(parseExpression(algoHandle.getExpressionInCondition()));

  const auto& name = algoHandle.getName();

  const auto cit = algorithm_map_.insert({name, algoHandle});
  if (not cit.second)
  {
    TM_LOG_ERR("failed to insert algorithm: " << TM_QUOTE(name));
  }
#if defined(SWIG)
  algorithm_map_p_.insert({name, &(algorithm_map_.at(name))});
#endif
}


void
esTriggerMenuHandle::setScaleMap(const tmtable::Scale& scale)
{
  for (const auto& scales: scale.scales)
  {
    esScaleHandle scaleHandle(scales);
    const auto& name = scaleHandle.getName();
    const auto objectType = static_cast<esObjectType>(scaleHandle.getObjectType());
    if (not isScaleStub(objectType)) // stub: scale without bins (precision, cicada)
    {
      for (const auto& bin: getScaleTable(scale.bins, name))
      {
        scaleHandle.addBin(esBinHandle(bin));
      }
      scaleHandle.sortBins();
    }
    scale_map_.insert({name, scaleHandle});
#if defined(SWIG)
    scale_map_p_.insert({name, &(scale_map_.at(name))});
#endif
  }
}


void
esTriggerMenuHandle::setExternalMap(const tmtable::ExtSignal& map)
{
  for (const auto& signal: map.extSignals)
  {
    const auto& name = tmtable::get(signal, kName);
    const auto& channel = tmtable::get(signal, kChannel);
    external_map_.insert({name, boost::lexical_cast<unsigned int>(channel)});
    TM_LOG_DBG(TM_QUOTE(name) << " : " << TM_QUOTE(channel));
  }
}


unsigned int
esTriggerMenuHandle::getIndex(const esCutValue& cut, const std::string& key, const tmtable::Table& bins) const
{
  // Convert double to string representation.
  const auto value = formatValue(cut.value);
  TM_LOG_DBG(TM_VALUE_DBG(value));
  TM_LOG_DBG(TM_VALUE_DBG(key));

  const auto match = [&key, &value](const tmtable::Row& bin){
    return tmtable::get(bin, key) == value;
  };

  // Try to find bin with matching minimum or maxmum value.
  const auto cit = std::find_if(std::begin(bins), std::end(bins), match);
  if (cit != std::end(bins))
  {
    try
    {
      const auto& number = tmtable::get(*cit, kNumber);
      return boost::lexical_cast<unsigned int>(number);
    }
    catch (boost::bad_lexical_cast&)
    {
      return std::numeric_limits<unsigned int>::max();
    }
  }

  return std::numeric_limits<unsigned int>::max();
}



void
esTriggerMenuHandle::setHwIndex(const tmtable::StringTableMap& bins)
{
  TM_LOG_DBG("");

  std::map<std::string, unsigned int> precision;
  getPrecisions(precision, scale_map_);

  for (const auto& condition: condition_map_)
  {
    for (const auto& object: condition.second.getObjects())
    {
      for (const auto& cut_: object.getCuts())
      {
        auto& cut = const_cast<esCut&>(cut_); // TODO
        const auto key = cut.getKey();
        TM_LOG_DBG(TM_VALUE_DBG(key));

        const auto type = static_cast<esCutType>(cut.getCutType());
        if (isThresholdType(type))
        {
          const auto& table = getScaleTable(bins, key);
          const auto& minimum = cut.getMinimum();
          cut.setMinimumIndex(getIndex(minimum, kMinimum, table));
        }
        else if (isWindowType(type))
        {
          const auto& table = getScaleTable(bins, key);
          const auto& minimum = cut.getMinimum();
          cut.setMinimumIndex(getIndex(minimum, kMinimum, table));
          const auto& maximum = cut.getMaximum();
          cut.setMaximumIndex(getIndex(maximum, kMaximum, table));
        }
        else if (type == esCutType::CicadaScore)
        {
          const auto value = cut.getMinimumValue();
          const auto precisionKey = toPrecisionPrefix(Cicada) + PRECISION_CSCORE;
          if (not precision.count(precisionKey))
          {
            TM_FATAL_ERROR("missing precision scale set: " << TM_QUOTE(precisionKey));
          }
          const size_t fraction = precision.at(precisionKey);
          cut.setMinimumIndex(tmutil::encodeFixedPoint<unsigned int>(value, fraction));
        }
      }
    }
  }
}


const std::string&
esTriggerMenuHandle::getPrefix4Precision(const std::vector<esObject>& objects)
{
  // Allow more than two objects to accept overlap removal conditions (third object).
  if (objects.size() < 2)
  {
    TM_FATAL_ERROR("# of objects < 2: " << objects.size());
  }
  const auto type1 = static_cast<esObjectType>(objects.at(0).getType());
  const auto type2 = static_cast<esObjectType>(objects.at(1).getType());
  return toPrecisionPrefix(type1, type2);
}


void
esTriggerMenuHandle::setFunctionCuts()
{
  std::map<std::string, unsigned int> dictionary;

  for (const auto& scale: scale_map_)
  {
    const auto& name = scale.first;
    const auto type = scale.second.getScaleType();
    const auto nBits = scale.second.getNbits();

    switch (type)
    {
      case DeltaPrecision:
      case MassPrecision:
      case TwoBodyPtPrecision:
      case OvRmDeltaPrecision:
        break;

      default:
        continue;
    }

    const auto cit = dictionary.insert({name, nBits});
    if (not cit.second)
    {
      TM_FATAL_ERROR("insertion failure: " << TM_QUOTE(name));
    }
  }

  if (dictionary.empty()) return; // hm, why bail out?


  for (const auto& condition: condition_map_)
  {
    const auto& objects = condition.second.getObjects();
    const auto& cuts = condition.second.getCuts();

    for (const auto& cut_: cuts)
    {
      auto& cut = const_cast<esCut&>(cut_); // TODO
      const auto type = cut.getCutType();
      switch (type)
      {
        case DeltaEta:
        case DeltaPhi:
        case DeltaR:
        case Mass:
        case MassUpt:
        case MassDeltaR:
        case TwoBodyPt:
        case OvRmDeltaEta:
        case OvRmDeltaPhi:
        case OvRmDeltaR:
          break;
        default:
          continue;
      }

      auto key = getPrefix4Precision(objects);

      auto minimum = cut.getMinimumValue();
      auto maximum = cut.getMaximumValue();
      unsigned int precision = 0;

      if (type == Mass or type == MassUpt or type == MassDeltaR)
      {
        key += PRECISION_MASS;
        if (not dictionary.count(key))
        {
          TM_FATAL_ERROR("missing precision scale set: " << TM_QUOTE(key));
        }
        precision = dictionary.at(key);
        // Note: calculate M^2/2 for mass
        const auto scale = tmutil::pow10(precision);
        minimum = std::floor(minimum * minimum * 0.5 * scale) / scale;
        maximum = std::ceil(maximum * maximum * 0.5 * scale) / scale;
      }
      else if (type == DeltaR)
      {
        key += PRECISION_DELTA;
        if (not dictionary.count(key))
        {
          TM_FATAL_ERROR("missing precision scale set: " << TM_QUOTE(key));
        }
        precision = dictionary.at(key);
        // Note: calculate dR^2 for dR
        const auto scale = tmutil::pow10(precision);
        minimum = std::floor(minimum * minimum * scale) / scale;
        maximum = std::ceil(maximum * maximum * scale) / scale;
      }
      else if (type == OvRmDeltaR)
      {
        key += PRECISION_OVRM_DELTA;
        if (not dictionary.count(key))
        {
          TM_FATAL_ERROR("missing precision scale set: " << TM_QUOTE(key));
        }
        precision = dictionary.at(key);
        // Note: calculate dR^2 for dR
        const auto scale = tmutil::pow10(precision);
        minimum = std::floor(minimum * minimum * scale) / scale;
        maximum = std::ceil(maximum * maximum * scale) / scale;
      }
      else if (type == TwoBodyPt)
      {
        key += PRECISION_TBPT;
        if (not dictionary.count(key))
        {
          TM_FATAL_ERROR("missing precision scale set: " << TM_QUOTE(key));
        }
        precision = dictionary.at(key);
        const auto scale = tmutil::pow10(precision);
        minimum = std::floor(minimum * minimum * scale) / scale;
        // TODO: actually this is not used for two body pt threshold
        maximum = std::ceil(maximum * maximum * scale) / scale;
      }
      else if (type == DeltaEta or type == DeltaPhi)
      {
        key += PRECISION_DELTA;
        if (not dictionary.count(key))
        {
          TM_FATAL_ERROR("missing precision scale set: " << TM_QUOTE(key));
        }
        precision = dictionary.at(key);
        const auto scale = tmutil::pow10(precision);
        minimum = std::floor(minimum * scale) / scale;
        maximum = std::ceil(maximum * scale) / scale;
      }
      else if (type == OvRmDeltaEta or type == OvRmDeltaPhi)
      {
        key += PRECISION_OVRM_DELTA;
        if (not dictionary.count(key))
        {
          TM_FATAL_ERROR("missing precision scale set: " << TM_QUOTE(key));
        }
        precision = dictionary.at(key);
        const auto scale = tmutil::pow10(precision);
        minimum = std::floor(minimum * scale) / scale;
        maximum = std::ceil(maximum * scale) / scale;
      }
      else
      {
        TM_FATAL_ERROR("unsupported cut " << TM_QUOTE(cut.getName()) << " of type: " << type);
      }

      cut.setMinimumValue(minimum);
      cut.setMaximumValue(maximum);
      cut.setPrecision(precision); // TODO set both minimum/maximum index
    } // for cut
  } // for condition
}

} // namespace tmeventsetup
