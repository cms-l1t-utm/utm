#include "tmEventSetup/esScaleHandle.hh"
#include "tmGrammar/Cut.hh"
#include "tmGrammar/Object.hh"
#include "tmUtil/tmUtil.hh"

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

#include <algorithm>
#include <map>

namespace tmeventsetup
{

// keys for tables
const std::string kObject{"object"};
const std::string kType{"type"};
const std::string kMinimum{"minimum"};
const std::string kMaximum{"maximum"};
const std::string kStep{"step"};
const std::string kNBits{"n_bits"};

// keys for scale names
const std::string kSeparator{"-"};

const std::map<std::string, esObjectType> ScaleObjectTypeMap {
  {Object::MU, Muon},
  {Object::EG, Egamma},
  {Object::TAU, Tau},
  {Object::JET, Jet},
  {Object::HTT, HTT},
  {Object::ETT, ETT},
  {Object::ETM, ETM},
  {Object::HTM, HTM},
  {Object::MBT0HFP, MBT0HFP},
  {Object::MBT1HFP, MBT1HFP},
  {Object::MBT0HFM, MBT0HFM},
  {Object::MBT1HFM, MBT1HFM},
  {Object::ETTEM, ETTEM},
  {Object::ETMHF, ETMHF},
  {Object::HTMHF, HTMHF},
  {Object::TOWERCOUNT, TOWERCOUNT},
  {Object::ASYMET, ASYMET},
  {Object::ASYMHT, ASYMHT},
  {Object::ASYMETHF, ASYMETHF},
  {Object::ASYMHTHF, ASYMHTHF},
  {Object::ZDCP, ZDCP},
  {Object::ZDCM, ZDCM},
  {Object::CICADA, Cicada},
  {PRECISION, Precision}
};

const std::map<std::string, esScaleType> ScaleTypeMap {
  {ET_THR, EtScale},
  {Cut::UPT, UnconstrainedPtScale},
  {Cut::ETA, EtaScale},
  {Cut::PHI, PhiScale},
  {Cut::INDEX, IndexScale},
  {COUNT, CountScale},
  {Cut::CSCORE, CScoreScale},
  {PRECISION_DELTA, DeltaPrecision},
  {PRECISION_OVRM_DELTA, OvRmDeltaPrecision},
  {PRECISION_MASS, MassPrecision},
  {PRECISION_MASSPT, MassPtPrecision},
  {PRECISION_MATH, MathPrecision},
  {PRECISION_TBPT, TwoBodyPtPrecision},
  {PRECISION_TBPT_MATH, TwoBodyPtMathPrecision},
  {PRECISION_INVERSE_DR, InverseDeltaRPrecision},
  {PRECISION_CSCORE, CScorePrecision}
};


esObjectType
toScaleObjectType(const std::string& object)
{
  const auto cit = ScaleObjectTypeMap.find(object);
  if (cit == std::end(ScaleObjectTypeMap))
  {
    TM_FATAL_ERROR("no such scale object: " << TM_QUOTE(object));
  }
  return cit->second;
}


esScaleType
toScaleType(const std::string& type)
{
  const auto cit = ScaleTypeMap.find(type);
  if (cit == std::end(ScaleTypeMap))
  {
    TM_FATAL_ERROR("no such scale type: " << TM_QUOTE(type));
  }
  return cit->second;
}


std::string
toScaleName(const std::string& object, const std::string& type)
{
  return object + kSeparator + type;
}


/** Return search key for scale types mapping.
 * eg. "MU-MU-Math" -> "Math"
 */
std::string
getScaleTypeKey(const std::string& type)
{
  const auto pos = type.find_last_of(kSeparator);
  if (pos != std::string::npos)
  {
    return type.substr(pos + 1);
  }
  return type;
}


esScaleHandle::esScaleHandle(const tmtable::Row& scale)
{
  const auto& object = tmtable::get(scale, kObject);
  object_ = toScaleObjectType(object);

  const auto& type = tmtable::get(scale, kType);
  type_ = toScaleType(getScaleTypeKey(type));

  name_ = toScaleName(object, type);
  minimum_ = boost::lexical_cast<double>(tmtable::get(scale, kMinimum));
  maximum_ = boost::lexical_cast<double>(tmtable::get(scale, kMaximum));
  step_ = boost::lexical_cast<double>(tmtable::get(scale, kStep));
  n_bits_ = boost::lexical_cast<unsigned int>(tmtable::get(scale, kNBits));
}


void
esScaleHandle::addBin(const esBin& bin)
{
  bins_.emplace_back(bin);
}


void
esScaleHandle::sortBins()
{
  switch (type_)
  {
    // Sort eta scales by minimum to preserve two's complement hw_index
    case EtaScale:
      sortBinsByMinimum();
      break;
    default:
      sortBinsByHwIndex();
      break;
  }
}


void
esScaleHandle::sortBinsByMinimum()
{
  std::sort(std::begin(bins_), std::end(bins_), [](const esBin& a, const esBin& b) {
    return (a.minimum < b.minimum);
  });
}


void
esScaleHandle::sortBinsByHwIndex()
{
  std::sort(std::begin(bins_), std::end(bins_), [](const esBin& a, const esBin& b) {
    return (a.hw_index < b.hw_index);
  });
}

} // namespace tmeventsetup
