FROM cern/cc7-base:latest

RUN yum install -y \
  which \
  make \
  gcc-c++ \
  boost-system \
  boost-filesystem \
  boost-devel \
  xsd \
  xerces-c-devel

WORKDIR /build
COPY . /build

RUN ./configure
RUN make clean
RUN make dist-clean
RUN make genxsd
RUN make all

RUN . ./env.sh; make test

CMD ["/bin/bash"]
