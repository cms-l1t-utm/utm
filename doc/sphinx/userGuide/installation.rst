..

Installation
============

Build locally
-------------

  .. code-block:: bash

    git clone https://gitlab.cern.ch/cms-l1t-utm/utm.git
    cd utm
    git checkout utm_0.13.0
    ./configure
    make all

Build for CMSSW
---------------

See :doc:`cmssw`

Build for XDAQ 15
-----------------

  .. code-block:: bash

    git clone https://gitlab.cern.ch/cms-l1t-utm/utm.git
    cd utm
    git checkout utm_0.13.0
    ./configure xdaq
    make all
    make rpm
