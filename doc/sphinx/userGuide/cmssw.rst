..

CMSSW integration
=================

The UTM library can be used as external CMSSW library. See also
https://twiki.cern.ch/twiki/bin/view/CMS/GlobalTriggerUpgradeL1T-uTme#uGT_Trigger_Menu_library_utm

In lack of support of the CodeSynthesis XSD-C++ compiler on CMSSW/LXPLUS machines
the auto generated XML bindings can be pre-generated. This requires to pack the
XSD headers with the UTM library.

Bundle XSD
----------

**Note:** these steps are required only when upgrading to a newer XSD-C++
version or in case the XML format is changed.

**Prerequisites:**

* Machine with CC7 (depends on CMSSW target)

  * Xerces C library (+3.1)
  * CodeSynthesis XSD-C++ compiler (+4.0.0)

**Instructions:**

* checkout codebase:

  .. code-block:: bash

    git clone https://gitlab.cern.ch/cms-l1t-utm/utm.git
    cd utm
    git checkout <tag/branch>

* embed the XSD headers:

  .. code-block:: bash

    cp -pr /usr/include/xsd xsd
    export XSDCXX=xsd

* generate XML bindings:

  .. code-block:: bash

    cd tmXsd/gen-xsd-type/
    make -f Makefile.xsdcxx all

Build external library
----------------------

* get CMSSW environment:

  .. code-block:: bash

    cmsrel CMSSW_13_0_0_pre4
    cd CMSSW_13_0_0_pre4
    cmsenv

* fetch tag/branch from gitlab

  .. code-block:: bash

    git clone https://gitlab.cern.ch/cms-l1t-utm/utm.git
    cd utm
    git checkout utm_0_11_0

* build the extgernal library (preliminary):

  .. code-block:: bash

    export $(scram tool info xerces-c | grep XERCES_C_BASE=)
    echo "XERCES_C_BASE=$XERCES_C_BASE"
    export $(scram tool info boost | grep BOOST_BASE=)
    echo "BOOST_BASE=$BOOST_BASE"
    ./configure
    make all
    make install PREFIX=.

* setup the external library:

  .. code-block:: bash

    cd $CMSSW_BASE/src
    # Now edit ../config/toolbox/${SCRAM_ARCH}/tools/selected/utm.xml and change
    # UTM_BASE to the directory pointed to by the output of "echo $CMSSW_BASE/utm"
    # or according to install PREFIX used above.
    git cms-addpkg L1Trigger/L1TGlobal ## for example
    scram setup utm
    scram b -j4
