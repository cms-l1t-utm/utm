..

Grammar
========

Level-1 Trigger algorithms (also referred as seeds) are expressed using a
purpose built language, referred to as UTM grammar.

.. note::
``
   This page is still a stub.

Objects
-------

  * ``MU`` muon
  * ``EG`` electron/gamma
  * ``TAU`` tau
  * ``JET`` jet
  * ``ETT`` total Et
  * ``HTT`` total hadronic Et
  * ``ETM`` missing Et
  * ``HTM`` missing hadronic Et
  * ``MBT0HFP`` Minimum Bias HF+ threshold 0
  * ``MBT1HFP`` Minimum Bias HF+ threshold 1
  * ``MBT0HFM`` Minimum Bias HF- threshold 0
  * ``MBT1HFM`` Minimum Bias HF- threshold 1
  * ``ETTEM`` total Et with ECAL only
  * ``ETMHF`` missing Et with HF
  * ``HTMHF`` missing Ht with HF
  * ``TOWERCOUNT`` calo tower count
  * ``ASYMET`` asymmetry ET
  * ``ASYMHT`` asymmetry HT
  * ``ASYMETHF`` asymmetry ET with HF
  * ``ASYMHTHF`` asymmetry HT with HF
  * ``ZDCP`` ZDC+
  * ``ZDCM`` ZDC-

Signals
-------

  * ``CENT0`` centrality 0
  * ``CENT1`` centrality 1
  * ``CENT2`` centrality 2
  * ``CENT3`` centrality 3
  * ``CENT4`` centrality 4
  * ``CENT5`` centrality 5
  * ``CENT6`` centrality 6
  * ``CENT7`` centrality 7
  * ``MUS0`` muon shower 0
  * ``MUS1`` muon shower 1
  * ``MUSOOT0`` muon shower out of time 0
  * ``MUSOOT1`` muon shower out of time 1
  * ``MUS2`` muon shower 2

ML Triggers
-----------

  * ``ADT`` anomaly detection trigger
  * ``AXO`` axol1tl trigger
  * ``TOPO`` topological trigger
  * ``CICADA`` cicada trigger

External Signals
----------------

- ``EXT``

Functions
---------

Invariant mass
..............

Calculates the invariant mass of two objects.

**Syntax**

  .. code-block:: c

    mass_inv{ obj1, obj2 }[ cut, ... ]

**Objects**

 * requires two objects of type MU, EG, TAU, JET.

**Cuts**

* mass range cut (MASS, required)
* delta-R cut (DR, optional)
* delta-Eta cut (DETA, optional)
* delta-Phi cut (DPHI, optional)
* two body Pt cut (TBPT, optional)


Transverse mass
...............

Calculates the transverse mass of two objects.

**Syntax**

  .. code-block:: c

    mass_trv{ obj1, obj2 }[ cut, ... ]

**Objects**

 * requires two objects of type MU, EG, TAU, JET, ETM, HTM, ETMHF or HTMHF.

**Cuts**

* mass range cut (MASS, required)
* delta-R cut (DR, optional)
* delta-Eta cut (DETA, optional)
* delta-Phi cut (DPHI, optional)
* two body Pt cut (TBPT, optional)

Function cuts
-------------

Two body Pt
...........

Defines a minimum threshold in GeV for the origin object used for mass triggers.

**Syntax**

  .. code-block:: c

    TBPT_<<NAME>>
