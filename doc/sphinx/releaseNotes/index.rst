.. utm - utm documentation release notes.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

UTM release notes
=================

Welcome to the UTM release notes!

Contents:

.. toctree::
   :maxdepth: 2

   v0_13_0
   v0_12_0
   v0_11_2
   v0_11_1
   v0_11_0
   v0_10_1
   v0_10_0
   v0_9_1
   v0_9_0
   v0_8_2
   v0_8_1
   v0_8_0
   v0_7_5
   v0_7_4
   v0_7_3
   v0_7_2
   v0_7_1
   v0_7_0
   v0_6_5
   v0_6_4
   v0_6_3
   v0_6_2
   v0_6_1
   v0_6_0
   v0_5_1
   v0_5_0
   v0_4_0
   v0_3_0
   v0_2_0
   v0_1_0
