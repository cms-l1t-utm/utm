..

Version 0.11.1
==============

**Bugfixes:**

* fixed compiling code without ``-DNDEBUG``.
* fixed potential issue with reference to local value using ``tmtable::get``.

**Backward incompatibility changes:**

Migration guide: v0.11.0 to 0.11.1
----------------------------------

Nothing to do.
